# JView UI

<img src="./logo.png" alt="logo" title="logo" width="250" />

JView UI，是一套基于 JQuery 的开源 UI 组件库，为 Web
应用提供了一些基础的 UI 组件，主要应用于 PC
界面。我们将大部分原先组件需要编写多行 HTML
代码才能实现的效果，封装为单行或少量的 HTML 代码，并且提供了部分 API
用于实现组件，另外我们还将持续优化和更新更多的 UI 组件。

## 概要

- 简短精悍、可识别度高的组件 HTML 标签

- 减少编写的 HTML 代码量，尽量避免冗余且庞大的结构

- 非常适合于小中型网页的使用

- 细致、漂亮的 UI

- 事无巨细的文档

- 友好的 API

### 安装

#### CSS 文件

将 JView 的 CSS 文件以 `<link>` 标签的形式添加到 `<head>` 标签中，并放置在所有其它样式表之前。

```html
<link rel="stylesheet" href="JView.min.css"></link>
```

#### JS 文件

JView 所提供的许多组件都依赖与 jQuery 以及我们自己的 JavaScript 插件。将以下 `<script>` 标签放到页面 `<body>` 标签之前即可起作用。引入顺序为： 先引入 jQuery，然后是 我们的 JavaScript 插件。

```html
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
<script src="JView.min.js"></script>
```

### 组件示例

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <!-- alert组件示例 -->
    <j-alert message="Hello, world! 这是一条提示消息来自alert组件"></j-alert>

    <!-- 1.引入jQuery -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <!-- 2.引入JView Js -->
    <script src="JView.min.js"></script>
    <script>
      $(() => {
        // Message组件示例
        $.Message.info("这是一条消息提示，来自Message组件");
        //Notice 组件示例
        $.Notice.info("这是一条通知提醒，来自Notice组件");
      });
    </script>
  </body>
</html>
```

## JView 使用示例

## `Alert 警告提示`

概述

警告提示，展现需要关注的信息。

## 代码示例

### 基础用法

- 最简单的用法，适用于简短的警告提示。

```html
<j-alert message="成功提示的文案" type="success"></j-alert>
```

### 四种样式

- 有四种样式可以选择 `info`、`success`、`warning`、`error`。

```html
<j-alert message="成功提示的文案" type="success"></j-alert>
<j-alert message="消息提示的文案"></j-alert>
<j-alert message="警告提示的文案" type="warning"></j-alert>
<j-alert message="错误提示的文案" type="error"></j-alert>
```

### 含描述信息

- 通过设置属性 `desc` 展示描述内容。

- 或者通过添加标签 `<slot name="desc"></slot>` 设置描述内容，会覆盖原先的 `desc` 属性的内容

```html
<j-alert
  message="成功提示的文案"
  desc="This is the message of success description. This is the message of success description"
  type="success"
>
</j-alert>
<j-alert
  message="消息提示的文案"
  desc="This is the message of info description. This is the message of info description"
>
</j-alert>
<j-alert
  message="警告提示的文案"
  desc="This is the message of warning description. This is the message of warning description"
  type="warning"
>
</j-alert>
<j-alert
  message="错误提示的文案"
  desc="This is the message of error description. This is the message of error description"
  type="error"
>
</j-alert>
<j-alert message="错误提示的文案" type="error">
  <slot name="desc"
    >This is the message of error description. This is the message of error
    description</slot
  >
</j-alert>
```

### 图标

- 通过设置属性 `data-show-icon="true"` 且根据 type 属性自动添加不同图标

- 可以通过属性 `data-icon` 自定义图标，或者使用 slot 标签 `<slot name="icon"></slot>`，自定义的图标会替换原先的关闭图标

```html
<j-alert
  message="成功提示的文案"
  type="success"
  data-show-icon="true"
  data-banner="true"
></j-alert>
<j-alert message="消息提示的文案" data-show-icon="true"></j-alert>
<j-alert
  message="警告提示的文案"
  type="warning"
  data-show-icon="true"
></j-alert>
<j-alert message="错误提示的文案" type="error" data-show-icon="true"></j-alert>
<j-alert
  message="成功提示的文案"
  desc="Detailed description and advice about successful copywriting."
  type="success"
  data-show-icon="true"
>
</j-alert>
<j-alert
  message="消息提示的文案"
  desc="Additional description and information about copywriting."
  data-show-icon="true"
>
</j-alert>
<j-alert
  message="警告提示的文案"
  desc="This is a warning notice about copywriting."
  type="warning"
  data-show-icon="true"
>
</j-alert>
<j-alert
  message="错误提示的文案"
  desc="This is an error message about copywriting."
  type="error"
  data-show-icon="true"
>
</j-alert>
<j-alert
  message="自定义图标"
  desc="Custom icon copywriting. Custom icon copywriting. Custom icon copywriting."
  data-show-icon="true"
  data-icon="<i class='jv-icon icon-paper-plane'></i>"
>
</j-alert>
```

### 可关闭的警告提示

- 通过设置属性 `data-closable="true"` 显示关闭按钮，点击可平滑、自然的关闭警告提示。

- 可以通过设置属性 `data-close-text` 自定义关闭文字，自定义的文字会替换原先的关闭图标。

```html
<j-alert message="成功提示的文案" type="success" data-closable="true">
</j-alert>
<j-alert
  message="警告提示的文案"
  desc="This is a warning notice about copywriting."
  type="warning"
  data-closable="true"
>
</j-alert>
<j-alert message="消息提示的文案" data-closable="true"> </j-alert>
<j-alert
  message="自定义 close-text"
  data-closable="true"
  data-close-text="知道了"
>
</j-alert>
```

### 顶部公告

- 设置属性 `data-banner="true"` 可以应用页面顶部通告形式。

```html
<j-alert message="顶部通告提示文案" type="warning" data-banner="true"></j-alert>
```

## Attributes

| 属性            | 说明                                                        | 类型    | 默认值 |
| --------------- | ----------------------------------------------------------- | ------- | ------ |
| type            | 警告提示样式，可选值为`info`、`success`、`warning`、`error` | String  | info   |
| message         | 警告提示内容                                                | String  | -      |
| desc            | 警告提示的辅助性文字介绍                                    | String  | -      |
| data-closable   | 是否可关闭                                                  | Boolean | false  |
| data-close-text | 关闭按钮自定义文本，`data-closable` 为 `true` 时有效        | String  | -      |
| data-show-icon  | 是否显示图标                                                | Boolean | false  |
| data-icon       | 自定义图标，`data-show-icon` 为 `true` 时有效               | String  | -      |
| data-banner     | 是否用作顶部公告                                            | Boolean | false  |

## Slot

| 名称    | 说明                   |
| ------- | ---------------------- |
| message | 警告提示标题           |
| desc    | 警告提示辅助性文字介绍 |
| icon    | 自定义图标内容         |

### 控制 alert 在合适的时机显示隐藏

- 通过给 alert 标签设置外层容器，用于显示隐藏

- 设置属性 `data-toggle="display"` 会自动根据 点击的 `data-target` 目标元素的 `display` 属性判断是否显示或隐藏

```html
<button
  class="jv-btn jv-btn-default"
  data-toggle="display"
  data-target="#demoShowHiddenAlert"
>
  显示alert
</button>
<div style="display:none;" id="demoShowHiddenAlert">
  <j-alert message="这是一条消息提示文案"></j-alert>
</div>
```

## `Avatar 头像`

## 概述

用来代表用户或事物，支持图片、图标或字符展示。

## 代码示例

### 基础用法

- 头像有四种尺寸，两种形状可选，默认为圆形。

```html
<span class="jv-avatar jv-avatar-circle jv-avatar-huge"
  ><i class="jv-icon icon-user"></i
></span>
<span class="jv-avatar jv-avatar-circle jv-avatar-large"
  ><i class="jv-icon icon-user"></i
></span>
<span class="jv-avatar jv-avatar-circle jv-avatar-medium"
  ><i class="jv-icon icon-user"></i
></span>
<span class="jv-avatar jv-avatar-circle jv-avatar-small"
  ><i class="jv-icon icon-user"></i
></span>
```

- 方形

```html
<span class="jv-avatar jv-avatar-square jv-avatar-huge"
  ><i class="jv-icon icon-user"></i
></span>
<span class="jv-avatar jv-avatar-square jv-avatar-large"
  ><i class="jv-icon icon-user"></i
></span>
<span class="jv-avatar jv-avatar-square jv-avatar-medium"
  ><i class="jv-icon icon-user"></i
></span>
<span class="jv-avatar jv-avatar-square jv-avatar-small"
  ><i class="jv-icon icon-user"></i
></span>
```

### 类型

- 支持三种类型：图片、Icon 以及字符，其中 Icon 和字符型可以自定义图标颜色及背景色。

```html
<span class="jv-avatar jv-avatar-circle jv-avatar-medium"
  ><i class="jv-icon icon-user"></i
></span>
<span class="jv-avatar jv-avatar-circle jv-avatar-medium"
  ><img src="./images.jpg"
/></span>
<span
  class="jv-avatar jv-avatar-circle jv-avatar-medium"
  style="background: #a123f1;"
>
  <span class="jv-avatar-text">瑞</span>
</span>
<span
  class="jv-avatar jv-avatar-circle jv-avatar-medium"
  style="background: #6bc800;"
>
  <i class="jv-icon icon-user"></i>
</span>
```

### 自动调整字符大小

- 对于字符型的头像，当字符串较长时，字体大小可以根据头像宽度自动调整。

```html
<span class="jv-avatar jv-avatar-circle jv-avatar-medium"
  ><span class="jv-avatar-text">Usase</span></span
>
<span class="jv-avatar jv-avatar-circle jv-avatar-medium"
  ><span class="jv-avatar-text">USER</span></span
>
```

## `BackTop 返回顶部`

## 概述

当页面内容冗长，需要快捷返回顶部时使用，一般放置在页面右下角位置。

## 代码示例

### 基础用法

- 默认位置距离页面右部和底部 30px，滚动至距顶端 400px 时显示。

```html
<j-backtop id="backTop"></j-backtop>
<script>
  $(() => {
    $("#backTop").backTop();
  });
</script>
```

### 自定义位置

- 可改变 backTop 组件的位置

```html
<j-backtop id="backTop"></j-backtop>
<script>
  $(() => {
    $("#backTop").backTop({
      bottom: 100,
      right: 60,
    });
  });
</script>
```

## API

- `$(el).backTop(config)`

| 属性     | 说明                                      | 类型   | 默认值 |
| -------- | ----------------------------------------- | ------ | ------ |
| height   | 页面滚动高度达到该值时才显示 BackTop 组件 | Number | 400    |
| bottom   | 组件距离底部的距离                        | Number | 30     |
| right    | 组件距离右部的距离                        | Number | 30     |
| duration | 滚动动画持续时间，单位 毫秒               | Number | 450    |

## `Badge 徽标`

## 概述

用于提示用户消息通知未读数的角标。

## 代码示例

### 基础用法

- 最基础的使用方法。

- 当 `<sup></sup>`的内容空为空或为"0"时会自动隐藏徽标

```html
<style>
  .demo-badge {
    width: 42px;
    height: 42px;
    background: #eee;
    border-radius: 6px;
    display: inline-block;
  }
</style>

<div class="jv-badge">
  <sup class="jv-badge-count jv-badge-error">6</sup>
  <a href="#" class="demo-badge"></a>
</div>
```

### 小红点

- 以红点的形式标注需要关注的内容。

```html
<div class="jv-badge">
  <sup class="jv-badge-dot">1</sup>
  <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
  <sup class="jv-badge-dot">1</sup>
  <a href="#">订阅号消息</a>
</div>
```

### 最大消息数

- 通过设置 `data-max` 属性设置一个最大值，当超过时，会显示\${max}+

```html
<div class="jv-badge" data-max="99">
  <sup class="jv-badge-count">100</sup>
  <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge" data-max="999">
  <sup class="jv-badge-count">1000</sup>
  <a href="#" class="demo-badge"></a>
</div>
```

### 独立展示徽标

- 可以不添加任何标签内容独立展示。

```html
<div class="jv-badge">
  <sup class="jv-badge-count jv-badge-alone">10</sup>
</div>
<div class="jv-badge">
  <sup class="jv-badge-count jv-badge-success jv-badge-alone">15</sup>
</div>
```

### 自定义内容

- 可以显示数字以外的文本内容。

```html
<div class="jv-badge">
  <sup class="jv-badge-count">new</sup>
  <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
  <sup class="jv-badge-count">hot</sup>
  <a href="#" class="demo-badge"></a>
</div>
```

### 预设颜色

- 提供六种 `primary` `normal` `success` `info` `warning` `error`

```html
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-primary">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-normal">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-success">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-info">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
 <s class="jv-badge">
    <sup class="jv-badge-count jv-badge-warning">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-error">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
```

## API

### Badge

| 属性     | 说明             | 类型   | 默认值 |
| -------- | ---------------- | ------ | ------ |
| data-max | 展示封顶的数字值 | Number | 99     |

## `Breadcrumb 面包屑`

## 概述

- 显示网站的层级结构，告知用户当前所在位置，以及在需要向上级导航时使用。

## 代码示例

```html
<ul class="jv-breadcrumb" role="navigation">
  <li>
    <b><a href="#">首页</a></b>
    <span class="jv-breadcrumb-item-separator">/</span>
  </li>
  <li>
    <a href="#">活动管理</a>
    <span class="jv-breadcrumb-item-separator">/</span>
  </li>
  <li>
    <span href="#">活动列表</span>
    <span class="jv-breadcrumb-item-separator ">/</span>
  </li>
  <li>
    <span href="#">活动详情</span>
  </li>
</ul>
```

### 自定义分隔符

```html
<ul class="jv-breadcrumb" role="navigation">
  <li>
    <b><a href="#">首页</a></b>
    <span class="jv-breadcrumb-item-separator"
      ><i class="demo-icon icon-right-open"></i
    ></span>
  </li>
  <li>
    <b><a href="#">活动管理</a></b>
    <span class="jv-breadcrumb-item-separator"
      ><i class="demo-icon icon-right-open"></i
    ></span>
  </li>
  <li>
    <span href="#">活动列表</span>
    <span class="jv-breadcrumb-item-separator "
      ><i class="demo-icon icon-right-open"></i
    ></span>
  </li>
  <li>
    <span href="#">活动详情</span>
  </li>
</ul>

<ul class="jv-breadcrumb" role="navigation">
  <li>
    <b><a href="#">首页</a></b>
    <b style="color: #f50;padding: 0 5px;" class="jv-breadcrumb-item-separator"
      >=></b
    >
  </li>
  <li>
    <b><a href="#">活动管理</a></b>
    <b style="color: #f50;padding: 0 5px;" class="jv-breadcrumb-item-separator"
      >=></b
    >
  </li>
  <li>
    <b><a href="#">活动列表</a></b>
    <b style="color: #f50;padding: 0 5px;" class="jv-breadcrumb-item-separator"
      >=></b
    >
  </li>
  <li>
    <span href="#">活动详情</span>
  </li>
</ul>
```

### 带图标

```html
<ul class="jv-breadcrumb" role="navigation">
    <li>
        <b><a href="#"><i class="demo-icon icon-home"></i>首页</a></b>
        <span class="jv-breadcrumb-item-separator">/</i></span>
    </li>
    <li>
        <b><a href="#"><i class="demo-icon icon-menu"></i>活动管理</a></b>
        <span class="jv-breadcrumb-item-separator">/</i></span>
    </li>
    <li>
        <b><a href="#"><i class="demo-icon icon-th-list"></i>活动列表</a></b>
        <span class="jv-breadcrumb-item-separator ">/</i></span>
    </li>
    <li>
        <b><a href="#"><i class="demo-icon icon-zoom-in"></i>活动详情</a></b>
    </li>
</ul>
```

## `Button 按钮`

常用的操作按钮，触发业务逻辑时使用。

## 代码示例

### 基础用法

- 按钮类型有：默认按钮、主按钮、虚线按钮、文字按钮以及五种颜色按钮。

```html
<button type="button" class="jv-btn jv-btn-default">默认按钮</button>
<button type="button" class="jv-btn jv-btn-primary">主要按钮</button>
<button type="button" class="jv-btn jv-btn-info">信息按钮</button>
<button type="button" class="jv-btn jv-btn-success">成功按钮</button>
<button type="button" class="jv-btn jv-btn-warning">警告按钮</button>
<button type="button" class="jv-btn jv-btn-error">危险按钮</button>
<button type="button" class="jv-btn jv-btn-dark">暗色按钮</button>
<button type="button" class="jv-btn jv-btn-dashed">虚线按钮</button>
<button type="button" class="jv-btn jv-btn-text">文字按钮</button>
```

### 反色按钮

- 无背景颜色，在鼠标经过时才显示。提供的按钮类型有：主按钮，信息按钮，警告按钮，危险按钮，暗色按钮

```html
<button type="button" class="jv-btn jv-btn-outline-primary">主要按钮</button>
<button type="button" class="jv-btn jv-btn-outline-info">信息按钮</button>
<button type="button" class="jv-btn jv-btn-outline-success">成功按钮</button>
<button type="button" class="jv-btn jv-btn-outline-dark">暗色按钮</button>
<button type="button" class="jv-btn jv-btn-outline-warning">警告按钮</button>
<button type="button" class="jv-btn jv-btn-outline-error">危险按钮</button>
```

### 圆角按钮

```html
<button type="button" class="jv-btn jv-btn-round jv-btn-default">
  默认按钮
</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-primary">
  主要按钮
</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-info">信息按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-success">
  成功按钮
</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-warning">
  警告按钮
</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-error">危险按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-dark">暗色按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-dashed">
  虚线按钮
</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-text">文字按钮</button>
```

### 长按钮

- 按钮宽度为 100%，常用于弹窗内操作按钮。

```html
<button type="button" class="jv-btn jv-btn-success jv-btn-long">提交</button>
<button type="button" class="jv-btn jv-btn-error jv-btn-long">删除</button>
```

### 按钮尺寸

- 按钮有三种尺寸：大、默认（中）、小。

```html
<button type="button" class="jv-btn jv-btn-primary jv-btn-large">
  大型按钮
</button>
<button type="button" class="jv-btn jv-btn-info">默认按钮</button>
<button type="button" class="jv-btn jv-btn-default jv-btn-small">
  小型按钮
</button>
<button type="button" class="jv-btn jv-btn-primary jv-btn-large jv-btn-circle">
  大型按钮
</button>
<button type="button" class="jv-btn jv-btn-info jv-btn-circle">默认按钮</button>
<button type="button" class="jv-btn jv-btn-default jv-btn-small jv-btn-circle">
  小型按钮
</button>
```

### 禁用状态

- 通过添加 disabled 属性可将按钮设置为不可用状态。

```html
<button type="button" disabled class="jv-btn jv-btn-default">默认按钮</button>
<button type="button" disabled class="jv-btn jv-btn-primary">主要按钮</button>
<button type="button" disabled class="jv-btn jv-btn-info">信息按钮</button>
<button type="button" disabled class="jv-btn jv-btn-success">成功按钮</button>
<button type="button" disabled class="jv-btn jv-btn-warning">警告按钮</button>
<button type="button" disabled class="jv-btn jv-btn-error">危险按钮</button>
<button type="button" disabled class="jv-btn jv-btn-dark">暗色按钮</button>
<button type="button" disabled class="jv-btn jv-btn-dashed">虚线按钮</button>
<button type="button" disabled class="jv-btn jv-btn-text">文字按钮</button>
```

### 图标按钮

- 带图标的按钮可增强辨识度（有文字）或节省空间（无文字）。

```html
<button type="button" class="jv-btn jv-btn-primary jv-btn-large">
  <i class="jv-icon icon-download"></i>Download
</button>
<button type="button" class="jv-btn jv-btn-default jv-btn-circle-large">
  <i class="jv-icon icon-search"></i>
</button>
<button type="button" class="jv-btn jv-btn-primary jv-btn-circle-large">
  <i class="jv-icon icon-off"></i>
</button>
<button type="button" class="jv-btn jv-btn-info jv-btn-circle-large">
  <i class="jv-icon icon-twitter"></i>
</button>
<button type="button" class="jv-btn jv-btn-success jv-btn-circle-large">
  <i class="jv-icon icon-ok"></i>
</button>
<button type="button" class="jv-btn jv-btn-warning jv-btn-circle-large">
  <i class="jv-icon icon-star-empty"></i>
</button>
<button type="button" class="jv-btn jv-btn-error jv-btn-circle-large">
  <i class="jv-icon icon-trash"></i>
</button>
<button type="button" class="jv-btn jv-btn-primary">
  <i class="jv-icon icon-search"></i>search
</button>
<button type="button" class="jv-btn jv-btn-primary jv-btn-round">
  <i class="jv-icon icon-search"></i>search
</button>
<button type="button" class="jv-btn jv-btn-default">
  <i class="jv-icon icon-search"></i>search
</button>
<button type="button" class="jv-btn jv-btn-default jv-btn-round">
  <i class="jv-icon icon-search"></i>search
</button>
```

### 按钮组

- 以按钮组的方式出现，常用于多项类似操作。

```html
<div class="jv-btn-group">
  <button type="button" class="jv-btn jv-btn-primary">
    <i class="demo-icon icon-left-open"></i>上一页
  </button>
  <button type="button" class="jv-btn jv-btn-primary">
    下一页<i class="demo-icon icon-right-open"></i>
  </button>
</div>
<div class="jv-btn-group">
  <button type="button" class="jv-btn jv-btn-primary">
    <i class="demo-icon icon-to-start"></i>
  </button>
  <button type="button" class="jv-btn jv-btn-primary">
    <i class="demo-icon icon-pause"></i>
  </button>
  <button type="button" class="jv-btn jv-btn-primary">
    <i class="demo-icon icon-to-end"></i>
  </button>
</div>
```

## `Collapse 折叠面板`

## 概述

- 将内容区域折叠/展开。

## 代码示例

### 基础用法

- 设置属性 data-default-active-key 与 data-key 相对应的数值可以设置默认展开第几个，不设置默认全部折叠

```html
<j-collapse data-default-active-key="1">
  <j-collapse-panel data-key="1" data-title="维克多· 雨果">
    <p>維克多·馬里·雨果，法國浪漫主義文學的代表人物......</p>
  </j-collapse-panel>
  <j-collapse-panel data-key="2" data-title="海伦凯勒">
    <p>
      海伦·凯勒（Helen
      Keller，1880年6月27日－1968年6月1日），美国著名的女作家、教育家、慈善家、社会活动家。
    </p>
  </j-collapse-panel>
  <j-collapse-panel data-key="3" data-title="艾萨克·牛顿">
    <p>
      艾薩克·牛頓爵士是一位英格蘭物理學家、數學家、天文學家、自然哲學家和煉金術士.....
    </p>
  </j-collapse-panel>
</j-collapse>
```

### 手风琴

- 通过设置属性 data-accordion="true" 开启手风琴模式，每次只能打开一个面板。

```html
<j-collapse data-default-active-key="1" data-accordion="true">
  <j-collapse-panel data-key="1" data-title="维克多· 雨果">
    <p>維克多·馬里·雨果，法國浪漫主義文學的代表人物......</p>
  </j-collapse-panel>
  <j-collapse-panel data-key="2" data-title="海伦凯勒">
    <p>
      海伦·凯勒（Helen
      Keller，1880年6月27日－1968年6月1日），美国著名的女作家、教育家、慈善家、社会活动家。
    </p>
  </j-collapse-panel>
  <j-collapse-panel data-key="3" data-title="艾萨克·牛顿">
    <p>
      艾薩克·牛頓爵士是一位英格蘭物理學家、數學家、天文學家、自然哲學家和煉金術士.....
    </p>
  </j-collapse-panel>
</j-collapse>
```

### 简洁模式

- 设置属性 data-simple="true" 可以显示为不带边框和背景色的简洁模式。

```html
<j-collapse data-simple="true">
  <j-collapse-panel data-title="维克多· 雨果">
    <p>維克多·馬里·雨果，法國浪漫主義文學的代表人物......</p>
  </j-collapse-panel>
  <j-collapse-panel data-title="海伦凯勒">
    <p>
      海伦·凯勒（Helen
      Keller，1880年6月27日－1968年6月1日），美国著名的女作家、教育家、慈善家、社会活动家。
    </p>
  </j-collapse-panel>
  <j-collapse-panel data-title="艾萨克·牛顿">
    <p>
      艾薩克·牛頓爵士是一位英格蘭物理學家、數學家、天文學家、自然哲學家和煉金術士.....
    </p>
  </j-collapse-panel>
</j-collapse>
```

## API

## collapse

| 属性                    | 说明                                           | 类型    | 默认值 |
| ----------------------- | ---------------------------------------------- | ------- | ------ |
| data-default-active-key | 当前激活的面板的 key                           | Number  | -1     |
| data-accordion          | 是否开启手风琴模式，开启后每次至多展开一个面板 | Boolean | false  |
| data-simple             | 是否开启简洁模式                               | Boolean | false  |

## collapse-panel

| 属性       | 说明                                                        | 类型   | 默认值 |
| ---------- | ----------------------------------------------------------- | ------ | ------ |
| data-title | 面板标题                                                    | String | -      |
| data-key   | 当前面板的 key，与 Collapse 的 data-default-active-key 对应 | Number | -2     |

## `Divider 分割线`

## 概述

- 区隔内容的分割线。对不同章节的文本段落进行分割。对行内文字/链接进行分割，例如表格的操作列。

## 代码示例

### 基础用法

- 分割线默认为为横向

```html
<div class="jv-divider jv-divider-horizontal"></div>
```

### 设置分割线文本内容

- 提供 3 个方向：left center right

```html
<div class="jv-divider jv-divider-horizontal">
  <span class="jv-divider-text-left">居左文本</span>
</div>
<div class="jv-divider jv-divider-horizontal">
  <span class="jv-divider-text-center">居中文本</span>
</div>
<div class="jv-divider jv-divider-horizontal">
  <span class="jv-divider-text-right">居右文本</span>
</div>
```

### 垂直分割线

```html
<div class="jv-divider jv-divider-vertical"></div>
```

## `Dropdown 下拉菜单`

## 概述

展示一组折叠的下拉菜单。

### 触发方式

- 通过设置属性 `data-trigger` 可以更改触发方式，可选项为 click 、 hover(默认)、contextMenu

```html
<div class="jv-dropdown">
  <div class="jv-dropdown-rel">
    <a href="javascript:void(0)">hover 触发</a>
  </div>
  <div class="jv-dropdown-select-dropdown jv-dropdown-slide-out">
    <ul class="jv-dropdown-menu" role="menu">
      <li class="jv-dropdown-item" role="menuitem">选项一</li>
      <li class="jv-dropdown-item" role="menuitem">选项二</li>
      <li class="jv-dropdown-item" role="menuitem">选项三</li>
    </ul>
  </div>
</div>
<div class="jv-dropdown" data-trigger="click">
  <div class="jv-dropdown-rel">
    <a href="javascript:void(0)">click 触发</a>
  </div>
  <div class="jv-dropdown-select-dropdown jv-dropdown-slide-out">
    <ul class="jv-dropdown-menu" role="menu">
      <li class="jv-dropdown-item" role="menuitem">选项一</li>
      <li class="jv-dropdown-item" role="menuitem">选项二</li>
      <li class="jv-dropdown-item" role="menuitem">选项三</li>
    </ul>
  </div>
</div>
<div class="jv-dropdown" data-trigger="contextMenu">
  <div class="jv-dropdown-rel">
    <a href="javascript:void(0)">右键触发</a>
  </div>
  <div class="jv-dropdown-select-dropdown jv-dropdown-slide-out">
    <ul class="jv-dropdown-menu" role="menu">
      <li class="jv-dropdown-item" role="menuitem">返回</li>
      <li class="jv-dropdown-item" role="menuitem">
        <a href="#" style="color: red;">删除</a>
      </li>
    </ul>
  </div>
</div>
```

### 对齐方向

- 通过设置属性 `data-placement` 可以更改下拉菜单出现的方向。支持 6 个弹出位置，默认方向为 bottom。

```html
<div class="jv-dropdown" data-placement="bottom-left">...</div>
<div class="jv-dropdown">...</div>
<div class="jv-dropdown" data-placement="bottom-right">...</div>
<div class="jv-dropdown" data-placement="top-left">...</div>
<div class="jv-dropdown" data-placement="top">...</div>
<div class="jv-dropdown" data-placement="top-right">...</div>
```

## Attributes

| 属性           | 说明                                                                                           | 类型   | 默认值 |
| -------------- | ---------------------------------------------------------------------------------------------- | ------ | ------ |
| data-trigger   | 触发方式，可选值为 `hover`（悬停）`click`（点击）`contextMenu`（右键）                         | String | hover  |
| data-placement | 下拉菜单出现的位置，可选值为`top` `top-left` `top-right` `bottom` `bottom-left` `bottom-right` | String | bottom |

## `Input 输入框`

## 概述

- 基本表单组件，支持 input 和 textarea，并在原生控件基础上进行了功能扩展，可以组合使用。

## 代码示例

### 基础用法

- 可以直接在 wrapper 父容器设置 style 来改变输入框的宽度，默认 100%。

```html
<div class="jv-input-wrapper" style="width: 350px;">
  <input type="text" placeholder="请输入内容" class="jv-input" />
</div>
```

### 尺寸

- 输入框有三种尺寸：大、默认（中）、小

```html
<div class="jv-input-wrapper">
  <input type="text" placeholder="大型输入框" class="jv-input jv-input-large" />
</div>
<div class="jv-input-wrapper">
  <input type="text" placeholder="默认输入框" class="jv-input" />
</div>
<div class="jv-input-wrapper">
  <input type="text" placeholder="小型输入框" class="jv-input jv-input-small" />
</div>
```

### 可清空

- 通过在 wrapper 父容器设置属性 `data-clearable="true"` 可显示清空按钮。

```html
<div class="jv-input-wrapper" data-clearable="true">
  <input type="text" placeholder="可清空的输入框" class="jv-input" />
</div>
```

### 输入长度限制

- 通过在 wrapper 父容器设置属性 `data-show-word-limit="true"` 可以显示字数统计，需配合 `maxlength` 属性来限制输入长度。

```html
<div class="jv-input-wrapper" data-show-word-limit="true">
  <input
    type="text"
    maxlength="10"
    placeholder="显示字数统计"
    class="jv-input jv-input-with-suffix"
  />
</div>
<div class="jv-input-wrapper jv-input-textarea" data-show-word-limit="true">
  <textarea
    maxlength="100"
    placeholder="请输入内容"
    class="jv-input"
  ></textarea>
</div>
```

### 密码框

- 在 `type="password"` 时会自动显示切换密码图标，可以切换显示隐藏密码。

- 不显示切换密码图标，则在 wrapper 父容器设置属性 `data-show-hide-password="false"`

```html
<div class="jv-input-wrapper">
  <input
    type="password"
    placeholder="请输入密码"
    class="jv-input jv-input-with-suffix"
  />
</div>
```

### 带 Icon 的输入框

```html
<div class="jv-input-wrapper">
  <span class="jv-input-prefix">
    <i class="jv-icon icon-user-circle-o"></i>
  </span>
  <input
    type="text"
    placeholder="请输入姓名"
    class="jv-input jv-input-with-prefix"
  />
</div>
<br />
<div class="jv-input-wrapper">
  <input
    type="text"
    placeholder="请输入内容"
    class="jv-input jv-input-with-suffix"
  />
  <span class="jv-input-suffix">
    <i class="jv-icon icon-search"></i>
  </span>
</div>
```

### 搜索框

```html
<div class="jv-input-wrapper">
  <i class="jv-icon jv-input-icon icon-search"></i>
  <input
    type="search"
    placeholder="请输入内容"
    class="jv-input jv-input-with-suffix"
  />
</div>
<div class="jv-input-wrapper jv-input-group jv-input-group-with-append">
  <input type="search" placeholder="请输入内容" class="jv-input" />
  <div class="jv-input-group-append jv-input-search">
    <i class="jv-icon icon-search"></i>
  </div>
</div>
<div class="jv-input-wrapper jv-input-group jv-input-group-with-append">
  <input type="search" placeholder="请输入内容" class="jv-input" />
  <div class="jv-input-group-append jv-input-search">搜索</div>
</div>
```

### 文本域

- 通过设置属性 `rows` 控制文本域默认显示的行数。

```html
<div class="jv-input-wrapper">
  <textarea rows="8" placeholder="请输入内容" class="jv-input"></textarea>
</div>
```

### 禁用状态

- 通过在 wrapper 父容器添加 class 类名 `jv-input-disabled` 设置为不可用状态。

```html
<div class="jv-input-wrapper jv-input-disabled">
  <input type="text" placeholder="请输入内容" class="jv-input" />
</div>
<div class="jv-input-wrapper jv-input-disabled">
  <textarea placeholder="请输入内容" class="jv-input"></textarea>
</div>
```

### 复合型输入框

```html
<div
  class="jv-input-wrapper jv-input-group jv-input-group-with-prepend jv-input-group-with-append"
>
  <div class="jv-input-group-prepend">http://</div>
  <input
    type="text"
    placeholder="请输入网页链接地址"
    class="jv-input jv-input"
  />
  <div class="jv-input-group-append">.com</div>
</div>

<div class="jv-input-wrapper jv-input-group jv-input-group-with-prepend">
  <div class="jv-input-group-prepend">@</div>
  <input type="text" placeholder="用户名" class="jv-input jv-input" />
</div>

<div class="jv-input-wrapper jv-input-group  jv-input-group-with-append">
  <input type="text" placeholder="请输入邮箱" class="jv-input jv-input" />
  <div class="jv-input-group-append">@example.com</div>
</div>

<div
  class="jv-input-wrapper jv-input-group jv-input-group-with-prepend jv-input-group-with-append"
>
  <div class="jv-input-group-prepend">$</div>
  <input type="text" placeholder="请输入价格" class="jv-input jv-input" />
  <div class="jv-input-group-append">.00</div>
</div>

<div class="jv-input-wrapper jv-input-group jv-input-group-with-prepend">
  <div class="jv-input-group-prepend">UN</div>
  <input type="text" placeholder="请输入用户名" class="jv-input jv-input" />
</div>

<div class="jv-input-wrapper jv-input-group jv-input-group-with-prepend">
  <div class="jv-input-group-prepend">PW</div>
  <input type="text" placeholder="请输入密码" class="jv-input jv-input" />
</div>
```

## API

Input 属性

| 属性                    | 说明                                          | 类型    | 默认值 |
| ----------------------- | --------------------------------------------- | ------- | ------ |
| data-clearable          | 是否显示清空按钮                              | Boolean | false  |
| data-show-word-limit    | 是否显示输入字数统计，需要配合 maxlength 使用 | Boolean | false  |
| data-show-hide-password | 是否显示切换密码图标                          | Boolean | true   |

|

Input 原生属性

| 属性         | 说明                                                   | 类型    | 默认值 |
| ------------ | ------------------------------------------------------ | ------- | ------ |
| autocomplete | 自动完成功能                                           | String  | on     |
| autofocus    | 自动获取焦点                                           | Boolean | false  |
| spellcheck   | spellcheck 属性                                        | Boolean | true   |
| wrap         | wrap 属性，可选值为 hard 和 soft，仅在 textarea 下生效 | String  | soft   |
| rows         | 文本域默认行数，仅在 textarea 类型下有效               | Number  | -      |
| placeholder  | 占位文本                                               | String  | -      |
| readonly     | 设置输入框为只读                                       | Boolean | false  |
| maxlength    | 最大输入长度                                           | Number  | -      |

## `InputNumber 计数器`

## 概述

- _使用鼠标或键盘输入一定范围的标准数值。_

## 代码示例

### 基础用法

- _可以通过输入、鼠标点击或键盘的上下键来改变数值大小，通过设置属性 `data-min` 和 `data-max` 来控制计数范围。_

```html
<j-input-number data-min="1" data-max="10"></j-input-number>
```

### 小数

- _通过设置 `data-step` 属性控制每次改变的精度_

```html
<j-input-number data-min="1" data-max="10" data-step="1.2"></j-input-number>
```

### 尺寸

- _通过设置 `size` 属性为 large 和 small 将输入框设置为大和小尺寸，不设置为默认（中）尺寸。_

```html
<j-input-number size="small"></j-input-number>
<j-input-number></j-input-number>
<j-input-number size="large"></j-input-number>
```

### 禁用

- _通过设置 `data-disabled="true"` 属性禁用输入框。_

```html
<j-input-number data-disabled="true" data-val="2"></j-input-number>
```

### 只读

- _通过设置 `data-readonly="true"` 属性开启只读。_

```html
<j-input-number data-readonly="true" data-val="2"></j-input-number>
```

### 不可编辑

- _通过设置 `data-editable="true"` 属性控制是否能编辑。_

```html
<j-input-number data-editable="true" data-val="2"></j-input-number>
```

## API

Attribute

| 属性          | 说明                                                    | 类型    | 默认值    |
| ------------- | ------------------------------------------------------- | ------- | --------- |
| size          | 输入框尺寸，可选值为`large`、`small`、`default`或者不填 | String  | default   |
| data-min      | 最小值                                                  | Number  | Infinity  |
| data-max      | 最大值                                                  | Number  | -Infinity |
| data-val      | 当前输入框的初始值                                      | Number  | -         |
| data-step     | 每次改变的步伐，可以是小数                              | Number  | 1         |
| data-disabled | 设置禁用状态                                            | Boolean | false     |
| data-readonly | 是否设置为只读                                          | Boolean | false     |
| data-editable | 是否可编辑                                              | Boolean | false     |
| id            | 为输入框设置 id 选择器                                  | String  | -         |
| placeholder   | 占位文本                                                | String  | -         |
| up-id         | 为输入框的增加按钮设置 id 选择器                        | String  | -         |
| down-id       | 输入框的减少按钮设置 id 选择器                          | String  | -         |

## InputNumber demo

通过示例代码演示如何获取输入框的值。

```html
<j-input-number id="demoInput" up-id="add" down-id="reduce"></j-input-number>
<script>
  $("#add,#reduce").click(() => {
    console.log($("#demoInput").val());
  });
</script>
```

## `Link 文字链接`

## 概述

- 展示不同状态下文字超链接的颜色

## 代码示例

基础用法

- 通过添加 class 类名显示不同外观颜色

```html
<a href="#">默认链接</a>
<a href="#" class="jv-link-primary">主要链接</a>
<a href="#" class="jv-link-success">成功链接</a>
<a href="#" class="jv-link-info">信息链接</a>
<a href="#" class="jv-link-warning">警告链接</a>
<a href="#" class="jv-link-error">错误链接</a>
```

不可点击状态

- 设置属性 disabled 令文字链接变为不可用状态。

```html
<a href="#" disabled>默认链接</a>
<a href="#" disabled class="jv-link-primary">主要链接</a>
<a href="#" disabled class="jv-link-success">成功链接</a>
<a href="#" disabled class="jv-link-info">信息链接</a>
<a href="#" disabled class="jv-link-warning">警告链接</a>
<a href="#" disabled class="jv-link-error">错误链接</a>
```

## `LoadingBar 加载进度条`

## 概述

全局创建一个显示页面加载、异步请求、文件上传等的加载进度条。

## 说明

LoadingBar 只会在全局创建一个，因此在任何位置调用的方法都会控制这同一个组件。主要使用场景是路由切换和 Ajax，因为这两者都不能拿到精确的进度，LoadingBar 会模拟进度，当然也可以通过 update()方法来传入一个精确的进度，比如在文件上传时会很有用，具体见 API。

- 在异步请求中使用，以 Ajax 为例

```html
<script>
  function getData() {
      $.Loading.start();
      $.ajax({
          url: '/api/someurl',
          type: 'get',
          success: () => {
              $.Loading.finish();
          }
          error: () => {
              $.Loading.error();
          }
      });
  };
</script>
```

## 代码示例

### 基本用法

- 点击 Start 开始进度，点击 Finish 结束。在调用 start()方法后，组件会自动模拟进度，当调用 finish()或 error()时，补全进度并自动消失。

```html
<button class="jv-btn jv-btn-default" onclick="$.Loading.start()">Start</button>
<button class="jv-btn jv-btn-default" onclick="$.Loading.finish()">
  Finish
</button>
<button class="jv-btn jv-btn-default" onclick="$.Loading.error()">Error</button>
```

## API

通过直接调用以下方法来使用组件：

- `$.Loading.start()`
- `$.Loading.finish()`
- `$.Loading.error()`
- `$.Loading.update(percent)`

以上方法隐式的创建及维护 LoadingBar 组件。函数及参数说明如下：

| 函数名 | 说明                                     | 参数                      |
| ------ | ---------------------------------------- | ------------------------- |
| start  | 开始从 0 显示进度条，并自动加载进度      | 无                        |
| finish | 结束进度条，自动补全剩余进度             | 无                        |
| error  | 以错误的类型结束进度条，自动补全剩余进度 | 无                        |
| update | 精确加载到指定的进度                     | percent，指定的进度百分比 |

|

还提供了全局配置和全局销毁的方法：

- `$.Loading.config(options)`
- `$.Loading.destroy()`

```js
$.Loading.config({
  color: "green",
  failedColor: "#f0ad4e",
  height: 5,
});
```

| 属性        | 说明                            | 类型   | 默认值  |
| ----------- | ------------------------------- | ------ | ------- |
| color       | 进度条的颜色，默认为 jView 主色 | String | primary |
| failedColor | 失败时的进度条颜色              | String | error   |
| height      | 进度条高度，单位 px             | Number | 3       |
| duration    | 进度条加载速度，单位 ms         | Number | 250     |

## `Message 全局提示`

## 概述

轻量级的信息反馈组件，在顶部居中显示，并自动消失。有多种不同的提示状态可选择。

## 代码示例

### 普通提示

- 最基本的提示，默认在 3 秒后消失。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.info('这是一条普通的提示')"
>
  显示普通提示
</button>
```

### 提示类型

- 不同的提示状态：成功、警告、错误。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.info('这是一条普通的提示')"
>
  显示普通提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.success('恭喜你，这是一条成功消息')"
>
  显示成功提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.warning('警告你哦，这是一条警告消息')"
>
  显示警告提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.error('message不想说话，并且向你抛出了一个异常')"
>
  显示错误提示
</button>
```

### 带背景色

- 设置属性 `background` 为 `true` 后，通知提示会显示背景色。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.info('这是一条普通的提示',{ background:true })"
>
  显示普通提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.success('恭喜你，这是一条成功消息',{ background:true })"
>
  显示成功提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.warning('警告你哦，这是一条警告消息',{ background:true })"
>
  显示警告提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.error('message不想说话，并且向你抛出了一个异常',{ background:true })"
>
  显示错误提示
</button>
```

### 加载中

- Loading 的状态，并异步在某个时机移除。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.loading('正在加载中...')"
>
  显示加载中
</button>
```

### 修改延时

- 自定义时长 10s，也可以在\$.Message.config()中全局配置，详见 API。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.success('这是成功的提示信息，我将在10秒内消失',{ duration:10 })"
>
  显示10秒提示
</button>
```

### 可关闭

- 设置参数 `closable` 为 `true` 可以手动关闭提示。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.info('可手动关闭的提示',{ closable:true })"
>
  显示可关闭提示
</button>
```

### 使用 HTML 片段

- `content` 属性支持传入 HTML 片段

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.info('<strong>这是 <i>HTML</i> 片段</strong>')"
>
  显示HTML 片段
</button>
```

### 显示顺序消息

- 可以通过内置方法 `afterClose()` 在关闭后运行 callback 。以下用例将在每个 message 将要结束时通过内置方法 `afterClose()` 显示新的 message ，可以嵌套使用。

```html
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.loading({
    content:' 正在加载中...',
    afterClose() {
        $.Message.success('加载成功',{ 
            afterClose() {
                $.Message.info('这是加载成功后的提示')
            }
      })
    }
 })"
>
  显示顺序消息
</button>
```

## API

组件提供了一些静态方法，通过直接调用以下方法来使用组件：

- `$.Message.info(content,config)`
- `$.Message.success(content,config)`
- `$.Message.warning(content,config)`
- `$.Message.error(content,config)`
- `$.Message.loading(content,config)`

以上方法隐式的创建及维护 Message 组件。

| 属性       | 说明                                   | 类型            | 默认值 |
| ---------- | -------------------------------------- | --------------- | ------ |
| duration   | 自动关闭的延时，单位秒，不关闭可以写 0 | Number          | 1.5    |
| onClose    | 手动点击关闭按钮时的回调               | Function        | -      |
| afterClose | message 自动关闭时的回调               | Boolean         | false  |
| closable   | 是否显示关闭按钮                       | Boolean         | false  |
| background | 是否显示背景色                         | Boolean         | false  |
| key        | 当前 message 的唯一标志                | String / Number | 时间戳 |

### 全局方法

- 还提供了全局配置和全局销毁方法：

- `$.Message.config(config)`
- `$.Message.destroy(key,callback)`

- 需要注意的是全局销毁方法在未使用 Message 实例化方法将组件实例化前直接使用销毁方法是无效的，由于它的参数 `key` 得到的值应在对应组件实例化后才获取得到，所以应在对应组件实例化之后再调用全局销毁方法

### _\$.Message.config_

```js
$.Message.config({
  top: 100,
  duration: 6,
});
```

| 属性     | 说明                             | 类型   | 默认值 |
| -------- | -------------------------------- | ------ | ------ |
| top      | 提示组件距离顶端的距离，单位像素 | Number | 12     |
| duration | 默认自动关闭的延时，单位秒       | Number | 3      |

### _\$.Message.destroy(key)_

通过 `$.Message.destroy(key)` 来关闭一条消息。这里以 Ajax 请求为例

```html
<button class="jv-btn jv-btn-primary" id="updata">更新内容</button>
<script>
  $(() => {
    const _updata = () => {
      //在ajax请求未有响应结果返回前一直显示加载中
      $.Message.loading("正在更新数据...", {
        duration: 0,
        key: "updatable",
      });

      $.ajax({
        url: "/api",
        type: "get",
        //some codes...
        success() {
          //收到返回结果后将loading移除
          $.Message.destroy("updatable", () => {
            $.Message.success("更新完成");
          });
          //do some things...
        },
      });
    };

    $("#updata").click(() => {
      _updata();
    });
  });
</script>
```

| 参数     | 说明                    | 类型            | 默认值 |
| -------- | ----------------------- | --------------- | ------ |
| key      | 当前 message 的唯一标志 | String / Number | -      |
| callback | 组件销毁后的回调        | Function        | -      |

## `MessageBox 弹框`

## 概述

模拟系统的消息提示框而实现的一套模态对话框组件，用于消息提示、确认消息和提交内容。并且在全局创建一个对话框，并在消失时移除，所以同时只能操作一个对话框。

## 代码示例

### 基本用法

- 四种基本的对话框，只提供一个确定按钮。

```html
<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.info({
    title:'对话框标题',
    content:'<p>一些对话框内容</p><p>一些对话框内容</p>'
})"
>
  消息
</button>

<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.success({
    title:'对话框标题',
    content:'<p>一些对话框内容</p><p>一些对话框内容</p>'
})"
>
  成功
</button>

<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.warning({
    title:'对话框标题',
    content:'<p>一些对话框内容</p><p>一些对话框内容</p>'
})"
>
  警告
</button>

<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.error({
    title:'对话框标题',
    content:'<p>一些对话框内容</p><p>一些对话框内容</p>'
})"
>
  错误
</button>
```

### 确认对话框

- 快速弹出确认对话框，并且可以自定义按钮文字及异步关闭。

```html
<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.confirm({
            title:'确认对话框标题',
            content:'<p>一些对话框内容</p><p>一些对话框内容</p>'
})"
>
  确认
</button>

<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.confirm({
    title:'确认对话框标题',
    content:'<p>一些对话框内容</p><p>一些对话框内容</p>',
    okText:'知道了',
    cancelText:'不理'
})"
>
  自定义按钮文字
</button>

<button class="jv-btn jv-btn-default" id="asyn">异步关闭</button>

<script>
  $(() => {
    $("#asyn").click(() => {
      $.MessageBox.confirm({
        title: "确认对话框标题",
        content: "<p>对话框将在 2秒 后关闭</p>",
        loading: true,
        ok() {
          setTimeout(() => {
            $.MessageBox.remove();
          }, 2000);
        },
      });
    });
  });
</script>
```

### 自定义内容

- `title`和`content`属性可以传入 HTML 代码片段进行自定义内容

```js
let title = `<span>账号登录</span>`;
let input = `<input type="text" class="jv-input" placeholder="请输入账户">`;
$.MessageBox.info({
  title: title,
  content: input,
});
```

## API

通过直接调用以下方法来使用：

- \$.MessageBox.info(config)
- \$.MessageBox.success(config)
- \$.MessageBox.warning(config)
- \$.MessageBox.error(config)
- \$.MessageBox.confirm(config)

以上方法隐式地创建及维护 MessageBox 组件。参数 config 为对象，具体说明如下：

| 属性       | 说明                                                                                                | 类型                    | 默认值 |
| ---------- | --------------------------------------------------------------------------------------------------- | ----------------------- | ------ |
| title      | 提示标题                                                                                            | String / Element String | -      |
| content    | 提示内容                                                                                            | String / Element String | -      |
| width      | 自定义消息框宽度，单位 px                                                                           | Number / String         | 416    |
| okText     | 确定按钮的文字                                                                                      | String                  | 确定   |
| cancelText | 取消按钮的文字，只在 `$.MessageBox.confirm()`下有效                                                 | String                  | 取消   |
| loading    | 点击确定按钮时，确定按钮是否显示 loading 状态，开启则需手动调用 `$.MessageBox.remove()`来关闭对话框 | Boolean                 | false  |
| scrollable | 页面是否可以滚动                                                                                    | Boolean                 | false  |
| closable   | 是否可以按 Esc 键关闭                                                                               | Boolean                 | false  |
| ok         | 点击确定的回调                                                                                      | Function                | -      |
| cancel     | 点击取消的回调，只在 `$.MessageBox.confirm()`下有效                                                 | Function                | -      |

|

还提供了全局关闭对话框的方法：

- \$.MessageBox.remove()

以 Ajax 请求为例

```js
$.MessageBox.confirm({
  title: "...",
  content: "...",
  loading: true,
  ok() {
    $.ajax({
      url: "/api/someurl",
      type: "post",
      data: userName,
      //....
      success: () => {
        $.MessageBox.remove();
      },
      error: () => {
        $.MessageBox.error({
          title: "出错了",
          content: "提交内容失败",
        });
        $.MessageBox.remove();
      },
    });
  },
});
```

## `Modal 对话框`

## 概述

在保留当前页面状态的情况下，告知用户并承载相关操作。

## 代码示例

### 基础用法

- 最简单的使用方法。

- 通过配合 \$Modal.toggle() 方法来控制对话框显示或隐藏，不可嵌套。

- 默认按键盘 ESC 键也可以关闭。

```html
<button id="exampleModal1">显示对话框</button>
<j-modal
  id="modal1"
  title="Modal对话框标题"
  data-ok-text="确定"
  data-cancel-text="取消"
>
  <p>这是对话框的信息</p>
  <p>这是对话框的信息</p>
  <p>这是对话框的信息</p>
</j-modal>
<script>
  $(() => {
    $Modal.toggle({
      trigger: "#exampleModal1",
      target: "#modal1",
      ok() {
        //点击确定按钮的回调事件
      },
      cancel() {
        //点击取消按钮的回调事件
      },
    });
  });
</script>
```

### 首屏显示

- 通过设置属性 data-hidden="false" 直接显示对话框

```html
<j-modal
  id="modal"
  title="Modal对话框标题"
  data-hidden="false"
  data-ok-text="确定"
  data-cancel-text="取消"
>
  <p>这是对话框的信息</p>
  <p>这是对话框的信息</p>
  <p>这是对话框的信息</p>
</j-modal>
```

### 自定义位置、宽度

- 通过设置属性 data-width 设置对话框宽度。单位无限制，但必须有

- 通过设置属性 data-position 设置对话框位置。单位无需添加，默认设定为 px

```html
<j-modal
  id="modal4"
  title="自定义宽度"
  data-width="300px"
  data-ok-text="确定"
  data-cancel-text="取消"
>
  <p>
    自定义宽度，单位无限制但必须有，默认
    520px。对话框的宽度是响应式的，当屏幕尺寸小于 768px 时，宽度会变为自动auto。
  </p>
</j-modal>
<j-modal
  id="modal5"
  title="自定义位置"
  data-position="[320,320]"
  data-ok-text="确定 "
  data-cancel-text="取消 "
>
  <p>我的位置可以出现在屏幕的任意一个地方哦！</p>
</j-modal>
<j-modal
  id="modal6"
  title="对话框标题"
  data-position="[280]"
  data-ok-text="确定"
  data-cancel-text="取消"
>
  <p>看！我现在处于屏幕中心了</p>
</j-modal>
```

### 限制内容区域的高度

- 通过设置属性 data-max-height 限制对话框内容区域的高度，默认为'none'。单位无限制，但必须有

```html
<j-modal
  id="modal14"
  title="对话框标题"
  data-max-height="400px"
  data-ok-text="确定 "
  data-cancel-text="取消 "
>
  <p>这是对话框的信息</p>
  <p>这是对话框的信息</p>
  <p>这是对话框的信息</p>
</j-modal>
```

### 不带标题栏和底部栏

- 将 title 属性去除则不带标题栏

- 通过设置属性 data-footer-hide="true" 不显示底部按钮栏

```html
<j-modal id="modal2" data-ok-text="确定" data-cancel-text="取消">
  <p>我头去哪了？</p>
  <p>我头去哪了？</p>
  <p>我头去哪了？</p>
</j-modal>
<j-modal
  id="modal3"
  title="不带按钮栏"
  data-footer-hide="true"
  data-ok-text="确定"
  data-cancel-text="取消"
>
  <p>我的按钮栏不见了，你有看到它吗？</p>
  <p>我的按钮栏不见了，你有看到它吗？</p>
  <p>我的按钮栏不见了，你有看到它吗？</p>
</j-modal>
```

### 禁用关闭

- 通过设置属性 data-closable="false" 可以禁用关闭(含 Esc 键)。

- 通过设置属性 data-mask-closable="false" 可以禁用遮罩层关闭。

```html
<j-modal
  id="modal7"
  title="对话框标题 "
  data-closable="false"
  data-ok-text="确定"
  data-cancel-text="取消 "
>
  <p>看！我右上角的关闭按钮不见了哟，你关闭不了我了（含ESC键）。</p>
</j-modal>
<j-modal
  id="modal8"
  title="禁用遮盖层"
  data-mask-closable="false"
  data-ok-text="确定"
  data-cancel-text="取消 "
>
  <p>┭┮﹏┭┮，遮盖层已被禁用关闭，你点不了了</p>
</j-modal>
```

### 全屏

设置属性 data-fullscreen="true" 可以全屏显示。

```html
<j-modal
  id="modal9"
  title="全屏对话框"
  data-fullscreen="true"
  data-ok-text="确定"
  data-cancel-text="取消"
>
  <p>我膨胀了，我变全屏了</p>
  <p>我膨胀了，我变全屏了</p>
  <p>我膨胀了，我变全屏了</p>
</j-modal>
```

### 隐藏遮盖层

- 通过设置属性 data-mask="false" 可以隐藏遮盖层

```html
<j-modal
  id="modal10"
  title="全屏对话框"
  data-mask="false"
  data-ok-text="确定"
  data-cancel-text="取消"
>
  <p>我膨胀了，我变全屏了</p>
  <p>我膨胀了，我变全屏了</p>
  <p>我膨胀了，我变全屏了</p>
</j-modal>
```

### 自定义内容

```html
<j-modal
  id="modal11"
  data-mask-closable="false"
  data-ok-text="确定"
  data-cancel-text="取消"
>
  <div>
    <p>邮箱：<input type="text" placeholder="请输入邮箱" /></p>
    <p>内容：<input type="text" placeholder="发送的内容" /></p>
  </div>
</j-modal>
```

## API

通过调用以下方法来控制 modal 显示或隐藏，以及事件的回调

- \$Modal.toggle(config)

| 参数    | 说明                                      | 类型     | 默认值 |
| ------- | ----------------------------------------- | -------- | ------ |
| trigger | 通过点击某个元素触发 modal 显示或隐藏     | String   | —      |
| target  | 触发的目标 modal 为标签上设置的 id 选择器 | String   | —      |
| ok      | 点击确定的回调                            | Function | —      |
| cancel  | 点击取消的回调                            | Function | —      |

Modal
| 属性 | 说明 | 类型 | 默认值 |
| ---- | ---- | ---- | --- |
| title | 对话框标题，如果不添加该属性则不显示 modal 页头 | String | — |
| data-hidden | 是否直接在打开页面时显示对话框 |Boolean | true |
| data-mask-closable | 是否允许点击遮罩层关闭 |Boolean | true |
| data-fullscreen | 是否全屏显示 |Boolean | false |
| data-footer-hide | 不显示底部 |Boolean | false |
| data-mask | 是否显示遮罩层 |Boolean | true |
| data-ok-text | 确定按钮文字 |String | 确定 |
| data-cancel-text | 取消按钮文字 |String | 取消 |
| id | 为对话框容器.jv-modal-wrap 的类名设置 id 选择器，可辅助实现垂直居中等自定义效果 |String | - |
| data-width | 对话框宽度，对话框的宽度是响应式的，当屏幕尺寸小于 768px 时，宽度会变为自动 auto。单位无限制，但必须有 |String| 520px |
| data-max-height |限制对话框内容最大高度，超出部分则滚动条显示。单位无限制，但必须有 |String| none |
| data-position | 对话框位置。值对应的位置 [top,left]。如果数组中只有一个值则默认为 top 值 [100] ，如果仅需设置 left 值需这样写 [0,50]。无需添加单位，默认设定为 px |Array| [20,0] |
| data-z-index| 层级 |Number | 1000 |

## `Notice 通知提醒`

## 概述

在界面右上角显示可关闭的全局通知，常用于以下场景：

- 通知内容带有描述信息

- 系统主动推送

## 代码示例

### 基础用法

- 最简单的用法，4 秒后自动关闭。如果 desc 参数为空或不填，则自动应用仅标题模式下的样式。

- 建议标题言简意赅，例如"删除成功"，更详细的内容可以放在描述信息里。

```js
$.Notice.open({ title: "这是通知标题" });
$.Notice.open({
  title: "这是通知标题",
  desc:
    "This is the content of the notification. This is the content of the notification. ",
});
```

### 带描述信息

- 有四种类型：`info、success、warning、error`

```js
$.Notice.info({
  title: "这是通知标题",
  desc:
    "This is the content of the notification. This is the content of the notification. ",
});
$.Notice.success({
  title: "这是通知标题",
  desc:
    "This is the content of the notification. This is the content of the notification. ",
});
$.Notice.warning({
  title: "这是通知标题",
  desc:
    "This is the content of the notification. This is the content of the notification. ",
});
$.Notice.error({
  title: "这是通知标题",
  desc:
    "This is the content of the notification. This is the content of the notification. ",
});
```

### 自定义时长

- 自定义时长，为 `0` 则不自动关闭。也可以在 `Notice.config()` 中全局配置，详见 API。

```js
$.Notice.open({
  title: "这是通知标题",
  desc: "这条通知不会自动关闭，需要点击关闭按钮才可以关闭。",
  duration: 0,
});
```

### 自定义弹出位置

使用 `placement` 属性定义通知的弹出位置，通知从右上角、右下角、左下角、左上角弹出。

```js
$.Notice.open({
             title:'自定义位置',
             desc:'右上角弹出的消息',
        });
$.Notice.open({
             title:'自定义位置',
             desc:'右下角弹出的消息',
             placement：'bottom-right'
        });
$.Notice.open({
             title:'自定义位置',
             desc:'左下角弹出的消息',
             placement：'bottom-left'
        });
$.Notice.open({
             title:'自定义位置',
             desc:'左上角弹出的消息',
             placement：'top-left'
        });
```

### 使用 HTML 片段

- `desc` 属性支持传入 HTML 片段

```js
$.Notice.info({
  title: "HTML 片段",
  desc: "<strong>这是 <i>HTML</i> 片段</strong>",
});
```

## API

通过直接调用以下方法来使用组件：

- \$.Notice.open(config)
- \$.Notice.info(config)
- \$.Notice.success(config)
- \$.Notice.warning(config)
- \$.Notice.error(config)

以上方法隐式地创建及维护 Notice 组件。参数 config 为对象，具体说明如下：

| 属性      | 说明                                                               | 类型     | 默认值    |
| --------- | ------------------------------------------------------------------ | -------- | --------- |
| title     | 通知提醒的标题                                                     | String   | -         |
| desc      | 通知提醒的内容，为空或不填时，自动应用仅标题模式下的样式           | String   | -         |
| duration  | 自动关闭的延时，单位秒，不关闭可以写 0                             | Number   | 4         |
| key       | 当前通知的唯一标识                                                 | String   | 自动      |
| onClose   | 点击通知关闭按钮时的回调                                           | Function | -         |
| onClick   | 点击通知时触发的回调函数                                           | Function | -         |
| placement | 弹出位置，可选 `top-left` `top-right` `bottom-left` `bottom-right` | string   | top-right |

还提供了提供了全局配置、全局关闭某个通知和全局销毁的方法：

- \$.Notice.config(options)
- \$.Notice.close(key,callback)
- \$.Notice.destroy()

\$.Notice.config 示例

```js
$.Notice.config({
  top: 50,
  duration: 3,
});
```

\$.Notice.close 示例

```js
$.Notice.open({
    title:'这是一条永远不会自动关闭的通知',
    desc:'我永远不会自动关闭。这是一个故意的很长的描述，有很多很多的字和词。',
    duration: 0，
    key: 'demoKey'
});

setTimeout(() => {
    $.Notice.close('demoKey',()=> {
         alert("我通知关闭后的回调事件");
    });
}, 3000);
```

\$.Notice.destroy

- 通过调用这个方法会全局关闭页面所有的通知

| 属性     | 说明                             | 类型   | 默认值 |
| -------- | -------------------------------- | ------ | ------ |
| top      | 通知组件距离顶端的距离，单位像素 | Number | 24     |
| duration | 默认自动关闭的延时，单位秒       | Number | 4      |

## `PageHeader 页头`

## 概述

如果页面的路径比较简单，推荐使用页头组件而非面包屑组件。

## 代码示例

### 基础用法

- 标准页头，适合使用在需要简单描述的场景

```html
<j-page-header title="返回" content="详情页面"></j-page-header>
```

- 如果内容过长或较复杂建议使用指定的 `slot` 标签代替

```html
<j-page-header>
  <slot name="title">...</slot>
  <slot name="content">...</slot>
</j-page-header>
```

## Attributes

| 属性    | 说明 | 类型   | 默认值 |
| ------- | ---- | ------ | ------ |
| title   | 标题 | string | —      |
| content | 内容 | string | —      |

|

## Slot

| 名称    | 说明                              |
| ------- | --------------------------------- |
| title   | 标题内容，会覆盖 title 属性的内容 |
| content | 内容，会覆盖 content 属性的内容   |

|

## Events

| 事件名称 | 说明             | 默认展 |
| -------- | ---------------- | ------ |
| on-back  | 点击左侧区域触发 | -      |

|

```html
<j-page-header
  on-back="demoBack"
  title="返回"
  content="详情页面"
></j-page-header>
<script>
  $(() => {
    demoBack = function () {
      //....some codes
    };
  });
</script>
```

## `Popconfirm气泡确认框`

## 概述

目标元素的操作需要用户进一步的确认时，在目标元素附近弹出浮层提示，询问用户。

和 confirm 弹出的全屏居中模态对话框相比，交互形式更轻量。

## 代码示例

### 基础用法

- Popconfirm 的属性与 Popover 很类似，因此对于重复属性，请参考 Popover 的文档，在此文档中不做详尽解释。

- 最简单的用法。

```html
<j-popconfirm
  title="您确认删除这条内容吗？"
  onConfirm="ok()"
  onCancel="cancel()"
>
  <a href="#">删除</a>
</j-popconfirm>
```

### 自定义按钮文字

- 设置属性 `ok-text` 和 `cancel-text` 自定义按钮文字。

```html
<j-popconfirm
  title="Are you sure delete this task?"
  ok-text="Yes"
  cancel-text="No"
  onConfirm="ok()"
  onCancel="cancel()"
>
  <a href="#">Delete</a>
</j-popconfirm>
```

## Attributes

| 属性        | 说明           | 类型     | 默认值 |
| ----------- | -------------- | -------- | ------ |
| title       | 确认框的描述   | String   | —      |
| ok-text     | 标题           | String   | —      |
| cancel-text | 标题           | String   | —      |
| onConfirm   | 点击确认的回调 | Function | —      |
| onCancel    | 点击取消的回调 | Function | —      |

|

## Slot

| 名称  | 说明                                          |
| ----- | --------------------------------------------- |
| title | 提示框标题，定义此 slot 时，会覆盖 属性 title |

|

## `Popover气泡卡片`

## 概述

当目标元素有进一步的描述和相关操作时，可以收纳到卡片中，根据用户的操作行为进行展现。
和 Tooltip 的区别是，用户可以对浮层上的元素进行操作，因此它可以承载更复杂的内容，比如链接或按钮等。

## 代码示例

### 基础用法

- 支持三种触发方式：鼠标悬停、点击、聚焦。默认是点击。

- 注意 Poptip 内的文本使用了 white-space: nowrap;，即不自动换行，如需展示很多内容并自动换行时，建议使用指定的 slot 标签

```html
<j-popover title="标题" content="<p>提示内容</p><p>提示内容</p>" placement="top-left" trigger="hover">
    <button class="jv-btn jv-btn-default">hover 激活</button>
</j-popover>
<j-popover title="标题" content="<p>提示内容</p><p>提示内容</p>">
    <button class="jv-btn jv-btn-default">click 激活</button>
</j-popover>
<j-popover title="标题" content="<p>提示内容</p><p>提示内容</p>" placement="top-right" trigger="focus">
    <button class="jv-btn jv-btn-default">focus 激活</button>
</j-popover>
<br><br>
<j-popover title="标题">
    <input type="text" class="jv-input" placeholder="输入框的 focus"></input>
    <slot name="content">
        <div class="t">
            <span style="color: #ccc;">输入些内容叭</span>
        </div>
    </slot>
</j-popover>

<script>
    $('input').bind("input propertychange", function(event) {
        $('.t').text($(this).val())
    });
</script>
```

### 位置

- 组件提供了 12 个不同的方向显示 Poptip，具体配置可查看 API。

```html
<j-popover
  title="标题"
  content="<p>提示内容</p><p>提示内容</p>"
  placement="top-left"
>
  <button class="jv-btn jv-btn-default">hover 激活</button>
</j-popover>
<j-popover title="标题" content="<p>提示内容</p><p>提示内容</p>">
  <button class="jv-btn jv-btn-default">hover 激活</button>
</j-popover>
<j-popover
  title="标题"
  content="<p>提示内容</p><p>提示内容</p>"
  placement="top-right"
>
  <button class="jv-btn jv-btn-default">hover 激活</button>
</j-popover>
....
```

### 从浮层内关闭

- 通过给 popover 添加 id 选择器来控制提示框的显示和隐藏。

```html
<j-popover title="标题" id="demoPopover">
  <button class="jv-btn jv-btn-primary">click激活</button>
  <slot name="content"><a href="#" class="close">关闭提示框</a></slot>
</j-popover>

<script>
  $(".close").click(() => {
    $("#demoPopover").hidePopover();
  });
</script>
```

### 嵌套复杂内容

- 通过使用指定的 slot 来实现复杂的内容。

```html
<j-popover id="demoPopover" placement="right">
  <button class="jv-btn jv-btn-default">click激活</button>
  <slot name="content">
    <div
      style="width: 350px;height: 150px;text-align: center;position: relative;"
    >
      <div
        style="width: 100%;height: 50%;line-height: 70px; background-color: skyblue;"
      >
        上部分
      </div>
      <div
        style="position: absolute;width: 50px;height: 50px;border-radius: 50%;background-color: goldenrod;top: 50%;left: 50%;transform: translate(-50%,-50%);"
      ></div>
      <div
        style="width: 100%;height: 50%;line-height: 70px; background-color:#eee"
      >
        下部分
      </div>
    </div>
  </slot>
</j-popover>
```

## Attributes

| 属性      | 说明                                                                                                                                     | 类型   | 默认值 |
| --------- | ---------------------------------------------------------------------------------------------------------------------------------------- | ------ | ------ |
| title     | 标题                                                                                                                                     | String | —      |
| content   | 内容                                                                                                                                     | String | —      |
| trigger   | 触发方式，可选值为 hover（悬停）click（点击）focus（聚焦）                                                                               | String | click  |
| placement | 提示框出现的位置，可选值为 top top-left top-right left-top left left-bottom bottom bottom-left bottom-right right right-top right-bottom | String | click  |

|

## Event

- 控制 popover 隐藏或显示的方法，注意！必须为 popover 添加 id 选择器才能使用

```js
$(idSelector).showPopover();
$(idSelector).hidePopover();
```

## Slot

| 名称    | 说明                                            |
| ------- | ----------------------------------------------- |
| title   | 提示框标题，定义此 slot 时，会覆盖 属性 title   |
| content | 提示框内容，定义此 slot 时，会覆盖 属性 content |

|

## `Progress 进度条`

## 概述

在操作需要较长时间才能完成时，为用户显示该操作的当前进度和状态。

当需要显示一个操作完成的百分比时。

## 代码示例

### 基本用法

- 处在不同状态下的进度条

```html
<div class="jv-progress">
  <div class="jv-progress-bar" role="progressbar" style="width: 25%;"></div>
</div>
<div class="jv-progress">
  <div
    class="jv-progress-bar info"
    role="progressbar"
    style="width: 45%;"
  ></div>
</div>
<div class="jv-progress">
  <div
    class="jv-progress-bar success"
    role="progressbar"
    style="width: 100%;"
  ></div>
</div>
<div class="jv-progress">
  <div
    class="jv-progress-bar warning"
    role="progressbar"
    style="width: 75%;"
  ></div>
</div>
<div class="jv-progress">
  <div
    class="jv-progress-bar error"
    role="progressbar"
    style="width: 55%;"
  ></div>
</div>
```

### 显示进度文字

```html
<div class="jv-progress">
  <div class="jv-progress-bar" role="progressbar" style="width: 40%;">40%</div>
</div>
<div class="jv-progress">
  <div class="jv-progress-bar success" role="progressbar" style="width: 100%;">
    成功
  </div>
</div>
```

### 渐变色

- 自定义背景色

```html
<div class="jv-progress">
  <div
    class="jv-progress-bar success"
    role="progressbar"
    style="width: 80%; background-image: linear-gradie(to right, rgb(16, 142, 233) 0%, rgb(135, 208, 104) 100%);"
  ></div>
</div>
```

### 分段进度条

- 标准的进度条。

```html
<div class="jv-progress">
  <div class="jv-progress-bar" role="progressbar" style="width: 15%;"></div>
  <div
    class="jv-progress-bar success"
    role="progressbar"
    style="width: 30%;"
  ></div>
  <div
    class="jv-progress-bar info"
    role="progressbar"
    style="width: 20%;"
  ></div>
</div>
```

### 动画效果

- 添加 class `active` 显示进度中动画

```html
<div class="jv-progress">
  <div
    class="jv-progress-bar active"
    role="progressbar"
    style="width: 50%;"
  ></div>
</div>
```

## `Result 结果`

## 概述

用于反馈一系列操作任务的处理结果。

## 代码示例

### 基础用法

- 最基本的使用方法，设置属性 `title` 设置 result 的主标题，`subTitle` 设置 result 的副标题，如果内容较长或复杂可以使用 slot 标签代替

```html
<j-result title="您已提交过申请了" subTitle="请勿重复提交申请！">
  <slot name="footer">
    <button class="jv-btn jv-btn-primary">知道了</button>
  </slot>
</j-result>
```

- 不同状态

- 提供 7 中不同的状态 `success`、`info`、`warning`、`error`、`403`、`404`、`500`，默认为 `info`

```html
<j-result status="success" title="这是标题" subTitle="这是副标题"></j-result>
<j-result title="这是标题" subTitle="这是副标题"></j-result>
<j-result status="warning" title="这是标题" subTitle="这是副标题"></j-result>
<j-result status="error" title="这是标题" subTitle="这是副标题"></j-result>
<j-result status="403" title="这是标题" subTitle="这是副标题"></j-result>
<j-result status="404" title="这是标题" subTitle="这是副标题"></j-result>
<j-result status="500" title="这是标题" subTitle="这是副标题"></j-result>
```

### 自定义图标

- 设置属性 `icon` 可以自定义图标，或者使用指定的 slot 标签自定义图标

```html
<j-result
  title="这是标题"
  icon="<i class='jv-icon icon-emo-happy'></i>"
></j-result>
<!--或者-->
<j-result title="这是标题">
  <slot name="icon"><i class="jv-icon icon-emo-happy"></i></slot>
</j-result>
```

## Attributes

| 属性     | 说明                                                                                           | 类型   | 默认值 |
| -------- | ---------------------------------------------------------------------------------------------- | ------ | ------ |
| title    | 主标题文字                                                                                     | String | -      |
| subTitle | 副标题文字                                                                                     | String | -      |
| status   | 结果的状态，决定图标和颜色，提供 7 中类型 `info` `success` `warning` `error` `403` `404` `500` | String | info   |
| icon     | 自定义图标                                                                                     | String | -      |
| footer   | 底部栏，一般用于放置操作按钮                                                                   | String | -      |

|

## Slot

| 名称     | 说明                                     |
| -------- | ---------------------------------------- |
| icon     | 自定义图标，会覆盖属性 `icon` 的内容     |
| title    | 主标题文字，会覆盖属性 `title` 的内容    |
| footer   | 底部栏，会覆盖属性 `footer` 的内容       |
| subTitle | 副标题文字，会覆盖属性 `subTitle` 的内容 |

|

### 演示

- 在合适的时机显示出来

- 将 result 标签放置在一个用于显示隐藏的容器里，并通过代码控制这个容器显示隐藏

```html
<style>
  #demo {
    display: none;
  }
</style>

<div id="demo">
  <j-result
    status="success"
    title="获得超级VIP会员服务100年！"
    subTitle="订单号：202008311500551811，VIP炒鸡会将在一秒钟后生效，祝您享受服务愉快！"
  >
    <slot name="footer">
      <button class="jv-btn jv-btn-primary">立即体验</button>
      <button class="jv-btn jv-btn-default">分享</button>
    </slot>
  </j-result>
</div>

<button class="jv-btn jv-btn-primary" data-toggle="display" data-target="#demo">
  立即申请
</button>
```

## `Skeleton骨架屏`

## 概述

在需要等待加载内容的位置提供一个占位图形组合。

## 代码示例

### 基础用法

- 最简单的占位效果。

- 设置属性 `data-title-width` 和 `data-paragraph-width` 可以设置标题占位图的宽度和段落占位图的宽度，段落占位图宽度设置接收一个数组，其值对应的每行宽度。

- 若不添加 `data-paragraph-width` 属性则段落占位图宽度全部为 100%

```html
<j-skeleton data-title-width="38%" data-paragraph-width="[0,0,61]"></j-skeleton>
```

### 复杂的组合

- 更复杂的组合。设置属性 `data-show-avatar="true"` 显示头像占位图，设置属性 `data-paragraph-row` 设置段落占位图的行数。

```html
<j-skeleton
  data-show-avatar="true"
  data-title-width="50%"
  data-paragraph-row="4"
  data-paragraph-width="[0,0,61]"
>
</j-skeleton>
```

### 动画效果

- 设置属性 `data-active="true"` 显示动画效果。

```html
<j-skeleton
  data-active="true"
  data-title-width="38%"
  data-paragraph-width="[0,0,61]"
></j-skeleton>
<j-skeleton
  data-active="true"
  data-show-avatar="true"
  data-title-width="38%"
  data-paragraph-row="2"
></j-skeleton>
```

## Attributes

| 属性                 | 说明                                                                                                                               | 类型    | 默认值 |
| -------------------- | ---------------------------------------------------------------------------------------------------------------------------------- | ------- | ------ |
| data-active          | 是否展示动画效果                                                                                                                   | Boolean | false  |
| data-show-avatar     | 是否显示头像占位图                                                                                                                 | Boolean | false  |
| data-show-paragraph  | 是否显示段落占位图                                                                                                                 | Boolean | true   |
| data-show-title      | 是否显示标题占位图                                                                                                                 | Boolean | true   |
| data-title-width     | 设置标题占位图的宽度，需要自行添加像素单位                                                                                         | String  | -      |
| data-paragraph-row   | 设置段落占位图的行数                                                                                                               | Number  | 3      |
| data-paragraph-width | 设置段落占位图的宽度，单位默认为百分比无需添加单位。数组的值对应的每行宽度，如果数组中的某个值是 0 则自动设置对应行的宽度为 100%。 | Array   | 100    |

### 占位图使用示例

- 简单的示例在实际中的应用

```html
<section style="border: 1px dashed #ccc; padding: 10px;">
  <div class="demo-content-box">
    <div class="skeleton-loading">
      <j-skeleton
        data-active="true"
        data-title-width="28%"
        data-paragraph-width="[0,0,50]"
      >
      </j-skeleton>
    </div>
    <div class="content" style="display: none;">
      <h3>标题</h3>
      <div style="color: dodgerblue; margin: 15px 0;">
        这是一段模拟等待从服务器加载完成数据并渲染到页面的内容消息，这是一段等待从服务器加载完成数据并渲染到页面的内容消息。
      </div>
      <button class="jv-btn jv-btn-default" id="demo-btn">显示骨架图</button>
    </div>
  </div>
</section>

<script>
  $(() => {
    //模拟第一次进入页面时区域内容数据正在加载，并显示骨架图
    setTimeout(() => {
      $(".content").fadeIn();
      $(".skeleton-loading").hide();
    }, 3000);
    //点击按钮显示骨架图
    $("#demo-btn").click(() => {
      $(".content").hide();
      $(".skeleton-loading").show();

      setTimeout(() => {
        $(".content").fadeIn();
        $(".skeleton-loading").hide();
      }, 4000);
    });
  });
</script>
```

## `Spin 加载中`

## 概述

当区块正在获取数据中时可使用，适当的等待动画可以提升用户体验。

## 代码示例

### 基础用法

- 最简单使用 Spin 的方法。

```html
<div class="jv-spin jv-spin-primary" role="status">
  <span class="jv-spin-dot"></span>
</div>
```

### 圆环型

- 默认居中固定

```html
<div class="jv-spin jv-spin-primary" role="status">
  <div class="jv-spin-border ">
    <svg viewBox="25 25 50 50" class="circular">
      <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
    </svg>
  </div>
</div>
```

### 居中固定

- 点状外观的 spin 在容器内部垂直居中固定，需要父级有 relative。

```html
<div class="jv-spin jv-spin-primary jv-spin-fix" role="status">
  <div class="jv-spin-inner">
    <span class="jv-spin-dot"></span>
  </div>
</div>
```

### 各种尺寸

- 通过添加 class 类名 `jv-spin-small`, `jv-spin-large` 将 Spin 设置为大和小尺寸，不设置为默认（中）尺寸。

```html
<div class="jv-spin jv-spin-primary  jv-spin-small" role="status">
  <span class="jv-spin-dot"></span>
</div>
<div class="jv-spin jv-spin-primary" role="status">
  <span class="jv-spin-dot"></span>
</div>
<div class="jv-spin jv-spin-primary  jv-spin-large" role="status">
  <span class="jv-spin-dot"></span>
</div>
```

### 不同状态

- 不同的颜色状态

```html
<div class="jv-spin jv-spin-primary" role="status">
  <span class="jv-spin-dot"></span>
</div>

<div class="jv-spin jv-spin-success" role="status">
  <span class="jv-spin-dot"></span>
</div>

<div class="jv-spin jv-spin-warning" role="status">
  <span class="jv-spin-dot"></span>
</div>

<div class="jv-spin jv-spin-error" role="status">
  <span class="jv-spin-dot"></span>
</div>

<div class="jv-spin jv-spin-primary" role="status">
  <div class="jv-spin-border ">
    <svg viewBox="25 25 50 50" class="circular">
      <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
    </svg>
  </div>
</div>

<div class="jv-spin jv-spin-success" role="status">
  <div class="jv-spin-border ">
    <svg viewBox="25 25 50 50" class="circular">
      <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
    </svg>
  </div>
</div>

<div class="jv-spin jv-spin-warning" role="status">
  <div class="jv-spin-border ">
    <svg viewBox="25 25 50 50" class="circular">
      <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
    </svg>
  </div>
</div>

<div class="jv-spin jv-spin-error" role="status">
  <div class="jv-spin-border ">
    <svg viewBox="25 25 50 50" class="circular">
      <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
    </svg>
  </div>
</div>
```

### 整页加载

- 使用内置的 `$.Spin` 方法可以全局加载。可设置属性 `type` 改变外观，可选值为 `dot` (默认)，`border`，通过设置属性 `color` 改变 spin 颜色，可选值为 `priamry` `success` `warning` `error`

```html
<button onclick="loading()">整页加载</button>
<script>
  $(() => {
    loading = () => {
      $.Spin.show();
      setTimeout(() => {
        $.Spin.hide();
      }, 3000);
    };
  });
</script>
```

```js
$.Spin.show(config);
```

## `Switch 开关`

## 表示两种相互对立的状态间的切换，多用于触发「开/关」

## 代码示例

### 基础用法

- 基本用法，状态切换时会触发事件。

```html
<j-switch></j-switch>
```

### 尺寸

- 通过设置属性 data-size 为 large 或 small 使用大号或小号的开关，默认为中尺寸

```html
<j-switch data-size="small"></j-switch>
<j-switch></j-switch>
<j-switch data-size="large"></j-switch>
```

### 文字描述

- 通过设置属性 data-open 和 data-close 来设置开关的文字描述。

- 如果使用 2 个汉字，建议将开关尺寸设置为 large。

- 可以直接放入图标 html 代码

```html
<j-switch data-open="开" data-close="关"></j-switch>
<j-switch
  data-open='<i class="demo-icon icon-ok"></i>'
  data-close='<i class="demo-icon icon-cancel"></i>'
></j-switch>
<j-switch data-size="large" data-open="开启" data-close="关闭"
  >j-switch>
  <j-switch data-size="large" data-open="ON" data-close="OFF"
    >j-switch></j-switch
  ></j-switch
>
```

### 选中状态

- 通过设置属性 data-actived 为 true 展示选中状态

```html
<j-switch data-actived="true"></j-switch>
```

### 禁用状态

```html
<j-switch disabled></j-switch>
```

### 自定义颜色

- 通过设置属性 data-true-color 和 data-false-color 可以自定义背景色。

```html
<j-switch data-true-color="#00c853" data-false-color="#ff4d4f"></j-switch>
```

## API

| 属性             | 说明                                                                                                 | 类型    | 默认值  |
| ---------------- | ---------------------------------------------------------------------------------------------------- | ------- | ------- |
| data-size        | 开关的尺寸，可选值为 large、small、default 或者不写。建议开关如果使用了 2 个汉字的文字，使用 large。 | String  | medium  |
| disabled         | 禁用开关                                                                                             | Boolean | false   |
| data-actived     | 是否选中状态                                                                                         | Boolean | false   |
| data-true-color  | 自定义打开时的背景色                                                                                 | String  | #2196f3 |
| data-false-color | 自定义关闭时的背景色                                                                                 | String  | #c5c8ce |
| ddata-open       | 自定义显示打开时的内容                                                                               | String  | —       |
| ddata-close      | 自定义显示关闭时的内容                                                                               | String  | —       |

## Events

- 通常需要点击 switch 来绑定一些事件，可以根据 switch 的属性 data-actived 的值 true 和 false 来做不同的事情

- 示例

```html
<j-switch class="className"></j-switch>

<script>
  $(() => {
    $(".className").click(() => {
      let flag = $(".className").data("actived"); //switch点击的状态
      if (flag) {
        alert(flag); // -> true
      } else {
        alert(flag); // -> false
      }
    });
  });
</script>
```

## `Timeline 时间线`

## 概述

对一系列信息进行时间排序时，垂直地展示。

## 代码示例

### 基础用法

- 最简单定义一个时间轴的用法。

```html
<style>
  .time {
    font-size: 14px;
    font-weight: bold;
  }
  .content {
    margin-top: 8px;
    font-size: 13px;
    color: #909399;
    line-height: 1;
  }
</style>

<j-timeline>
  <j-timeline-item>
    <p class="time">1949年</p>
    <p class="content">中华人民共和国成立</p>
  </j-timeline-item>
  <j-timeline-item>
    <p class="time">1978年</p>
    <p class="content">邓小平提出改革开放政策</p>
  </j-timeline-item>
  <j-timeline-item>
    <p class="time">2008年</p>
    <p class="content">北京成功举办奥运会</p>
  </j-timeline-item>
  <j-timeline-item>
    <p class="time">2010年</p>
    <p class="content">中国―东盟自由贸易区正式启动</p>
  </j-timeline-item>
  <j-timeline-item>
    <p class="time">2020年</p>
    <p class="content">将全面建成小康社会，实现第一个百年奋斗目标。</p>
  </j-timeline-item>
</j-timeline>
```

### 圆圈颜色

- 用各种颜色来标识不同状态，通过属性 `data-color` 可以使用`green`、`red`、`blue`、`yellow`、`lightblue`、`dark`，默认是 `blue` 。

```html
<j-timeline>
  <j-timeline-item data-color="yellow">发布1.0版本</j-timeline-item>
  <j-timeline-item data-color="lightblue">发布2.0版本</j-timeline-item>
  <j-timeline-item data-color="red">严重故障</j-timeline-item>
  <j-timeline-item data-color="blue">发布3.0版本</j-timeline-item>
  <j-timeline-item data-color="green">发布最终版本</j-timeline-item>
</j-timeline>
```

### 最后一个

- 通过设置属性 `data-pending="true"` 来标记最后一个为幽灵节点，标识还未完成。

```html
<j-timeline data-pending="true">
  <j-timeline-item>发布1.0版本</j-timeline-item>
  <j-timeline-item>发布2.0版本</j-timeline-item>
  <j-timeline-item>发布3.0版本</j-timeline-item>
  <j-timeline-item
    ><a href="#" style="color: #2d8cf0;">查看更多</a></j-timeline-item
  >
</j-timeline>
```

### 自定义时间轴点

- 通过属性 `data-dot`自定义时间轴点，可使用图标作为时间轴点

```html
<j-timeline>
  <j-timeline-item data-dot='<i class="jv-icon icon-sun-filled"></i>'
    >今日天气晴朗</j-timeline-item
  >
  <j-timeline-item data-dot='<i class="jv-icon icon-cloud-sun"></i>'
    >明天晴转多云</j-timeline-item
  >
  <j-timeline-item data-dot='<i class="jv-icon icon-rain"></i>'
    >后天大到暴雨</j-timeline-item
  >
</j-timeline>
```

## API

j-timeline

| 属性         | 说明                           | 类型    | 默认值 |
| ------------ | ------------------------------ | ------- | ------ |
| data-pending | 指定是否最后一个节点为幽灵节点 | Boolean | false  |

j-timeline-item

| 属性       | 说明                                                                            | 类型   | 默认值 |
| ---------- | ------------------------------------------------------------------------------- | ------ | ------ |
| data-color | 设置时间轴点颜色，可选值为`green`、`red`、`blue`、`yellow`、`lightblue`、`dark` | String | blue   |
| data-dot   | 自定义时间轴点内容                                                              | String | -      |

### 相关链接

- [jQuery 官方文档](https://jquery.com/)

- [ES6 入门教程](https://es6.ruanyifeng.com/)

- [码云](https://gitee.com/Ryan-Zhong/jview-ui)
