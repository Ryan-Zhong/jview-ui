# Link 文字链接

## 概述

- 展示不同状态下文字超链接的颜色

## 代码示例

基础用法

- 通过添加 class 类名显示不同外观颜色

```html
<a href="#">默认链接</a>
<a href="#" class="jv-link-primary">主要链接</a>
<a href="#" class="jv-link-success">成功链接</a>
<a href="#" class="jv-link-info">信息链接</a>
<a href="#" class="jv-link-warning">警告链接</a>
<a href="#" class="jv-link-error">错误链接</a>
```

不可点击状态

- 设置属性 disabled 令文字链接变为不可用状态。

```html
<a href="#" disabled>默认链接</a>
<a href="#" disabled class="jv-link-primary">主要链接</a>
<a href="#" disabled class="jv-link-success">成功链接</a>
<a href="#" disabled class="jv-link-info">信息链接</a>
<a href="#" disabled class="jv-link-warning">警告链接</a>
<a href="#" disabled class="jv-link-error">错误链接</a>
```
