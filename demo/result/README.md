# Result 结果

## 概述

用于反馈一系列操作任务的处理结果。

## 代码示例

### 基础用法

- 最基本的使用方法，设置属性 `title` 设置result的主标题，`subTitle` 设置result的副标题，如果内容较长或复杂可以使用 slot 标签代替

```html
<j-result title="您已提交过申请了" subTitle="请勿重复提交申请！">
    <slot name="footer">
        <button class="jv-btn jv-btn-primary">知道了</button>
    </slot>
</j-result>
```

- 不同状态

- 提供7中不同的状态 `success`、`info`、`warning`、`error`、`403`、`404`、`500`，默认为 `info`

```html
<j-result status="success" title="这是标题" subTitle="这是副标题"></j-result>
<j-result title="这是标题" subTitle="这是副标题"></j-result>
<j-result status="warning" title="这是标题" subTitle="这是副标题"></j-result>
<j-result status="error" title="这是标题" subTitle="这是副标题"></j-result>
<j-result status="403" title="这是标题" subTitle="这是副标题"></j-result>
<j-result status="404" title="这是标题" subTitle="这是副标题"></j-result>
<j-result status="500" title="这是标题" subTitle="这是副标题"></j-result>
```

### 自定义图标

- 设置属性 `icon` 可以自定义图标，或者使用指定的 slot 标签自定义图标

```html
<j-result title="这是标题" icon="<i class='jv-icon icon-emo-happy'></i>"></j-result>
<!--或者-->
<j-result title="这是标题">
    <slot name="icon"><i class='jv-icon icon-emo-happy'></i></slot>
</j-result>
```

## Attributes

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
|  title | 主标题文字  | String | - |
|  subTitle | 副标题文字  | String | - |
|  status | 结果的状态，决定图标和颜色，提供7中类型 `info` `success` `warning` `error` `403` `404` `500`| String | info |
|  icon | 自定义图标 | String | - |
|  footer | 底部栏，一般用于放置操作按钮 | String | - |
|

## Slot

|  名称   | 说明  |
|  ----  | ----  |
|  icon | 自定义图标，会覆盖属性 `icon` 的内容 |
|  title | 主标题文字，会覆盖属性 `title` 的内容  |
|  footer | 底部栏，会覆盖属性 `footer` 的内容  |
|  subTitle | 副标题文字，会覆盖属性 `subTitle` 的内容  |
|

### 演示

- 在合适的时机显示出来

- 将 result 标签放置在一个用于显示隐藏的容器里，并通过代码控制这个容器显示隐藏

```html
<style>
    #demo {
        display: none;
    }
</style>

<div id="demo">
    <j-result
        status="success"
        title="获得超级VIP会员服务100年！"
        subTitle="订单号：202008311500551811，VIP炒鸡会将在一秒钟后生效，祝您享受服务愉快！">
        <slot name="footer">
            <button class="jv-btn jv-btn-primary">立即体验</button>
            <button class="jv-btn jv-btn-default">分享</button>
        </slot>
    </j-result>
</div>

<button class="jv-btn jv-btn-primary" data-toggle="display" data-target="#demo">立即申请</button>
```
