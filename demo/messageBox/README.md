# MessageBox 弹框

## 概述

模拟系统的消息提示框而实现的一套模态对话框组件，用于消息提示、确认消息和提交内容。并且在全局创建一个对话框，并在消失时移除，所以同时只能操作一个对话框。

## 代码示例

### 基本用法

- 四种基本的对话框，只提供一个确定按钮。

```html
<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.info({
    title:'对话框标题',
    content:'<p>一些对话框内容</p><p>一些对话框内容</p>'
})"
>
  消息
</button>

<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.success({
    title:'对话框标题',
    content:'<p>一些对话框内容</p><p>一些对话框内容</p>'
})"
>
  成功
</button>

<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.warning({
    title:'对话框标题',
    content:'<p>一些对话框内容</p><p>一些对话框内容</p>'
})"
>
  警告
</button>

<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.error({
    title:'对话框标题',
    content:'<p>一些对话框内容</p><p>一些对话框内容</p>'
})"
>
  错误
</button>
```

### 确认对话框

- 快速弹出确认对话框，并且可以自定义按钮文字及异步关闭。

```html
<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.confirm({
            title:'确认对话框标题',
            content:'<p>一些对话框内容</p><p>一些对话框内容</p>'
})"
>
  确认
</button>

<button
  class="jv-btn jv-btn-default"
  onclick="$.MessageBox.confirm({
    title:'确认对话框标题',
    content:'<p>一些对话框内容</p><p>一些对话框内容</p>',
    okText:'知道了',
    cancelText:'不理'
})"
>
  自定义按钮文字
</button>

<button class="jv-btn jv-btn-default" id="asyn">异步关闭</button>

<script>
  $(() => {
    $("#asyn").click(() => {
      $.MessageBox.confirm({
        title: "确认对话框标题",
        content: "<p>对话框将在 2秒 后关闭</p>",
        loading: true,
        ok() {
          setTimeout(() => {
            $.MessageBox.remove();
          }, 2000);
        },
      });
    });
  });
</script>
```

### 自定义内容

- `title`和`content`属性可以传入 HTML 代码片段进行自定义内容

```js
let title = `<span>账号登录</span>`;
let input = `<input type="text" class="jv-input" placeholder="请输入账户">`;
$.MessageBox.info({
  title: title,
  content: input,
});
```

## API

通过直接调用以下方法来使用：

- \$.MessageBox.info(config)
- \$.MessageBox.success(config)
- \$.MessageBox.warning(config)
- \$.MessageBox.error(config)
- \$.MessageBox.confirm(config)

以上方法隐式地创建及维护 MessageBox 组件。参数 config 为对象，具体说明如下：

| 属性       | 说明                                                                                                | 类型                    | 默认值 |
| ---------- | --------------------------------------------------------------------------------------------------- | ----------------------- | ------ |
| title      | 提示标题                                                                                            | String / Element String | -      |
| content    | 提示内容                                                                                            | String / Element String | -      |
| width      | 自定义消息框宽度，单位 px                                                                           | Number / String         | 416    |
| okText     | 确定按钮的文字                                                                                      | String                  | 确定   |
| cancelText | 取消按钮的文字，只在 `$.MessageBox.confirm()`下有效                                                 | String                  | 取消   |
| loading    | 点击确定按钮时，确定按钮是否显示 loading 状态，开启则需手动调用 `$.MessageBox.remove()`来关闭对话框 | Boolean                 | false  |
| scrollable | 页面是否可以滚动                                                                                    | Boolean                 | false  |
| closable   | 是否可以按 Esc 键关闭                                                                               | Boolean                 | false  |
| ok         | 点击确定的回调                                                                                      | Function                | -      |
| cancel     | 点击取消的回调，只在 `$.MessageBox.confirm()`下有效                                                 | Function                | -      |

|

还提供了全局关闭对话框的方法：

- \$.MessageBox.remove()

以 Ajax 请求为例

```js
$.MessageBox.confirm({
  title: "...",
  content: "...",
  loading: true,
  ok() {
    $.ajax({
      url: "/api/someurl",
      type: "post",
      data: userName,
      //....
      success: () => {
        $.MessageBox.remove();
      },
      error: () => {
        $.MessageBox.error({
          title: "出错了",
          content: "提交内容失败",
        });
        $.MessageBox.remove();
      },
    });
  },
});
```
