# Popover气泡卡片

## 概述

当目标元素有进一步的描述和相关操作时，可以收纳到卡片中，根据用户的操作行为进行展现。
和 Tooltip 的区别是，用户可以对浮层上的元素进行操作，因此它可以承载更复杂的内容，比如链接或按钮等。

## 代码示例

### 基础用法

- 支持三种触发方式：鼠标悬停、点击、聚焦。默认是点击。

- 注意 Poptip 内的文本使用了 white-space: nowrap;，即不自动换行，如需展示很多内容并自动换行时，建议使用指定的slot标签

```html
<j-popover title="标题" content="<p>提示内容</p><p>提示内容</p>" placement="top-left" trigger="hover">
    <button class="jv-btn jv-btn-default">hover 激活</button>
</j-popover>
<j-popover title="标题" content="<p>提示内容</p><p>提示内容</p>">
    <button class="jv-btn jv-btn-default">click 激活</button>
</j-popover>
<j-popover title="标题" content="<p>提示内容</p><p>提示内容</p>" placement="top-right" trigger="focus">
    <button class="jv-btn jv-btn-default">focus 激活</button>
</j-popover>
<br><br>
<j-popover title="标题">
    <input type="text" class="jv-input" placeholder="输入框的 focus"></input>
    <slot name="content">
        <div class="t">
            <span style="color: #ccc;">输入些内容叭</span>
        </div>
    </slot>
</j-popover>

<script>
    $('input').bind("input propertychange", function(event) {
        $('.t').text($(this).val())
    });
</script>
```

### 位置

- 组件提供了12个不同的方向显示Poptip，具体配置可查看API。

```html
<j-popover title="标题" content="<p>提示内容</p><p>提示内容</p>" placement="top-left">
    <button class="jv-btn jv-btn-default">hover 激活</button>
</j-popover>
<j-popover title="标题" content="<p>提示内容</p><p>提示内容</p>">
    <button class="jv-btn jv-btn-default">hover 激活</button>
</j-popover>
<j-popover title="标题" content="<p>提示内容</p><p>提示内容</p>" placement="top-right">
    <button class="jv-btn jv-btn-default">hover 激活</button>
</j-popover>
....
```

### 从浮层内关闭

- 通过给popover添加id选择器来控制提示框的显示和隐藏。

```html
<j-popover title="标题" id="demoPopover">
    <button class="jv-btn jv-btn-primary">click激活</button>
    <slot name="content"><a href="#" class="close">关闭提示框</a></slot>
</j-popover>

<script>
    $('.close').click(() => {
        $('#demoPopover').hidePopover();
    });
</script>
```

### 嵌套复杂内容

- 通过使用指定的 slot 来实现复杂的内容。

```html
<j-popover id="demoPopover" placement="right">
    <button class="jv-btn jv-btn-default">click激活</button>
    <slot name="content">
        <div style="width: 350px;height: 150px;text-align: center;position: relative;">
            <div style="width: 100%;height: 50%;line-height: 70px; background-color: skyblue;">上部分</div>
            <div style="position: absolute;width: 50px;height: 50px;border-radius: 50%;background-color: goldenrod;top: 50%;left: 50%;transform: translate(-50%,-50%);"></div>
            <div style="width: 100%;height: 50%;line-height: 70px; background-color:#eee">下部分</div>
        </div>
    </slot>
</j-popover>
```

## Attributes

|  属性  | 说明  |  类型   | 默认值  |
|  ----  | ----  |  ----  |  ----  |
| title  | 标题 | String | — |
| content  | 内容 | String | — |
| trigger  | 触发方式，可选值为hover（悬停）click（点击）focus（聚焦） | String | click |
| placement  | 提示框出现的位置，可选值为top top-left top-right left-top left left-bottom bottom bottom-left bottom-right right right-top right-bottom | String | click |
|

## Event

- 控制popover隐藏或显示的方法，注意！必须为popover添加id选择器才能使用

```js
$(idSelector).showPopover();
$(idSelector).hidePopover();
```

## Slot

|  名称  | 说明  |
|  ----  | ----  |
| title  | 提示框标题，定义此 slot 时，会覆盖 属性 title |
| content  | 提示框内容，定义此 slot 时，会覆盖 属性 content |
|
