# Avatar 头像

## 概述

用来代表用户或事物，支持图片、图标或字符展示。

## 代码示例

### 基础用法

- 头像有四种尺寸，两种形状可选，默认为圆形。

```html
<span class="jv-avatar jv-avatar-circle jv-avatar-huge"><i class="jv-icon icon-user"></i></span>
<span class="jv-avatar jv-avatar-circle jv-avatar-large"><i class="jv-icon icon-user"></i></span>
<span class="jv-avatar jv-avatar-circle jv-avatar-medium"><i class="jv-icon icon-user"></i></span>
<span class="jv-avatar jv-avatar-circle jv-avatar-small"><i class="jv-icon icon-user"></i></span>
```

- 方形

```html
<span class="jv-avatar jv-avatar-square jv-avatar-huge"><i class="jv-icon icon-user"></i></span>
<span class="jv-avatar jv-avatar-square jv-avatar-large"><i class="jv-icon icon-user"></i></span>
<span class="jv-avatar jv-avatar-square jv-avatar-medium"><i class="jv-icon icon-user"></i></span>
<span class="jv-avatar jv-avatar-square jv-avatar-small"><i class="jv-icon icon-user"></i></span>
```

### 类型

- 支持三种类型：图片、Icon 以及字符，其中 Icon 和字符型可以自定义图标颜色及背景色。

```html
<span class="jv-avatar jv-avatar-circle jv-avatar-medium"><i class="jv-icon icon-user"></i></span>
<span class="jv-avatar jv-avatar-circle jv-avatar-medium"><img src="./images.jpg"></span>
<span class="jv-avatar jv-avatar-circle jv-avatar-medium" style="background: #a123f1;">
    <span class="jv-avatar-text">瑞</span>
</span>
<span class="jv-avatar jv-avatar-circle jv-avatar-medium" style="background: #6bc800;">
    <i class="jv-icon icon-user"></i>
</span>
```

### 自动调整字符大小

- 对于字符型的头像，当字符串较长时，字体大小可以根据头像宽度自动调整。

```html
<span class="jv-avatar jv-avatar-circle jv-avatar-medium"><span class="jv-avatar-text">Usase</span></span>
<span class="jv-avatar jv-avatar-circle jv-avatar-medium"><span class="jv-avatar-text">USER</span></span>
```
