# Alert 警告提示

## 概述

警告提示，展现需要关注的信息。

## 代码示例

### 基础用法

- 最简单的用法，适用于简短的警告提示。

```html
<j-alert message="成功提示的文案" type="success"></j-alert>
```

### 四种样式

- 有四种样式可以选择 `info`、`success`、`warning`、`error`。

```html
<j-alert message="成功提示的文案" type="success"></j-alert>
<j-alert message="消息提示的文案"></j-alert>
<j-alert message="警告提示的文案" type="warning"></j-alert>
<j-alert message="错误提示的文案" type="error"></j-alert>
```

### 含描述信息

- 通过设置属性 `desc` 展示描述内容。

- 或者通过添加标签 `<slot name="desc"></slot>` 设置描述内容，会覆盖原先的 `desc` 属性的内容

```html
 <j-alert
    message="成功提示的文案"
    desc="This is the message of success description. This is the message of success description"
    type="success">
 </j-alert>
<j-alert
    message="消息提示的文案"
    desc="This is the message of info description. This is the message of info description">
</j-alert>
<j-alert
    message="警告提示的文案"
    desc="This is the message of warning description. This is the message of warning description"
    type="warning">
</j-alert>
<j-alert
    message="错误提示的文案"
    desc="This is the message of error description. This is the message of error description"
    type="error">
</j-alert>
<j-alert message="错误提示的文案" type="error">
    <slot name="desc">This is the message of error description. This is the message of error description</slot>
</j-alert>
```

### 图标

- 通过设置属性 `data-show-icon="true"` 且根据 type 属性自动添加不同图标

- 可以通过属性 `data-icon` 自定义图标，或者使用slot标签 `<slot name="icon"></slot>`，自定义的图标会替换原先的关闭图标

```html
<j-alert message="成功提示的文案" type="success" data-show-icon="true" data-banner="true"></j-alert>
<j-alert message="消息提示的文案" data-show-icon="true"></j-alert>
<j-alert message="警告提示的文案" type="warning" data-show-icon="true"></j-alert>
<j-alert message="错误提示的文案" type="error" data-show-icon="true"></j-alert>
<j-alert
    message="成功提示的文案"
    desc="Detailed description and advice about successful copywriting."
    type="success"
    data-show-icon="true">
</j-alert>
<j-alert
    message="消息提示的文案"
    desc="Additional description and information about copywriting."
    data-show-icon="true">
</j-alert>
<j-alert
    message="警告提示的文案"
    desc="This is a warning notice about copywriting."
    type="warning"
    data-show-icon="true">
</j-alert>
<j-alert
    message="错误提示的文案"
    desc="This is an error message about copywriting."
    type="error"
    data-show-icon="true">
</j-alert>
<j-alert
    message="自定义图标"
    desc="Custom icon copywriting. Custom icon copywriting. Custom icon copywriting."
    data-show-icon="true"
    data-icon="<i class='jv-icon icon-paper-plane'></i>">
</j-alert>
```

### 可关闭的警告提示

- 通过设置属性 `data-closable="true"` 显示关闭按钮，点击可平滑、自然的关闭警告提示。

- 可以通过设置属性 `data-close-text` 自定义关闭文字，自定义的文字会替换原先的关闭图标。

```html
<j-alert
    message="成功提示的文案"
    type="success"
    data-closable="true">
</j-alert>
<j-alert
    message="警告提示的文案"
    desc="This is a warning notice about copywriting."
    type="warning"
    data-closable="true">
</j-alert>
<j-alert
    message="消息提示的文案"
    data-closable="true">
</j-alert>
<j-alert
    message="自定义 close-text"
    data-closable="true"
    data-close-text="知道了">
</j-alert>
```

### 顶部公告

- 设置属性 `data-banner="true"` 可以应用页面顶部通告形式。

```html
<j-alert message="顶部通告提示文案" type="warning" data-banner="true"></j-alert>
```

## Attributes

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| type | 警告提示样式，可选值为`info`、`success`、`warning`、`error` | String | info |
| message | 警告提示内容 | String | - |
| desc | 警告提示的辅助性文字介绍 | String | - |
| data-closable | 是否可关闭 | Boolean | false |
| data-close-text | 关闭按钮自定义文本，`data-closable` 为 `true` 时有效 | String | - |
| data-show-icon | 是否显示图标 | Boolean | false |
| data-icon | 自定义图标，`data-show-icon` 为 `true` 时有效 | String | - |
| data-banner | 是否用作顶部公告 | Boolean | false |

## Slot

|  名称   | 说明  |
|  ----  | ----  |
| message | 警告提示标题 |
| desc | 警告提示辅助性文字介绍 |
| icon | 自定义图标内容 |

### 控制alert在合适的时机显示隐藏

- 通过给alert标签设置外层容器，用于显示隐藏

- 设置属性 `data-toggle="display"` 会自动根据 点击的 `data-target` 目标元素的 `display` 属性判断是否显示或隐藏

```html
<button class="jv-btn jv-btn-default" data-toggle="display" data-target="#demoShowHiddenAlert">显示alert</button>
<div style="display:none;" id="demoShowHiddenAlert">
    <j-alert message="这是一条消息提示文案"></j-alert>
</div>
```
