# Badge 徽标

## 概述

用于提示用户消息通知未读数的角标。

## 代码示例

### 基础用法

- 最基础的使用方法。

- 当 `<sup></sup>`的内容空为空或为"0"时会自动隐藏徽标

```html
<style>
    .demo-badge {
        width: 42px;
        height: 42px;
        background: #eee;
        border-radius: 6px;
        display: inline-block;
    }
</style>

<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-error">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
```

### 小红点

- 以红点的形式标注需要关注的内容。

```html
<div class="jv-badge">
     <sup class= "jv-badge-dot">1</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
    <sup class= "jv-badge-dot">1</sup>
    <a href="#">订阅号消息</a>
</div>
```

### 最大消息数

- 通过设置 `data-max` 属性设置一个最大值，当超过时，会显示${max}+

```html
<div class="jv-badge" data-max="99">
    <sup class="jv-badge-count">100</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge" data-max="999">
    <sup class="jv-badge-count">1000</sup>
    <a href="#" class="demo-badge"></a>
</div>
```

### 独立展示徽标

- 可以不添加任何标签内容独立展示。

```html
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-alone">10</sup>
</div>
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-success jv-badge-alone">15</sup>
</div>
```

### 自定义内容

- 可以显示数字以外的文本内容。

```html
<div class="jv-badge">
    <sup class="jv-badge-count">new</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
    <sup class="jv-badge-count">hot</sup>
    <a href="#" class="demo-badge"></a>
</div>
```

### 预设颜色

- 提供六种 `primary` `normal` `success` `info` `warning` `error`

```html
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-primary">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-normal">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-success">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-info">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
 <s class="jv-badge">
    <sup class="jv-badge-count jv-badge-warning">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
<div class="jv-badge">
    <sup class="jv-badge-count jv-badge-error">6</sup>
    <a href="#" class="demo-badge"></a>
</div>
```

## API

### Badge props

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| data-max  | 展示封顶的数字值 | Number | 99 |
