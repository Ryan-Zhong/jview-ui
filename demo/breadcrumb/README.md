# Breadcrumb 面包屑

## 概述

- 显示网站的层级结构，告知用户当前所在位置，以及在需要向上级导航时使用。

## 代码示例

```html
<ul class="jv-breadcrumb" role="navigation">
    <li>
        <b><a href="#">首页</a></b>
        <span class="jv-breadcrumb-item-separator">/</span>
    </li>
    <li>
        <a href="#">活动管理</a>
        <span class="jv-breadcrumb-item-separator">/</span>
    </li>
    <li>
        <span href="#">活动列表</span>
        <span class="jv-breadcrumb-item-separator ">/</span>
    </li>
    <li>
        <span href="#">活动详情</span>
    </li>
</ul>
```

### 自定义分隔符

```html
<ul class="jv-breadcrumb" role="navigation">
    <li>
        <b><a href="#">首页</a></b>
        <span class="jv-breadcrumb-item-separator"><i class="demo-icon icon-right-open"></i></span>
    </li>
    <li>
        <b><a href="#">活动管理</a></b>
        <span class="jv-breadcrumb-item-separator"><i class="demo-icon icon-right-open"></i></span>
    </li>
    <li>
        <span href="#">活动列表</span>
        <span class="jv-breadcrumb-item-separator "><i class="demo-icon icon-right-open"></i></span>
    </li>
    <li>
        <span href="#">活动详情</span>
    </li>
</ul>

<ul class="jv-breadcrumb" role="navigation">
    <li>
        <b><a href="#">首页</a></b>
        <b style="color: #f50;padding: 0 5px;" class="jv-breadcrumb-item-separator">=></b>
    </li>
    <li>
        <b><a href="#">活动管理</a></b>
        <b style="color: #f50;padding: 0 5px;" class="jv-breadcrumb-item-separator">=></b>
    </li>
    <li>
        <b><a href="#">活动列表</a></b>
        <b style="color: #f50;padding: 0 5px;" class="jv-breadcrumb-item-separator">=></b>
    </li>
    <li>
        <span href="#">活动详情</span>
    </li>
</ul>
```


### 带图标

```html
<ul class="jv-breadcrumb" role="navigation">
    <li>
        <b><a href="#"><i class="demo-icon icon-home"></i>首页</a></b>
        <span class="jv-breadcrumb-item-separator">/</i></span>
    </li>
    <li>
        <b><a href="#"><i class="demo-icon icon-menu"></i>活动管理</a></b>
        <span class="jv-breadcrumb-item-separator">/</i></span>
    </li>
    <li>
        <b><a href="#"><i class="demo-icon icon-th-list"></i>活动列表</a></b>
        <span class="jv-breadcrumb-item-separator ">/</i></span>
    </li>
    <li>
        <b><a href="#"><i class="demo-icon icon-zoom-in"></i>活动详情</a></b>
    </li>
</ul>
```
