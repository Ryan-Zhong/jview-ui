# Message 全局提示

## 概述

轻量级的信息反馈组件，在顶部居中显示，并自动消失。有多种不同的提示状态可选择。

## 代码示例

### 普通提示

- 最基本的提示，默认在 3 秒后消失。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.info('这是一条普通的提示')"
>
  显示普通提示
</button>
```

### 提示类型

- 不同的提示状态：成功、警告、错误。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.info('这是一条普通的提示')"
>
  显示普通提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.success('恭喜你，这是一条成功消息')"
>
  显示成功提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.warning('警告你哦，这是一条警告消息')"
>
  显示警告提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.error('message不想说话，并且向你抛出了一个异常')"
>
  显示错误提示
</button>
```

### 带背景色

- 设置属性 `background` 为 `true` 后，通知提示会显示背景色。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.info('这是一条普通的提示',{ background:true })"
>
  显示普通提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.success('恭喜你，这是一条成功消息',{ background:true })"
>
  显示成功提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.warning('警告你哦，这是一条警告消息',{ background:true })"
>
  显示警告提示
</button>
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.error('message不想说话，并且向你抛出了一个异常',{ background:true })"
>
  显示错误提示
</button>
```

### 加载中

- Loading 的状态，并异步在某个时机移除。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.loading('正在加载中...')"
>
  显示加载中
</button>
```

### 修改延时

- 自定义时长 10s，也可以在\$.Message.config()中全局配置，详见 API。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.success('这是成功的提示信息，我将在10秒内消失',{ duration:10 })"
>
  显示10秒提示
</button>
```

### 可关闭

- 设置参数 `closable` 为 `true` 可以手动关闭提示。

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.info('可手动关闭的提示',{ closable:true })"
>
  显示可关闭提示
</button>
```

### 使用 HTML 片段

- `content` 属性支持传入 HTML 片段

```html
<button
  class="jv-btn jv-btn-primary"
  onclick="$.Message.info('<strong>这是 <i>HTML</i> 片段</strong>')"
>
  显示HTML 片段
</button>
```

### 显示顺序消息

- 可以通过内置方法 `afterClose()` 在关闭后运行 callback 。以下用例将在每个 message 将要结束时通过内置方法 `afterClose()` 显示新的 message ，可以嵌套使用。

```html
<button
  class="jv-btn jv-btn-default"
  onclick="$.Message.loading({
    content:' 正在加载中...',
    afterClose() {
        $.Message.success('加载成功',{ 
            afterClose() {
                $.Message.info('这是加载成功后的提示')
            }
      })
    }
 })"
>
  显示顺序消息
</button>
```

## API

组件提供了一些静态方法，通过直接调用以下方法来使用组件：

- `$.Message.info(content,config)`
- `$.Message.success(content,config)`
- `$.Message.warning(content,config)`
- `$.Message.error(content,config)`
- `$.Message.loading(content,config)`

以上方法隐式的创建及维护 Message 组件。

| 属性       | 说明                                   | 类型            | 默认值 |
| ---------- | -------------------------------------- | --------------- | ------ |
| duration   | 自动关闭的延时，单位秒，不关闭可以写 0 | Number          | 1.5    |
| onClose    | 手动点击关闭按钮时的回调               | Function        | -      |
| afterClose | message 自动关闭时的回调               | Boolean         | false  |
| closable   | 是否显示关闭按钮                       | Boolean         | false  |
| background | 是否显示背景色                         | Boolean         | false  |
| key        | 当前 message 的唯一标志                | String / Number | 时间戳 |

### 全局方法

- 还提供了全局配置和全局销毁方法：

- `$.Message.config(config)`
- `$.Message.destroy(key,callback)`

- 需要注意的是全局销毁方法在未使用 Message 实例化方法将组件实例化前直接使用销毁方法是无效的，由于它的参数 `key` 得到的值应在对应组件实例化后才获取得到，所以应在对应组件实例化之后再调用全局销毁方法

### _\$.Message.config_

```js
$.Message.config({
  top: 100,
  duration: 6,
});
```

| 属性     | 说明                             | 类型   | 默认值 |
| -------- | -------------------------------- | ------ | ------ |
| top      | 提示组件距离顶端的距离，单位像素 | Number | 12     |
| duration | 默认自动关闭的延时，单位秒       | Number | 3      |

### _\$.Message.destroy(key)_

通过 `$.Message.destroy(key)` 来关闭一条消息。这里以 Ajax 请求为例

```html
<button class="jv-btn jv-btn-primary" id="updata">更新内容</button>
<script>
  $(() => {
    const _updata = () => {
      //在ajax请求未有响应结果返回前一直显示加载中
      $.Message.loading("正在更新数据...", {
        duration: 0,
        key: "updatable",
      });

      $.ajax({
        url: "/api",
        type: "get",
        //some codes...
        success() {
          //收到返回结果后将loading移除
          $.Message.destroy("updatable", () => {
            $.Message.success("更新完成");
          });
          //do some things...
        },
      });
    };

    $("#updata").click(() => {
      _updata();
    });
  });
</script>
```

| 参数     | 说明                    | 类型            | 默认值 |
| -------- | ----------------------- | --------------- | ------ |
| key      | 当前 message 的唯一标志 | String / Number | -      |
| callback | 组件销毁后的回调        | Function        | -      |
