# Dropdown 下拉菜单

## 概述

展示一组折叠的下拉菜单。

### 触发方式

- 通过设置属性 `data-trigger` 可以更改触发方式，可选项为 click 、 hover(默认)、contextMenu

```html
<div class="jv-dropdown">
     <div class="jv-dropdown-rel">
         <a href="javascript:void(0)">hover 触发</a>
     </div>
     <div class="jv-dropdown-select-dropdown jv-dropdown-slide-out">
         <ul class="jv-dropdown-menu" role="menu">
             <li class="jv-dropdown-item" role="menuitem">选项一</li>
             <li class="jv-dropdown-item" role="menuitem">选项二</li>
             <li class="jv-dropdown-item" role="menuitem">选项三</li>
         </ul>
     </div>
 </div>
 <div class="jv-dropdown" data-trigger="click">
     <div class="jv-dropdown-rel">
         <a href="javascript:void(0)">click 触发</a>
     </div>
     <div class="jv-dropdown-select-dropdown jv-dropdown-slide-out">
         <ul class="jv-dropdown-menu" role="menu">
             <li class="jv-dropdown-item" role="menuitem">选项一</li>
             <li class="jv-dropdown-item" role="menuitem">选项二</li>
             <li class="jv-dropdown-item" role="menuitem">选项三</li>
         </ul>
     </div>
 </div>
 <div class="jv-dropdown" data-trigger="contextMenu">
     <div class="jv-dropdown-rel">
         <a href="javascript:void(0)">右键触发</a>
     </div>
     <div class="jv-dropdown-select-dropdown jv-dropdown-slide-out">
         <ul class="jv-dropdown-menu" role="menu">
             <li class="jv-dropdown-item" role="menuitem">返回</li>
             <li class="jv-dropdown-item" role="menuitem"><a href="#" style="color: red;">删除</a></li>
         </ul>
     </div>
 </div>
```

### 对齐方向

- 通过设置属性 `data-placement` 可以更改下拉菜单出现的方向。支持 6 个弹出位置，默认方向为bottom。

```html
 <div class="jv-dropdown" data-placement="bottom-left">...</div>
 <div class="jv-dropdown">...</div>
 <div class="jv-dropdown" data-placement="bottom-right">...</div>
 <div class="jv-dropdown" data-placement="top-left">...</div>
 <div class="jv-dropdown" data-placement="top">...</div>
 <div class="jv-dropdown" data-placement="top-right">...</div>
```

## Attributes

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| data-trigger | 触发方式，可选值为 `hover`（悬停）`click`（点击）`contextMenu`（右键） | String  | hover |
| data-placement | 下拉菜单出现的位置，可选值为`top` `top-left` `top-right` `bottom` `bottom-left` `bottom-right`| String  | bottom |
|
