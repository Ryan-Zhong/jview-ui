# Modal 对话框

## 概述

在保留当前页面状态的情况下，告知用户并承载相关操作。

## 代码示例

### 基础用法

- 最简单的使用方法。

- 通过配合 $Modal.toggle() 方法来控制对话框显示或隐藏，不可嵌套。

- 默认按键盘ESC键也可以关闭。

```html
<button id="exampleModal1">显示对话框</button>
<j-modal id="modal1" title="Modal对话框标题" data-ok-text="确定" data-cancel-text="取消">
    <p>这是对话框的信息</p>
    <p>这是对话框的信息</p>
    <p>这是对话框的信息</p>
</j-modal>
<script>
    $(()=> {
        $Modal.toggle({
            trigger: '#exampleModal1',
            target: '#modal1',
            ok() {
                //点击确定按钮的回调事件
            },
            cancel() {
                 //点击取消按钮的回调事件
            }
        });
    });
</script>
```

### 首屏显示

- 通过设置属性 data-hidden="false" 直接显示对话框

```html
<j-modal id="modal" title="Modal对话框标题" data-hidden="false" data-ok-text="确定" data-cancel-text="取消">
    <p>这是对话框的信息</p>
    <p>这是对话框的信息</p>
    <p>这是对话框的信息</p>
</j-modal>
```

### 自定义位置、宽度

- 通过设置属性 data-width 设置对话框宽度。单位无限制，但必须有

- 通过设置属性 data-position 设置对话框位置。单位无需添加，默认设定为px

```html
<j-modal id="modal4" title="自定义宽度" data-width="300px" data-ok-text="确定" data-cancel-text="取消">
        <p>自定义宽度，单位无限制但必须有，默认 520px。对话框的宽度是响应式的，当屏幕尺寸小于 768px 时，宽度会变为自动auto。</p>
</j-modal>
<j-modal id="modal5" title="自定义位置" data-position="[320,320]" data-ok-text="确定 " data-cancel-text="取消 ">
        <p>我的位置可以出现在屏幕的任意一个地方哦！</p>
</j-modal>
<j-modal id="modal6" title="对话框标题" data-position="[280]" data-ok-text="确定" data-cancel-text="取消">
        <p>看！我现在处于屏幕中心了</p>
</j-modal>
```

### 限制内容区域的高度

- 通过设置属性 data-max-height 限制对话框内容区域的高度，默认为'none'。单位无限制，但必须有

```html
<j-modal id="modal14" title="对话框标题" data-max-height="400px" data-ok-text="确定 " data-cancel-text="取消 ">
    <p>这是对话框的信息</p>
    <p>这是对话框的信息</p>
    <p>这是对话框的信息</p>
</j-modal>
```

### 不带标题栏和底部栏

- 将 title 属性去除则不带标题栏

- 通过设置属性 data-footer-hide="true" 不显示底部按钮栏

```html
<j-modal id="modal2" data-ok-text="确定" data-cancel-text="取消">
        <p>我头去哪了？</p>
        <p>我头去哪了？</p>
        <p>我头去哪了？</p>
</j-modal>
<j-modal id="modal3" title="不带按钮栏" data-footer-hide="true" data-ok-text="确定" data-cancel-text="取消">
        <p>我的按钮栏不见了，你有看到它吗？</p>
        <p>我的按钮栏不见了，你有看到它吗？</p>
        <p>我的按钮栏不见了，你有看到它吗？</p>
</j-modal>
```

### 禁用关闭

- 通过设置属性 data-closable="false" 可以禁用关闭(含Esc键)。

- 通过设置属性 data-mask-closable="false" 可以禁用遮罩层关闭。

```html
<j-modal id="modal7" title="对话框标题 " data-closable="false" data-ok-text="确定" data-cancel-text="取消 ">
        <p>看！我右上角的关闭按钮不见了哟，你关闭不了我了（含ESC键）。</p>
</j-modal>
<j-modal id="modal8" title="禁用遮盖层" data-mask-closable="false" data-ok-text="确定" data-cancel-text="取消 ">
        <p>┭┮﹏┭┮，遮盖层已被禁用关闭，你点不了了</p>
 </j-modal>
```

### 全屏

设置属性 data-fullscreen="true" 可以全屏显示。

```html
<j-modal id="modal9" title="全屏对话框" data-fullscreen="true" data-ok-text="确定" data-cancel-text="取消">
        <p>我膨胀了，我变全屏了</p>
        <p>我膨胀了，我变全屏了</p>
        <p>我膨胀了，我变全屏了</p>
</j-modal>
```

### 隐藏遮盖层

- 通过设置属性 data-mask="false" 可以隐藏遮盖层

```html
<j-modal id="modal10" title="全屏对话框" data-mask="false" data-ok-text="确定" data-cancel-text="取消">
        <p>我膨胀了，我变全屏了</p>
        <p>我膨胀了，我变全屏了</p>
        <p>我膨胀了，我变全屏了</p>
</j-modal>
```

### 自定义内容

```html
<j-modal id="modal11" data-mask-closable="false" data-ok-text="确定" data-cancel-text="取消">
    <div>
        <p>邮箱：<input type="text" placeholder="请输入邮箱"></p>
        <p>内容：<input type="text" placeholder="发送的内容"></p>
    </div>
</j-modal>
```

## API

通过调用以下方法来控制modal显示或隐藏，以及事件的回调

- $Modal.toggle(config)

|  参数   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| trigger | 通过点击某个元素触发modal显示或隐藏 | String | — |
| target  | 触发的目标modal为标签上设置的id选择器 |String |  —  |
| ok | 点击确定的回调 |Function| — |
| cancel | 点击取消的回调 |Function| — |

Modal props
|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| title | 对话框标题，如果不添加该属性则不显示modal页头 | String | — |
| data-hidden | 是否直接在打开页面时显示对话框 |Boolean | true |
| data-mask-closable | 是否允许点击遮罩层关闭 |Boolean | true |
| data-fullscreen | 是否全屏显示 |Boolean | false |
| data-footer-hide | 不显示底部 |Boolean | false |
| data-mask | 是否显示遮罩层 |Boolean | true |
| data-ok-text | 确定按钮文字 |String | 确定 |
| data-cancel-text | 取消按钮文字 |String | 取消 |
| id | 为对话框容器.jv-modal-wrap的类名设置id选择器，可辅助实现垂直居中等自定义效果 |String | - |
| data-width | 对话框宽度，对话框的宽度是响应式的，当屏幕尺寸小于 768px 时，宽度会变为自动auto。单位无限制，但必须有 |String| 520px |
| data-max-height |限制对话框内容最大高度，超出部分则滚动条显示。单位无限制，但必须有 |String| none |
| data-position | 对话框位置。值对应的位置 [top,left]。如果数组中只有一个值则默认为top值 [100] ，如果仅需设置left值需这样写 [0,50]。无需添加单位，默认设定为px |Array| [20,0] |
| data-z-index| 层级 |Number | 1000 |
