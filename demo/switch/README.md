# Switch 开关

## 表示两种相互对立的状态间的切换，多用于触发「开/关」

## 代码示例

### 基础用法

- 基本用法，状态切换时会触发事件。

```html
<j-switch></j-switch>
```

### 尺寸

- 通过设置属性 data-size 为 large 或 small 使用大号或小号的开关，默认为中尺寸

```html
<j-switch data-size="small"></j-switch>
<j-switch></j-switch>
<j-switch data-size="large"></j-switch>
```

### 文字描述

- 通过设置属性 data-open 和 data-close 来设置开关的文字描述。

- 如果使用 2 个汉字，建议将开关尺寸设置为 large。

- 可以直接放入图标 html 代码

```html
<j-switch data-open="开" data-close="关"></j-switch>
<j-switch
  data-open='<i class="demo-icon icon-ok"></i>'
  data-close='<i class="demo-icon icon-cancel"></i>'
></j-switch>
<j-switch data-size="large" data-open="开启" data-close="关闭"
  >j-switch>
  <j-switch data-size="large" data-open="ON" data-close="OFF"
    >j-switch></j-switch
  ></j-switch
>
```

### 选中状态

- 通过设置属性 data-actived 为 true 展示选中状态

```html
<j-switch data-actived="true"></j-switch>
```

### 禁用状态

```html
<j-switch disabled></j-switch>
```

### 自定义颜色

- 通过设置属性 data-true-color 和 data-false-color 可以自定义背景色。

```html
<j-switch data-true-color="#00c853" data-false-color="#ff4d4f"></j-switch>
```

## API

| 属性             | 说明                                                                                                 | 类型    | 默认值  |
| ---------------- | ---------------------------------------------------------------------------------------------------- | ------- | ------- |
| data-size        | 开关的尺寸，可选值为 large、small、default 或者不写。建议开关如果使用了 2 个汉字的文字，使用 large。 | String  | medium  |
| disabled         | 禁用开关                                                                                             | Boolean | false   |
| data-actived     | 是否选中状态                                                                                         | Boolean | false   |
| data-true-color  | 自定义打开时的背景色                                                                                 | String  | #2196f3 |
| data-false-color | 自定义关闭时的背景色                                                                                 | String  | #c5c8ce |
| ddata-open       | 自定义显示打开时的内容                                                                               | String  | —       |
| ddata-close      | 自定义显示关闭时的内容                                                                               | String  | —       |

## Events

- 通常需要点击 switch 来绑定一些事件，可以根据 switch 的属性 data-actived 的值 true 和 false 来做不同的事情

- 示例

```html
<j-switch class="className"></j-switch>

<script>
  $(() => {
    $(".className").click(() => {
      let flag = $(".className").data("actived"); //switch点击的状态
      if (flag) {
        alert(flag); // -> true
      } else {
        alert(flag); // -> false
      }
    });
  });
</script>
```
