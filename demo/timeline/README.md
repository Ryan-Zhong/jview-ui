# Timeline 时间线

## 概述

对一系列信息进行时间排序时，垂直地展示。

## 代码示例

### 基础用法

- 最简单定义一个时间轴的用法。

```html
<style>
    .time{
        font-size: 14px;
        font-weight: bold;
    }
    .content {
        margin-top: 8px;
        font-size: 13px;
        color: #909399;
        line-height: 1;
    }
</style>

<j-timeline>
    <j-timeline-item>
        <p class="time">1949年</p>
        <p class="content">中华人民共和国成立</p>
    </j-timeline-item>
    <j-timeline-item>
        <p class="time">1978年</p>
        <p class="content">邓小平提出改革开放政策</p>
    </j-timeline-item>
    <j-timeline-item>
        <p class="time">2008年</p>
        <p class="content">北京成功举办奥运会</p>
    </j-timeline-item>
    <j-timeline-item>
        <p class="time">2010年</p>
        <p class="content">中国―东盟自由贸易区正式启动</p>
    </j-timeline-item>
    <j-timeline-item>
        <p class="time">2020年</p>
        <p class="content">将全面建成小康社会，实现第一个百年奋斗目标。</p>
    </j-timeline-item>
</j-timeline>
```

### 圆圈颜色

- 用各种颜色来标识不同状态，通过属性 `data-color` 可以使用`green`、`red`、`blue`、`yellow`、`lightblue`、`dark`，默认是 `blue` 。

```html
<j-timeline>
    <j-timeline-item data-color="yellow">发布1.0版本</j-timeline-item>
    <j-timeline-item data-color="lightblue">发布2.0版本</j-timeline-item>
    <j-timeline-item data-color="red">严重故障</j-timeline-item>
    <j-timeline-item data-color="blue">发布3.0版本</j-timeline-item>
    <j-timeline-item data-color="green">发布最终版本</j-timeline-item>
</j-timeline>
```

### 最后一个

- 通过设置属性 `data-pending="true"` 来标记最后一个为幽灵节点，标识还未完成。

```html
<j-timeline data-pending="true">
    <j-timeline-item>发布1.0版本</j-timeline-item>
    <j-timeline-item>发布2.0版本</j-timeline-item>
    <j-timeline-item>发布3.0版本</j-timeline-item>
    <j-timeline-item><a href="#" style="color: #2d8cf0;">查看更多</a></j-timeline-item>
</j-timeline>
```

### 自定义时间轴点

- 通过属性 `data-dot`自定义时间轴点，可使用图标作为时间轴点

```html
<j-timeline>
    <j-timeline-item data-dot='<i class="jv-icon icon-sun-filled"></i>'>今日天气晴朗</j-timeline-item>
    <j-timeline-item data-dot='<i class="jv-icon icon-cloud-sun"></i>'>明天晴转多云</j-timeline-item>
    <j-timeline-item data-dot='<i class="jv-icon icon-rain"></i>'>后天大到暴雨</j-timeline-item>
</j-timeline>
```

## API

j-timeline props

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| data-pending  | 指定是否最后一个节点为幽灵节点 | Boolean  | false

j-timeline-item props

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| data-color | 设置时间轴点颜色，可选值为`green`、`red`、`blue`、`yellow`、`lightblue`、`dark` | String  | blue
| data-dot  | 自定义时间轴点内容 | String  | -
