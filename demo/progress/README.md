# Progress 进度条

## 概述

在操作需要较长时间才能完成时，为用户显示该操作的当前进度和状态。

当需要显示一个操作完成的百分比时。

## 代码示例

### 基本用法

- 处在不同状态下的进度条

```html
<div class="jv-progress">
    <div class="jv-progress-bar" role="progressbar" style="width: 25%;"></div>
</div>
<div class="jv-progress">
    <div class="jv-progress-bar info" role="progressbar" style="width: 45%;"></div>
</div>
<div class="jv-progress">
    <div class="jv-progress-bar success" role="progressbar" style="width: 100%;"></div>
</div>
<div class="jv-progress">
    <div class="jv-progress-bar warning" role="progressbar" style="width: 75%;"></div>
</div>
<div class="jv-progress">
    <div class="jv-progress-bar error" role="progressbar" style="width: 55%;"></div>
</div>
```

### 显示进度文字

```html
<div class="jv-progress">
    <div class="jv-progress-bar" role="progressbar" style="width: 40%;">40%</div>
</div>
<div class="jv-progress">
    <div class="jv-progress-bar success" role="progressbar" style="width: 100%;">成功</div>
</div>
```

### 渐变色

- 自定义背景色

```html
<div class="jv-progress">
    <div class="jv-progress-bar success" role="progressbar" style="width: 80%; background-image: linear-gradie(to right, rgb(16, 142, 233) 0%, rgb(135, 208, 104) 100%);"></div>
</div>
```

### 分段进度条

- 标准的进度条。

```html
<div class="jv-progress">
    <div class="jv-progress-bar" role="progressbar" style="width: 15%;"></div>
    <div class="jv-progress-bar success" role="progressbar" style="width: 30%;"></div>
    <div class="jv-progress-bar info" role="progressbar" style="width: 20%;"></div>
</div>
```

### 动画效果

- 添加class `active` 显示进度中动画

```html
<div class="jv-progress">
    <div class="jv-progress-bar active" role="progressbar" style="width: 50%;"></div>
</div>
```
