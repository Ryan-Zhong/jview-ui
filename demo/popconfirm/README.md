# Popconfirm气泡确认框

## 概述

目标元素的操作需要用户进一步的确认时，在目标元素附近弹出浮层提示，询问用户。

和 confirm 弹出的全屏居中模态对话框相比，交互形式更轻量。

## 代码示例

### 基础用法

- Popconfirm 的属性与 Popover 很类似，因此对于重复属性，请参考 Popover 的文档，在此文档中不做详尽解释。

- 最简单的用法。

```html
<j-popconfirm title="您确认删除这条内容吗？" onConfirm="ok()" onCancel="cancel()">
    <a href="#">删除</a>
</j-popconfirm>
```

### 自定义按钮文字

- 设置属性 `ok-text` 和 `cancel-text` 自定义按钮文字。

```html
<j-popconfirm
  title="Are you sure delete this task?"
  ok-text="Yes"
  cancel-text="No"
  onConfirm="ok()"
  onCancel="cancel()">
    <a href="#">Delete</a>
</j-popconfirm>
```

## Attributes

|  属性  | 说明  |  类型   | 默认值  |
|  ----  | ----  |  ----  |  ----  |
| title  | 确认框的描述 | String | — |
| ok-text  | 标题 | String | — |
| cancel-text  | 标题 | String | — |
| onConfirm  | 点击确认的回调 | Function | — |
| onCancel  | 点击取消的回调 | Function | — |
|

## Slot

|  名称  | 说明  |
|  ----  | ----  |
| title  | 提示框标题，定义此 slot 时，会覆盖 属性 title |
|
