# InputNumber 计数器

## 概述

- _使用鼠标或键盘输入一定范围的标准数值。_

## 代码示例

### 基础用法

- _可以通过输入、鼠标点击或键盘的上下键来改变数值大小，通过设置属性 `data-min` 和 `data-max` 来控制计数范围。_

```html
<j-input-number data-min="1" data-max="10"></j-input-number>
```

### 小数

- _通过设置 `data-step` 属性控制每次改变的精度_

```html
<j-input-number data-min="1" data-max="10" data-step="1.2"></j-input-number>
```

### 尺寸

- _通过设置 `size` 属性为 large 和 small 将输入框设置为大和小尺寸，不设置为默认（中）尺寸。_

```html
<j-input-number size="small"></j-input-number>
<j-input-number></j-input-number>
<j-input-number size="large"></j-input-number>
```

### 禁用

- _通过设置 `data-disabled="true"` 属性禁用输入框。_

```html
<j-input-number data-disabled="true" data-val="2"></j-input-number>
```

### 只读

- _通过设置 `data-readonly="true"` 属性开启只读。_

```html
<j-input-number data-readonly="true" data-val="2"></j-input-number>
```

### 不可编辑

- _通过设置 `data-editable="true"` 属性控制是否能编辑。_

```html
<j-input-number data-editable="true" data-val="2"></j-input-number>
```

## API

Attribute

| 属性          | 说明                                                    | 类型    | 默认值    |
| ------------- | ------------------------------------------------------- | ------- | --------- |
| size          | 输入框尺寸，可选值为`large`、`small`、`default`或者不填 | String  | default   |
| data-min      | 最小值                                                  | Number  | Infinity  |
| data-max      | 最大值                                                  | Number  | -Infinity |
| data-val      | 当前输入框的初始值                                      | Number  | -         |
| data-step     | 每次改变的步伐，可以是小数                              | Number  | 1         |
| data-disabled | 设置禁用状态                                            | Boolean | false     |
| data-readonly | 是否设置为只读                                          | Boolean | false     |
| data-editable | 是否可编辑                                              | Boolean | false     |
| id            | 为输入框设置 id 选择器                                  | String  | -         |
| placeholder   | 占位文本                                                | String  | -         |
| up-id         | 为输入框的增加按钮设置 id 选择器                        | String  | -         |
| down-id       | 输入框的减少按钮设置 id 选择器                          | String  | -         |

## InputNumber demo

通过示例代码演示如何获取输入框的值。

```html
<j-input-number id="demoInput" up-id="add" down-id="reduce"></j-input-number>
<script>
  $(() => {
    $("#add,#reduce").click(() => {
      console.log($("#demoInput").val());
    });
  });
</script>
```
