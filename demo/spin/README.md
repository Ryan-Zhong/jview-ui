# Spin 加载中

## 概述

当区块正在获取数据中时可使用，适当的等待动画可以提升用户体验。

## 代码示例

### 基础用法

- 最简单使用 Spin 的方法。

```html
<div class="jv-spin jv-spin-primary" role="status">
    <span class="jv-spin-dot"></span>
</div>
```

### 圆环型

- 默认居中固定

```html
<div class="jv-spin jv-spin-primary" role="status">
     <div class="jv-spin-border ">
          <svg viewBox="25 25 50 50" class="circular">
                <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
          </svg>
     </div>
</div>
```

### 居中固定

- 点状外观的spin在容器内部垂直居中固定，需要父级有relative。

```html
<div class="jv-spin jv-spin-primary jv-spin-fix" role="status">
    <div class="jv-spin-inner">
        <span class="jv-spin-dot"></span>
    </div>
</div>
```

### 各种尺寸

- 通过添加class类名 `jv-spin-small`, `jv-spin-large` 将 Spin 设置为大和小尺寸，不设置为默认（中）尺寸。

```html
<div class="jv-spin jv-spin-primary  jv-spin-small" role="status">
    <span class="jv-spin-dot"></span>
</div>
<div class="jv-spin jv-spin-primary" role="status">
    <span class="jv-spin-dot"></span>
</div>
<div class="jv-spin jv-spin-primary  jv-spin-large" role="status">
    <span class="jv-spin-dot"></span>
</div>
```

### 不同状态

- 不同的颜色状态

```html
<div class="jv-spin jv-spin-primary" role="status">
    <span class="jv-spin-dot"></span>
</div>

<div class="jv-spin jv-spin-success" role="status">
    <span class="jv-spin-dot"></span>
</div>

<div class="jv-spin jv-spin-warning" role="status">
    <span class="jv-spin-dot"></span>
</div>

<div class="jv-spin jv-spin-error" role="status">
    <span class="jv-spin-dot"></span>
</div>

<div class="jv-spin jv-spin-primary" role="status">
     <div class="jv-spin-border ">
          <svg viewBox="25 25 50 50" class="circular">
                <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
          </svg>
     </div>
</div>

<div class="jv-spin jv-spin-success" role="status">
     <div class="jv-spin-border ">
          <svg viewBox="25 25 50 50" class="circular">
                <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
          </svg>
     </div>
</div>

<div class="jv-spin jv-spin-warning" role="status">
     <div class="jv-spin-border ">
          <svg viewBox="25 25 50 50" class="circular">
                <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
          </svg>
     </div>
</div>

<div class="jv-spin jv-spin-error" role="status">
     <div class="jv-spin-border ">
          <svg viewBox="25 25 50 50" class="circular">
                <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
          </svg>
     </div>
</div>
```

### 整页加载

- 使用内置的 `$.Spin` 方法可以全局加载。可设置属性 `type` 改变外观，可选值为 `dot` (默认)，`border`，通过设置属性 `color` 改变spin颜色，可选值为 `priamry` `success` `warning` `error`

```html
<button onclick="loading()">整页加载</button>
<script>
    $(() => {
        loading = () => {
            $.Spin.show();
            setTimeout(() => {
                $.Spin.hide();
            }, 3000);
        }
    });
</script>
```

$.Spin.show(config)
