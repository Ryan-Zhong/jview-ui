# Button 按钮

常用的操作按钮，触发业务逻辑时使用。

## 代码示例

### 基础用法

- 按钮类型有：默认按钮、主按钮、虚线按钮、文字按钮以及五种颜色按钮。

```html
<button type="button" class="jv-btn jv-btn-default">默认按钮</button>
<button type="button" class="jv-btn jv-btn-primary">主要按钮</button>
<button type="button" class="jv-btn jv-btn-info">信息按钮</button>
<button type="button" class="jv-btn jv-btn-success">成功按钮</button>
<button type="button" class="jv-btn jv-btn-warning">警告按钮</button>
<button type="button" class="jv-btn jv-btn-error">危险按钮</button>
<button type="button" class="jv-btn jv-btn-dark">暗色按钮</button>
<button type="button" class="jv-btn jv-btn-dashed">虚线按钮</button>
<button type="button" class="jv-btn jv-btn-text">文字按钮</button>
```

### 反色按钮

- 无背景颜色，在鼠标经过时才显示。提供的按钮类型有：主按钮，信息按钮，警告按钮，危险按钮，暗色按钮

```html
 <button type="button" class="jv-btn jv-btn-outline-primary">主要按钮</button>
 <button type="button" class="jv-btn jv-btn-outline-info">信息按钮</button>
 <button type="button" class="jv-btn jv-btn-outline-success">成功按钮</button>
 <button type="button" class="jv-btn jv-btn-outline-dark">暗色按钮</button>
 <button type="button" class="jv-btn jv-btn-outline-warning">警告按钮</button>
 <button type="button" class="jv-btn jv-btn-outline-error">危险按钮</button>
```

### 圆角按钮

```html
<button type="button" class="jv-btn jv-btn-round jv-btn-default">默认按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-primary">主要按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-info">信息按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-success">成功按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-warning">警告按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-error">危险按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-dark">暗色按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-dashed">虚线按钮</button>
<button type="button" class="jv-btn jv-btn-round jv-btn-text">文字按钮</button>
```

### 长按钮

- 按钮宽度为 100%，常用于弹窗内操作按钮。

```html
<button type="button" class="jv-btn jv-btn-success jv-btn-long">提交</button>
<button type="button" class="jv-btn jv-btn-error jv-btn-long">删除</button>
```

### 按钮尺寸

- 按钮有三种尺寸：大、默认（中）、小。

```html
<button type="button" class="jv-btn jv-btn-primary jv-btn-large">大型按钮</button>
<button type="button" class="jv-btn jv-btn-info">默认按钮</button>
<button type="button" class="jv-btn jv-btn-default jv-btn-small">小型按钮</button>
<button type="button" class="jv-btn jv-btn-primary jv-btn-large jv-btn-circle">大型按钮</button>
<button type="button" class="jv-btn jv-btn-info jv-btn-circle">默认按钮</button>
<button type="button" class="jv-btn jv-btn-default jv-btn-small jv-btn-circle">小型按钮</button>
```

### 禁用状态

- 通过添加disabled属性可将按钮设置为不可用状态。

```html
<button type="button" disabled class="jv-btn jv-btn-default">默认按钮</button>
<button type="button" disabled class="jv-btn jv-btn-primary">主要按钮</button>
<button type="button" disabled class="jv-btn jv-btn-info">信息按钮</button>
<button type="button" disabled class="jv-btn jv-btn-success">成功按钮</button>
<button type="button" disabled class="jv-btn jv-btn-warning">警告按钮</button>
<button type="button" disabled class="jv-btn jv-btn-error">危险按钮</button>
<button type="button" disabled class="jv-btn jv-btn-dark">暗色按钮</button>
<button type="button" disabled class="jv-btn jv-btn-dashed">虚线按钮</button>
<button type="button" disabled class="jv-btn jv-btn-text">文字按钮</button>
```

### 图标按钮

- 带图标的按钮可增强辨识度（有文字）或节省空间（无文字）。

```html
<button type="button" class="jv-btn jv-btn-primary jv-btn-large">
    <i class="jv-icon icon-download"></i>Download
</button>
<button type="button" class="jv-btn jv-btn-default jv-btn-circle-large">
    <i class="jv-icon icon-search"></i>
</button>
<button type="button" class="jv-btn jv-btn-primary jv-btn-circle-large">
    <i class="jv-icon icon-off"></i>
</button>
<button type="button" class="jv-btn jv-btn-info jv-btn-circle-large">
    <i class="jv-icon icon-twitter"></i>
</button>
<button type="button" class="jv-btn jv-btn-success jv-btn-circle-large">
    <i class="jv-icon icon-ok"></i>
</button>
<button type="button" class="jv-btn jv-btn-warning jv-btn-circle-large">
    <i class="jv-icon icon-star-empty"></i>
</button>
<button type="button" class="jv-btn jv-btn-error jv-btn-circle-large">
    <i class="jv-icon icon-trash"></i>
</button>
<button type="button" class="jv-btn jv-btn-primary">
    <i class="jv-icon icon-search"></i>search
</button>
<button type="button" class="jv-btn jv-btn-primary jv-btn-round">
    <i class="jv-icon icon-search"></i>search
</button>
<button type="button" class="jv-btn jv-btn-default">
    <i class="jv-icon icon-search"></i>search
</button>
<button type="button" class="jv-btn jv-btn-default jv-btn-round">
    <i class="jv-icon icon-search"></i>search
</button>
```

### 按钮组

- 以按钮组的方式出现，常用于多项类似操作。

```html
 <div class="jv-btn-group">
     <button type="button" class="jv-btn jv-btn-primary">
        <i class="demo-icon icon-left-open"></i>上一页
     </button>
     <button type="button" class="jv-btn jv-btn-primary">
        下一页<i class="demo-icon icon-right-open"></i>
     </button>
</div>
 <div class="jv-btn-group">
     <button type="button" class="jv-btn jv-btn-primary">
        <i class="demo-icon icon-to-start"></i>
     </button>
     <button type="button" class="jv-btn jv-btn-primary">
        <i class="demo-icon icon-pause"></i>
     </button>
     <button type="button" class="jv-btn jv-btn-primary">
        <i class="demo-icon icon-to-end"></i>
     </button>
</div>
```
