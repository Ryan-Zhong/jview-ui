# BackTop 返回顶部

## 概述

当页面内容冗长，需要快捷返回顶部时使用，一般放置在页面右下角位置。

## 代码示例

### 基础用法

- 默认位置距离页面右部和底部 30px，滚动至距顶端 400px 时显示。

```html
<j-backtop id="backTop"></j-backtop>
<script>
  $(() => {
    $("#backTop").backTop();
  });
</script>
```

### 自定义位置

- 可改变 backTop 组件的位置

```html
<j-backtop id="backTop"></j-backtop>
<script>
  $(() => {
    $("#backTop").backTop({
      bottom: 100,
      right: 60,
    });
  });
</script>
```

## API

- `$(el).backTop(config)`

| 属性     | 说明                                      | 类型   | 默认值 |
| -------- | ----------------------------------------- | ------ | ------ |
| height   | 页面滚动高度达到该值时才显示 BackTop 组件 | Number | 400    |
| bottom   | 组件距离底部的距离                        | Number | 30     |
| right    | 组件距离右部的距离                        | Number | 30     |
| duration | 滚动动画持续时间，单位 毫秒               | Number | 450    |
