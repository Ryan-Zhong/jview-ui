# Divider 分割线

## 概述

- 区隔内容的分割线。对不同章节的文本段落进行分割。对行内文字/链接进行分割，例如表格的操作列。

## 代码示例

### 基础用法

- 分割线默认为为横向

```html
 <div class="jv-divider jv-divider-horizontal"></div>
```

### 设置分割线文本内容

- 提供3个方向：left center right

```html
<div class="jv-divider jv-divider-horizontal">
    <span class="jv-divider-text-left">居左文本</span>
</div>
<div class="jv-divider jv-divider-horizontal">
    <span class="jv-divider-text-center">居中文本</span>
</div>
<div class="jv-divider jv-divider-horizontal">
    <span class="jv-divider-text-right">居右文本</span>
</div>
```

### 垂直分割线

```html
<div class="jv-divider jv-divider-vertical"></div>
```
