# Skeleton骨架屏

## 概述

在需要等待加载内容的位置提供一个占位图形组合。

## 代码示例

### 基础用法

- 最简单的占位效果。

- 设置属性 `data-title-width` 和 `data-paragraph-width` 可以设置标题占位图的宽度和段落占位图的宽度，段落占位图宽度设置接收一个数组，其值对应的每行宽度。

- 若不添加 `data-paragraph-width` 属性则段落占位图宽度全部为100%

```html
<j-skeleton data-title-width="38%" data-paragraph-width="[0,0,61]"></j-skeleton>
```

### 复杂的组合

- 更复杂的组合。设置属性 `data-show-avatar="true"` 显示头像占位图，设置属性 `data-paragraph-row` 设置段落占位图的行数。

```html
<j-skeleton
    data-show-avatar="true"
    data-title-width="50%"
    data-paragraph-row="4"
    data-paragraph-width="[0,0,61]">
</j-skeleton>
```

### 动画效果

- 设置属性 `data-active="true"` 显示动画效果。

```html
<j-skeleton data-active="true" data-title-width="38%" data-paragraph-width="[0,0,61]"></j-skeleton>
<j-skeleton data-active="true" data-show-avatar="true" data-title-width="38%" data-paragraph-row="2"></j-skeleton>
```

## Attributes

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
|  data-active  | 是否展示动画效果 | Boolean | false |
|  data-show-avatar  | 是否显示头像占位图 | Boolean | false |
|  data-show-paragraph  | 是否显示段落占位图 | Boolean | true |
|  data-show-title  | 是否显示标题占位图 | Boolean | true |
|  data-title-width  | 设置标题占位图的宽度，需要自行添加像素单位 | String | - |
|  data-paragraph-row  | 设置段落占位图的行数 | Number | 3 |
|  data-paragraph-width  | 设置段落占位图的宽度，单位默认为百分比无需添加单位。数组的值对应的每行宽度，如果数组中的某个值是 0 则自动设置对应行的宽度为100%。 | Array | 100 |

### 占位图使用示例

- 简单的示例在实际中的应用

```html
<section style="border: 1px dashed #ccc; padding: 10px;">
    <div class="demo-content-box">
        <div class="skeleton-loading">
            <j-skeleton
             data-active="true"
             data-title-width="28%"
             data-paragraph-width="[0,0,50]">
            </j-skeleton>
        </div>
        <div class="content" style="display: none;">
            <h3>标题</h3>
            <div style="color: dodgerblue; margin: 15px 0;">
            这是一段模拟等待从服务器加载完成数据并渲染到页面的内容消息，这是一段等待从服务器加载完成数据并渲染到页面的内容消息。
            </div>
            <button class="jv-btn jv-btn-default" id="demo-btn">显示骨架图</button>
        </div>
    </div>
</section>

<script>
    $(() => {
        //模拟第一次进入页面时区域内容数据正在加载，并显示骨架图
        setTimeout(() => {
            $('.content').fadeIn();
            $('.skeleton-loading').hide();
        }, 3000);
        //点击按钮显示骨架图
        $('#demo-btn').click(() => {
            $('.content').hide();
            $('.skeleton-loading').show();

            setTimeout(() => {
                $('.content').fadeIn();
                $('.skeleton-loading').hide();
            }, 4000);

        });

    });
</script>
```
