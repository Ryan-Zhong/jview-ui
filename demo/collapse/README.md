# Collapse 折叠面板

## 概述

- 将内容区域折叠/展开。

## 代码示例

### 基础用法

- 设置属性  data-default-active-key 与 data-key 相对应的数值可以设置默认展开第几个，不设置默认全部折叠

```html
<j-collapse data-default-active-key="1">
    <j-collapse-panel data-key="1" data-title="维克多· 雨果">
        <p>維克多·馬里·雨果，法國浪漫主義文學的代表人物......</p>
    </j-collapse-panel>
    <j-collapse-panel data-key="2" data-title="海伦凯勒">
        <p>海伦·凯勒（Helen Keller，1880年6月27日－1968年6月1日），美国著名的女作家、教育家、慈善家、社会活动家。</p>
    </j-collapse-panel>
    <j-collapse-panel data-key="3" data-title="艾萨克·牛顿">
        <p>艾薩克·牛頓爵士是一位英格蘭物理學家、數學家、天文學家、自然哲學家和煉金術士.....</p>
    </j-collapse-panel>
</j-collapse>
```

### 手风琴

- 通过设置属性 data-accordion="true" 开启手风琴模式，每次只能打开一个面板。

```html
<j-collapse data-default-active-key="1" data-accordion="true">
    <j-collapse-panel data-key="1" data-title="维克多· 雨果">
        <p>維克多·馬里·雨果，法國浪漫主義文學的代表人物......</p>
    </j-collapse-panel>
    <j-collapse-panel data-key="2" data-title="海伦凯勒">
        <p>海伦·凯勒（Helen Keller，1880年6月27日－1968年6月1日），美国著名的女作家、教育家、慈善家、社会活动家。</p>
    </j-collapse-panel>
    <j-collapse-panel data-key="3" data-title="艾萨克·牛顿">
        <p>艾薩克·牛頓爵士是一位英格蘭物理學家、數學家、天文學家、自然哲學家和煉金術士.....</p>
    </j-collapse-panel>
</j-collapse>
```

### 简洁模式

- 设置属性 data-simple="true" 可以显示为不带边框和背景色的简洁模式。

```html
<j-collapse data-simple="true">
    <j-collapse-panel data-title="维克多· 雨果">
        <p>維克多·馬里·雨果，法國浪漫主義文學的代表人物......</p>
    </j-collapse-panel>
    <j-collapse-panel data-title="海伦凯勒">
        <p>海伦·凯勒（Helen Keller，1880年6月27日－1968年6月1日），美国著名的女作家、教育家、慈善家、社会活动家。</p>
    </j-collapse-panel>
    <j-collapse-panel data-title="艾萨克·牛顿">
        <p>艾薩克·牛頓爵士是一位英格蘭物理學家、數學家、天文學家、自然哲學家和煉金術士.....</p>
    </j-collapse-panel>
</j-collapse>
```

## API

## collapse props

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| data-default-active-key  | 当前激活的面板的key| Number  | -1 |
| data-accordion| 是否开启手风琴模式，开启后每次至多展开一个面板 | Boolean |  false  |
| data-simple| 是否开启简洁模式 | Boolean |  false  |

## collapse-panel props

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| data-title  | 面板标题| String  | - |
| data-key | 当前面板的key，与 Collapse的data-default-active-key对应 | Number | -2 |
