# Input 输入框

## 概述

- 基本表单组件，支持 input 和 textarea，并在原生控件基础上进行了功能扩展，可以组合使用。

## 代码示例

### 基础用法

- 可以直接在wrapper父容器设置 style 来改变输入框的宽度，默认 100%。

```html
<div class="jv-input-wrapper" style="width: 350px;">
    <input type="text" placeholder="请输入内容" class="jv-input">
</div>
```

### 尺寸

- 输入框有三种尺寸：大、默认（中）、小

```html
<div class="jv-input-wrapper">
    <input type="text" placeholder="大型输入框" class="jv-input jv-input-large">
</div>
<div class="jv-input-wrapper">
    <input type="text" placeholder="默认输入框" class="jv-input">
</div>
<div class="jv-input-wrapper">
    <input type="text" placeholder="小型输入框" class="jv-input jv-input-small">
</div>
```

### 可清空

- 通过在wrapper父容器设置属性 `data-clearable="true"` 可显示清空按钮。

```html
<div class="jv-input-wrapper" data-clearable="true">
    <input type="text" placeholder="可清空的输入框" class="jv-input">
</div>
```

### 输入长度限制

- 通过在wrapper父容器设置属性 `data-show-word-limit="true"` 可以显示字数统计，需配合 `maxlength` 属性来限制输入长度。

```html
<div class="jv-input-wrapper" data-show-word-limit="true">
    <input type="text" maxlength="10" placeholder="显示字数统计" class="jv-input jv-input-with-suffix">
</div>
<div class="jv-input-wrapper jv-input-textarea" data-show-word-limit="true">
    <textarea maxlength="100" placeholder="请输入内容" class="jv-input"></textarea>
</div>
```

### 密码框

- 在 `type="password"` 时会自动显示切换密码图标，可以切换显示隐藏密码。

- 不显示切换密码图标，则在wrapper父容器设置属性 `data-show-hide-password="false"`

```html
<div class="jv-input-wrapper">
    <input type="password" placeholder="请输入密码" class="jv-input jv-input-with-suffix">
</div>
```

### 带Icon的输入框

```html
<div class="jv-input-wrapper">
    <span class="jv-input-prefix">
        <i class="jv-icon icon-user-circle-o"></i>
    </span>
    <input type="text" placeholder="请输入姓名" class="jv-input jv-input-with-prefix">
</div><br>
<div class="jv-input-wrapper">
    <input type="text" placeholder="请输入内容" class="jv-input jv-input-with-suffix">
    <span class="jv-input-suffix">
        <i class="jv-icon icon-search"></i>
    </span>
</div>
```

### 搜索框

```html
<div class="jv-input-wrapper">
    <i class="jv-icon jv-input-icon icon-search"></i>
    <input type="search" placeholder="请输入内容" class="jv-input jv-input-with-suffix">
</div>
<div class="jv-input-wrapper jv-input-group jv-input-group-with-append">
    <input type="search" placeholder="请输入内容" class="jv-input">
    <div class="jv-input-group-append jv-input-search">
        <i class="jv-icon icon-search"></i>
    </div>
</div>
<div class="jv-input-wrapper jv-input-group jv-input-group-with-append">
    <input type="search" placeholder="请输入内容" class="jv-input">
    <div class="jv-input-group-append jv-input-search">搜索</div>
</div>
```

### 文本域

- 通过设置属性 `rows` 控制文本域默认显示的行数。

```html
<div class="jv-input-wrapper">
    <textarea rows="8" placeholder="请输入内容" class="jv-input"></textarea>
</div>
```

### 禁用状态

- 通过在wrapper父容器添加class类名 `jv-input-disabled` 设置为不可用状态。

```html
<div class="jv-input-wrapper jv-input-disabled">
    <input type="text" placeholder="请输入内容" class="jv-input">
</div>
<div class="jv-input-wrapper jv-input-disabled">
    <textarea placeholder="请输入内容" class="jv-input"></textarea>
</div>
```

### 复合型输入框

```html
<div class="jv-input-wrapper jv-input-group jv-input-group-with-prepend jv-input-group-with-append">
    <div class="jv-input-group-prepend">http://</div>
    <input type="text" placeholder="请输入网页链接地址" class="jv-input jv-input">
    <div class="jv-input-group-append">.com</div>
</div>

<div class="jv-input-wrapper jv-input-group jv-input-group-with-prepend">
    <div class="jv-input-group-prepend">@</div>
    <input type="text" placeholder="用户名" class="jv-input jv-input">
</div>

<div class="jv-input-wrapper jv-input-group  jv-input-group-with-append">
    <input type="text" placeholder="请输入邮箱" class="jv-input jv-input">
    <div class="jv-input-group-append">@example.com</div>
</div>

<div class="jv-input-wrapper jv-input-group jv-input-group-with-prepend jv-input-group-with-append">
    <div class="jv-input-group-prepend">$</div>
    <input type="text" placeholder="请输入价格" class="jv-input jv-input">
    <div class="jv-input-group-append">.00</div>
</div>

<div class="jv-input-wrapper jv-input-group jv-input-group-with-prepend">
    <div class="jv-input-group-prepend">UN</div>
    <input type="text" placeholder="请输入用户名" class="jv-input jv-input">
</div>

<div class="jv-input-wrapper jv-input-group jv-input-group-with-prepend">
    <div class="jv-input-group-prepend">PW</div>
    <input type="text" placeholder="请输入密码" class="jv-input jv-input">
</div>
```

## API

Input 属性

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| data-clearable  |是否显示清空按钮 | Boolean  | false |
| data-show-word-limit | 是否显示输入字数统计，需要配合 maxlength 使用 | Boolean  | false |
| data-show-hide-password  | 是否显示切换密码图标 | Boolean  | true |
|

Input 原生属性

|  属性   | 说明  | 类型 |  默认值 |
|  ----  | ----  | ---- | ---  |
| autocomplete  | 自动完成功能 | String  | on |
| autofocus | 自动获取焦点 | Boolean  | false |
| spellcheck  |  spellcheck 属性 | Boolean  | true |
| wrap  |  wrap 属性，可选值为 hard 和 soft，仅在 textarea 下生效 | String  | soft |
| rows  | 文本域默认行数，仅在 textarea 类型下有效 | Number  | - |
| placeholder  | 占位文本 | String  | - |
| readonly  | 设置输入框为只读 | Boolean  | false |
| maxlength  | 最大输入长度 | Number  | - |
|
