# Notice 通知提醒

## 概述

在界面右上角显示可关闭的全局通知，常用于以下场景：

- 通知内容带有描述信息

- 系统主动推送

## 代码示例

### 基础用法

- 最简单的用法，4 秒后自动关闭。如果 desc 参数为空或不填，则自动应用仅标题模式下的样式。

- 建议标题言简意赅，例如"删除成功"，更详细的内容可以放在描述信息里。

```js
$.Notice.open({ title: "这是通知标题" });
$.Notice.open({
  title: "这是通知标题",
  desc:
    "This is the content of the notification. This is the content of the notification. ",
});
```

### 带描述信息

- 有四种类型：`info、success、warning、error`

```js
$.Notice.info({
  title: "这是通知标题",
  desc:
    "This is the content of the notification. This is the content of the notification. ",
});
$.Notice.success({
  title: "这是通知标题",
  desc:
    "This is the content of the notification. This is the content of the notification. ",
});
$.Notice.warning({
  title: "这是通知标题",
  desc:
    "This is the content of the notification. This is the content of the notification. ",
});
$.Notice.error({
  title: "这是通知标题",
  desc:
    "This is the content of the notification. This is the content of the notification. ",
});
```

### 自定义时长

- 自定义时长，为 `0` 则不自动关闭。也可以在 `Notice.config()` 中全局配置，详见 API。

```js
$.Notice.open({
  title: "这是通知标题",
  desc: "这条通知不会自动关闭，需要点击关闭按钮才可以关闭。",
  duration: 0,
});
```

### 自定义弹出位置

使用 `placement` 属性定义通知的弹出位置，通知从右上角、右下角、左下角、左上角弹出。

```js
$.Notice.open({
             title:'自定义位置',
             desc:'右上角弹出的消息',
        });
$.Notice.open({
             title:'自定义位置',
             desc:'右下角弹出的消息',
             placement：'bottom-right'
        });
$.Notice.open({
             title:'自定义位置',
             desc:'左下角弹出的消息',
             placement：'bottom-left'
        });
$.Notice.open({
             title:'自定义位置',
             desc:'左上角弹出的消息',
             placement：'top-left'
        });
```

### 使用 HTML 片段

- `desc` 属性支持传入 HTML 片段

```js
$.Notice.info({
  title: "HTML 片段",
  desc: "<strong>这是 <i>HTML</i> 片段</strong>",
});
```

## API

通过直接调用以下方法来使用组件：

- \$.Notice.open(config)
- \$.Notice.info(config)
- \$.Notice.success(config)
- \$.Notice.warning(config)
- \$.Notice.error(config)

以上方法隐式地创建及维护 Notice 组件。参数 config 为对象，具体说明如下：

| 属性      | 说明                                                               | 类型     | 默认值    |
| --------- | ------------------------------------------------------------------ | -------- | --------- |
| title     | 通知提醒的标题                                                     | String   | -         |
| desc      | 通知提醒的内容，为空或不填时，自动应用仅标题模式下的样式           | String   | -         |
| duration  | 自动关闭的延时，单位秒，不关闭可以写 0                             | Number   | 4         |
| key       | 当前通知的唯一标识                                                 | String   | 自动      |
| onClose   | 点击通知关闭按钮时的回调                                           | Function | -         |
| onClick   | 点击通知时触发的回调函数                                           | Function | -         |
| placement | 弹出位置，可选 `top-left` `top-right` `bottom-left` `bottom-right` | string   | top-right |

还提供了提供了全局配置、全局关闭某个通知和全局销毁的方法：

- \$.Notice.config(options)
- \$.Notice.close(key,callback)
- \$.Notice.destroy()

\$.Notice.config 示例

```js
$.Notice.config({
  top: 50,
  duration: 3,
});
```

\$.Notice.close 示例

```js
$.Notice.open({
    title:'这是一条永远不会自动关闭的通知',
    desc:'我永远不会自动关闭。这是一个故意的很长的描述，有很多很多的字和词。',
    duration: 0，
    key: 'demoKey'
});

setTimeout(() => {
    $.Notice.close('demoKey',()=> {
         alert("我通知关闭后的回调事件");
    });
}, 3000);
```

\$.Notice.destroy

- 通过调用这个方法会全局关闭页面所有的通知

| 属性     | 说明                             | 类型   | 默认值 |
| -------- | -------------------------------- | ------ | ------ |
| top      | 通知组件距离顶端的距离，单位像素 | Number | 24     |
| duration | 默认自动关闭的延时，单位秒       | Number | 4      |
