# PageHeader 页头

## 概述

如果页面的路径比较简单，推荐使用页头组件而非面包屑组件。

## 代码示例

### 基础用法

- 标准页头，适合使用在需要简单描述的场景

```html
<j-page-header title="返回" content="详情页面"></j-page-header>
```

- 如果内容过长或较复杂建议使用指定的 `slot` 标签代替

```html
<j-page-header>
    <slot name="title">...</slot>
    <slot name="content">...</slot>
</j-page-header>
```

## Attributes

|  属性  | 说明  |  类型   | 默认值  |
|  ----  | ----  |  ----  |  ----  |
| title  | 标题 | string | — |
| content  | 内容 | string | — |
|

## Slot

|  名称  | 说明  |
|  ----  | ----  |
| title  | 标题内容，会覆盖title属性的内容 |
| content  | 内容，会覆盖content属性的内容 |
|

## Events

|  事件名称  | 说明  |  默认展   |
|  ----  | ----  |  ----  |
| on-back  | 点击左侧区域触发 | - |
|

```html
<j-page-header on-back="demoBack" title="返回" content="详情页面"></j-page-header>
<script>
    $(()=> {
        demoBack = function() {
           //....some codes
        };
    });
</script>
```
