"use strict";
if ("undefined" == typeof jQuery)
  throw Error("JQuery is not introduced on this page");
$(function () {
  function t(t, e) {
    var o = n(e);
    t.attr("onclick", o);
  }
  function n(t) {
    if (e(t)) return t;
  }
  function e(t) {
    var n = /\(\)$/;
    return (
      console.warn(
        "提示！自定义的点击事件回调函数的正确写法: funcName = function() {} / funcName = () => {}; 不要写成 var funcName = () {} 或者 function name() {}"
      ),
      n.test(t)
    );
  }
  var o = function (t) {
      return void 0 === t;
    },
    i = $("<!>"),
    a = function (t) {
      i.append(t), $("body").append(i);
    },
    s = function (t, n) {
      t.after(n), a(t);
    },
    c = { idSelector: /^#/, isFunc: /$()/ },
    d = function (t, n) {
      if ("" === t || "" === n)
        throw new Error(
          'Please check if you are missing the content of "data-toggle" or "data-target". They need to be used together to be effective'
        );
      if (t && n) {
        if (!c.idSelector.test(n))
          throw new Error(
            'The "#" is missing in front of the ' +
              n +
              ", and it only id selector are supported"
          );
        switch (t) {
          case "modal":
            $Modal.show(n);
            break;
          case "drawer":
            $Drawer.show(n);
            break;
          case "popover":
            $(n).hidePopover();
            break;
          case "display":
            var e = $(n).css("display");
            "none" === e
              ? $(n).fadeIn(250)
              : "block" === e && $(n).fadeOut("fast");
        }
      }
    };
  $("*").click(function () {
    var t = $(this).data("toggle"),
      n = $(this).data("target");
    d(t, n);
  });
  var r = function (t, n, e) {
      $(t).map(function (t, o) {
        $(o).removeClass(n).addClass(e),
          setTimeout(function () {
            $(o).remove();
          }, 300);
      });
    },
    l = function (t, n, e) {
      setTimeout(function () {
        "function" == typeof e && e(), t.remove();
      }, 300);
    },
    v = function (t, n, e, o, i, a) {
      "function" == typeof a && a(),
        t.map(function (t, s) {
          n == $(s).data(e) && l($(s), $(s).removeClass(o).addClass(i), a);
        });
    },
    u = function (t, n, e, o, i, a) {
      n <= 0
        ? clearTimeout(t)
        : (t = setTimeout(function () {
            e.removeClass(o).addClass(i),
              "function" == typeof a && a(),
              setTimeout(function () {
                e.remove();
              }, 300);
          }, 1e3 * n));
    },
    f = function (t, n) {
      return Math.floor(
        -1 * ($(n).height() + $(t).height() - 0.7 * $(t).height())
      );
    },
    p = function (t) {
      return Math.floor($(t).height());
    },
    h = function (t) {
      return Math.floor($(t).width());
    },
    m = function (t) {
      return Math.floor(-1 * ($(t).width() + 10));
    },
    g = function (t, n) {
      var e = $(t).width() - $(n).width();
      return [Math.floor((e / 2) * -1), Math.floor(-1 * e)];
    },
    j = function (t, n) {
      var e = $(t).height() - $(n).height();
      return [Math.floor((e / 2) * -1), Math.floor(-1 * e)];
    },
    b = {
      create: function () {
        var t = "jv-input-number",
          n = $("j-input-number");
        n.map(function (e, i) {
          var a = $(i).attr("id") || "",
            c = $(i).data("val") || "",
            d = $(i).data("max") || 1 / 0,
            r = $(i).data("min") || -1 / 0,
            l = 1,
            v = $(i).attr("size") || "default",
            u = $(i).data("step") || 1,
            f = 0,
            p = $(i).data("readonly"),
            h = $(i).data("editable"),
            m = $(i).data("disabled"),
            g = $(i).attr("up-id") || "",
            j = $(i).attr("down-id") || "",
            b = "",
            w = $(i).attr("placeholder") || "";
          "small" === v
            ? ((l = 0), (f = -4))
            : "large" === v && ((l = 2), (f = 1)),
            o(p) && (p = !1),
            o(h) && (h = !1),
            o(m) && (m = !1),
            m && (b = t + "-disabled");
          var k =
            '<div class="' +
            t +
            " " +
            t +
            "-" +
            v +
            " " +
            b +
            '">\n                                        <div class="' +
            t +
            '-handler-container">\n                                            <a class="' +
            t +
            "-handler " +
            t +
            '-handler-up" id="' +
            g +
            '">\n                                                <span class="' +
            t +
            '-handler-up-inner jv-icon icon-up-open" style="top:' +
            l +
            'px"></span>\n                                            </a>\n                                            <a class="' +
            t +
            "-handler " +
            t +
            '-handler-down" id="' +
            j +
            '">\n                                                <span class="' +
            t +
            '-handler-down-inner jv-icon icon-down-open" style="bottom:' +
            f +
            'px"></span>\n                                            </a>\n                                        </div>\n                                        <div class="' +
            t +
            '-input-container">\n                                            <input type="number" placeholder="' +
            w +
            '" value="' +
            c +
            '" max="' +
            d +
            '" min="' +
            r +
            '" step="' +
            u +
            '" class="' +
            t +
            '-input" id="' +
            a +
            '">\n                                        </div>\n                                    </div>';
          s($(n).eq(e), k),
            (p || h) &&
              $("." + t + "-input")
                .eq(e)
                .attr("readonly", !0),
            m &&
              $("." + t + "-input")
                .eq(e)
                .attr("disabled", !0),
            $("." + t).click(function () {
              m ||
                $("." + t)
                  .eq(e)
                  .addClass(t + "-focused");
            }),
            $(document).click(function (n) {
              var o = $("." + t).eq(e);
              o.is(n.target) ||
                0 !== o.has(n.target).length ||
                $("." + t)
                  .eq(e)
                  .removeClass(t + "-focused");
            });
          var C = "";
          r !== -1 / 0 &&
            (C = $("." + t + "-input")
              .eq(e)
              .val()),
            "" !== C ||
              p ||
              h ||
              $("." + t + "-handler-down")
                .eq(e)
                .addClass(t + "-handler-down-disabled"),
            $("." + t + "-input")
              .eq(e)
              .bind("input propertychange", function (n) {
                $("." + t + "-input")
                  .eq(e)
                  .val() >= d && d !== 1 / 0
                  ? ($("." + t + "-input")
                      .eq(e)
                      .val(d),
                    $("." + t + "-handler-up")
                      .eq(e)
                      .addClass(t + "-handler-up-disabled"))
                  : $("." + t + "-handler-up")
                      .eq(e)
                      .removeClass(t + "-handler-up-disabled"),
                  $("." + t + "-input")
                    .eq(e)
                    .val() <= r && d !== -1 / 0
                    ? ($("." + t + "-input")
                        .eq(e)
                        .val(r),
                      $("." + t + "-handler-down")
                        .eq(e)
                        .addClass(t + "-handler-down-disabled"))
                    : $("." + t + "-handler-down")
                        .eq(e)
                        .removeClass(t + "-handler-down-disabled"),
                  (C = $("." + t + "-input")
                    .eq(e)
                    .val());
              }),
            p ||
              ($("." + t + "-handler-up")
                .eq(e)
                .click(function () {
                  if (
                    ((C = $("." + t + "-input")
                      .eq(e)
                      .val()),
                    C++,
                    $("." + t + "-handler-down")
                      .eq(e)
                      .removeClass(t + "-handler-down-disabled"),
                    C > d)
                  )
                    return !1;
                  C >= d &&
                    $("." + t + "-handler-up")
                      .eq(e)
                      .addClass(t + "-handler-up-disabled"),
                    $("." + t + "-input")
                      .eq(e)
                      .val(C);
                }),
              $("." + t + "-handler-down")
                .eq(e)
                .click(function () {
                  if (
                    ((C = $("." + t + "-input")
                      .eq(e)
                      .val()),
                    $("." + t + "-handler-up")
                      .eq(e)
                      .removeClass(t + "-handler-up-disabled"),
                    C <= r + 1 &&
                      $("." + t + "-handler-down")
                        .eq(e)
                        .addClass(t + "-handler-down-disabled"),
                    C <= r)
                  )
                    return !1;
                  $("." + t + "-input")
                    .eq(e)
                    .val((C -= 1));
                }));
        });
      },
    };
  !(function () {
    var t = "jv-loading-bar",
      n = {
        height: 3,
        color: "#2196f3",
        failedColor: "#ff4d4f",
        duration: 250,
      },
      e = null,
      o = 0,
      i = 0,
      a = 0,
      s = $(
        '\n                <div class="' +
          t +
          '" style="height:' +
          n.height +
          'px">\n                    <div class="' +
          t +
          "-inner " +
          t +
          '-inner-color-primary"></div>\n                </div>\n            '
      );
    $("body").append(s);
    var c = $("." + t),
      d = $("." + t + "-inner");
    setTimeout(function () {
      (i = n.duration),
        c.height(n.height + "px"),
        d
          .width(0)
          .height(n.height + "px")
          .css("background-color", "" + n.color);
    }, 0);
    var r = function () {
        e && (clearInterval(e), (e = null));
      },
      l = function () {
        setTimeout(function () {
          (o = 100), d.width(o + "%");
        }, 0),
          setTimeout(function () {
            (o = 0),
              d.fadeOut(),
              setTimeout(function () {
                d.width(o + "%")
                  .css("background-color", n.color)
                  .fadeIn("fast");
              }, 200);
          }, 800);
      },
      v = function (t) {
        "finish" === t && d.css("background-color", n.color),
          "error" === t &&
            setTimeout(function () {
              d.css("background-color", n.failedColor);
            }, 0),
          t && (l(), r());
      },
      u = function () {
        (o = a),
          (e = setInterval(function () {
            if (
              ((o += Math.floor(3 * Math.random() + 1)), o > 95 && r(), o > 100)
            )
              throw (
                (r(),
                new Error(
                  "Because your custom precision value is too large, the progress bar overflows the 100% range. Please reduce the precision value and debug again until no exception occurs"
                ))
              );
            d.fadeIn("fast").width(o + "%");
          }, i));
      };
    $.extend({
      Loading: {
        start: function () {
          if (!e)
            return (
              setTimeout(function () {
                u();
              }, 0),
              this
            );
        },
        finish: function () {
          return v("finish"), this;
        },
        error: function () {
          return v("error"), this;
        },
        update: function (t) {
          return t && ((o = t), (a = t)), this;
        },
        config: function () {
          var t =
              arguments.length > 0 && void 0 !== arguments[0]
                ? arguments[0]
                : {},
            e = t.color,
            o = t.failedColor,
            i = t.height,
            a = t.duration;
          return (
            void 0 === e ? (e = n.color) : (n.color = e),
            void 0 === o ? (o = n.failedColor) : (n.failedColor = o),
            void 0 === i ? (i = n.height) : (n.height = i),
            void 0 === a ? (a = n.duration) : (n.duration = a),
            this
          );
        },
        destroy: function () {
          return r(), $("." + t).remove(), this;
        },
      },
    }),
      (window.$.Loading = $.Loading);
  })(),
    (function () {
      var t = "jv-message",
        n = $('<div class="jv-message"></div>');
      $("body").append(n);
      var e = { top: 12, duration: 3 };
      setTimeout(function () {
        n.css("top", e.top);
      }, 0);
      var o = n.css("z-index"),
        i = function () {
          o++, n.css("z-index", o);
        },
        a = function (o, a, s, c, d, r, v, f) {
          var p = "",
            h = "",
            m = "";
          void 0 === o && (o = "" + new Date().getTime()),
            3 !== e.duration && (c = e.duration),
            s &&
              (h =
                '\n                          <a class="jv-message-close" role="button">\n                              <i class="jv-icon icon-cancel"></i>\n                          </a>\n                      '),
            r && (m = "jv-message-notice-content-with-background"),
            "info" === d && (p = "icon-info-circled"),
            "success" === d && (p = "icon-ok-circled"),
            "warning" === d && (p = "icon-attention"),
            "error" === d && (p = "icon-cancel-circled"),
            "loading" === d && ((d = "info"), (p = "icon-spin5 animate-spin"));
          var g = $(
              '<div class="jv-message-notice ' +
                m +
                '" data-message-key="' +
                o +
                '" role="alert"></div>'
            ),
            j = $(
              '\n                          <div class="jv-message-notice-content">\n                              <div class="jv-message-custom-content jv-message-' +
                d +
                '">\n                                  <i class="jv-icon ' +
                p +
                '"></i>\n                                  <span class="' +
                t +
                '-text">' +
                a +
                "</span>\n                                  " +
                h +
                "\n                              </div>\n                          </div>\n              "
            );
          i(),
            n.append(g.addClass("jv-message-fade-in").append(j)),
            u(null, c, g, "jv-message-fade-in", "jv-message-fade-out", f),
            s &&
              $(".jv-message-notice").map(function (t, n) {
                $(n)
                  .find(".jv-message-close")
                  .click(function () {
                    "function" == typeof v && v(),
                      l(
                        $(n),
                        $(n)
                          .removeClass("jv-message-fade-in")
                          .addClass("jv-message-fade-out")
                      );
                  });
              });
        };
      $.extend({
        Message: {
          info: function (t) {
            var n =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : {},
              e = n.key,
              o = n.duration,
              i = void 0 === o ? 3 : o,
              s = n.closable,
              c = void 0 !== s && s,
              d = n.background,
              r = void 0 !== d && d,
              l = n.onClose,
              v = void 0 === l ? function () {} : l,
              u = n.afterClose;
            return (
              a(e, t, c, i, "info", r, v, void 0 === u ? function () {} : u),
              this
            );
          },
          success: function (t) {
            var n =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : {},
              e = n.key,
              o = n.duration,
              i = void 0 === o ? 3 : o,
              s = n.closable,
              c = void 0 !== s && s,
              d = n.background,
              r = void 0 !== d && d,
              l = n.onClose,
              v = void 0 === l ? function () {} : l,
              u = n.afterClose;
            return (
              a(e, t, c, i, "success", r, v, void 0 === u ? function () {} : u),
              this
            );
          },
          warning: function (t) {
            var n =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : {},
              e = n.key,
              o = n.duration,
              i = void 0 === o ? 3 : o,
              s = n.closable,
              c = void 0 !== s && s,
              d = n.background,
              r = void 0 !== d && d,
              l = n.onClose,
              v = void 0 === l ? function () {} : l,
              u = n.afterClose;
            return (
              a(e, t, c, i, "warning", r, v, void 0 === u ? function () {} : u),
              this
            );
          },
          error: function (t) {
            var n =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : {},
              e = n.key,
              o = n.duration,
              i = void 0 === o ? 3 : o,
              s = n.closable,
              c = void 0 !== s && s,
              d = n.background,
              r = void 0 !== d && d,
              l = n.onClose,
              v = void 0 === l ? function () {} : l,
              u = n.afterClose;
            return (
              a(e, t, c, i, "error", r, v, void 0 === u ? function () {} : u),
              this
            );
          },
          loading: function (t) {
            var n =
                arguments.length > 1 && void 0 !== arguments[1]
                  ? arguments[1]
                  : {},
              e = n.key,
              o = n.duration,
              i = void 0 === o ? 3 : o,
              s = n.closable,
              c = void 0 !== s && s,
              d = n.background,
              r = void 0 !== d && d,
              l = n.onClose,
              v = void 0 === l ? function () {} : l,
              u = n.afterClose;
            return (
              a(e, t, c, i, "loading", r, v, void 0 === u ? function () {} : u),
              this
            );
          },
          config: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.top,
              o = t.duration;
            return (e.top = n), (e.duration = o), this;
          },
          destroy: function (t, n) {
            var e = $(".jv-message-notice");
            return (
              void 0 === t
                ? r(e, "jv-message-fade-in", "jv-message-fade-out")
                : v(
                    e,
                    t,
                    "message-key",
                    "jv-message-fade-in",
                    "jv-message-fade-out",
                    n
                  ),
              this
            );
          },
        },
      }),
        (window.$.Message = $.Message);
    })(),
    (function () {
      var t = "jv-msgbox",
        n = 1010,
        e = {
          info: "icon-info-circled",
          success: "icon-ok-circled",
          warning: "icon-attention",
          error: "icon-cancel-circled",
          confirm: "icon-help-circled",
          loading: "icon-spin6 animate-spin",
        },
        o = function (t, n, e) {
          t.fadeIn("fast"),
            n.fadeIn("fast"),
            e.addClass("jv-msgbox-fade-enter");
        },
        i = function (t, n, e) {
          t.fadeOut("fast"),
            n.fadeOut("fast"),
            e
              .addClass("jv-msgbox-fade-leave")
              .removeClass("jv-msgbox-fade-enter"),
            setTimeout(function () {
              t.remove(), n.remove();
            }, 300),
            $("body").removeAttr("style");
        },
        a = function (a, s, c, d, r, l, v, u, f, p, h) {
          var m = "",
            g = "";
          "info" === a && (m = e.info),
            "success" === a && (m = e.success),
            "warning" === a && (m = e.warning),
            "error" === a && (m = e.error),
            "confirm" === a &&
              ((a = "warning"),
              (m = e.confirm),
              (g =
                '<button class="jv-btn jv-btn-text" id="msgBoxCancelBtn">' +
                l +
                "</button>")),
            h || $("body").css({ paddingRight: 17, overflow: "hidden" });
          var j = $(
            '\n                    <div class="jv-msgbox-mask"></div>\n                    <div class="jv-msgbox-wrap" tabindex="-1" role="document">\n                        <div class="jv-msgbox" style="width: ' +
              d +
              'px;" role="dialog">\n                            <div class="' +
              t +
              '-content">\n                                <div class="' +
              t +
              '-body">\n                                    <div class="' +
              t +
              '-confirm">\n                                        <header class="' +
              t +
              '-confirm-head">\n                                            <div class="' +
              t +
              "-confirm-head-icon " +
              t +
              "-confirm-head-icon-" +
              a +
              '">\n                                                <i class="jv-icon ' +
              m +
              '"></i>\n                                            </div>\n                                            <div class="' +
              t +
              '-confirm-head-title">' +
              s +
              '</div>\n                                        </header>\n                                        <section class="' +
              t +
              '-confirm-body">\n                                            <div>' +
              c +
              '</div>\n                                        </section>\n                                        <footer class="' +
              t +
              '-confirm-footer">\n                                          ' +
              g +
              '\n                                            <button class="jv-btn jv-btn-primary" id="msgBoxOkBtn">' +
              r +
              "</button>\n                                        </footer>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n            "
          );
          n++, $("body").append(j.css("z-index", n));
          var b = $(".jv-msgbox-wrap");
          b.map(function (t, n) {
            var a = $(n).prev(".jv-msgbox-mask"),
              s = $(n).find(".jv-msgbox"),
              c = $(n).find("#msgBoxOkBtn"),
              d = $(n).find("#msgBoxCancelBtn");
            o(a, $(n), s),
              c.click(function () {
                if (("function" == typeof v && v(), p)) {
                  var t = '<i class="jv-icon ' + e.loading + '"></i>';
                  c.addClass("loading").attr("disabled", !0).prepend(t);
                } else i(a, b, s);
              }),
              d.click(function () {
                "function" == typeof u && u(), i(a, b, s);
              });
          }),
            f &&
              $(document).keydown(function (t) {
                27 === t.keyCode && i(mask, b, msgBox);
              });
        };
      $.extend({
        MessageBox: {
          info: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.title,
              e = void 0 === n ? "" : n,
              o = t.content,
              i = void 0 === o ? "" : o,
              s = t.width,
              c = void 0 === s ? 416 : s,
              d = t.okText,
              r = void 0 === d ? "确定" : d,
              l = t.cancelText,
              v = void 0 === l ? "取消" : l,
              u = t.ok,
              f = void 0 === u ? function () {} : u,
              p = t.closable,
              h = void 0 !== p && p,
              m = t.loading,
              $ = void 0 !== m && m,
              g = t.scrollable;
            return (
              a("info", e, i, c, r, v, f, void 0, h, $, void 0 !== g && g), this
            );
          },
          success: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.title,
              e = void 0 === n ? "" : n,
              o = t.content,
              i = void 0 === o ? "" : o,
              s = t.width,
              c = void 0 === s ? 416 : s,
              d = t.okText,
              r = void 0 === d ? "确定" : d,
              l = t.cancelText,
              v = void 0 === l ? "取消" : l,
              u = t.ok,
              f = void 0 === u ? function () {} : u,
              p = t.closable,
              h = void 0 !== p && p,
              m = t.loading,
              $ = void 0 !== m && m,
              g = t.scrollable;
            return (
              a("success", e, i, c, r, v, f, void 0, h, $, void 0 !== g && g),
              this
            );
          },
          warning: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.title,
              e = void 0 === n ? "" : n,
              o = t.content,
              i = void 0 === o ? "" : o,
              s = t.width,
              c = void 0 === s ? 416 : s,
              d = t.okText,
              r = void 0 === d ? "确定" : d,
              l = t.cancelText,
              v = void 0 === l ? "取消" : l,
              u = t.ok,
              f = void 0 === u ? function () {} : u,
              p = t.closable,
              h = void 0 !== p && p,
              m = t.loading,
              $ = void 0 !== m && m,
              g = t.scrollable;
            return (
              a("warning", e, i, c, r, v, f, void 0, h, $, void 0 !== g && g),
              this
            );
          },
          error: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.title,
              e = void 0 === n ? "" : n,
              o = t.content,
              i = void 0 === o ? "" : o,
              s = t.width,
              c = void 0 === s ? 416 : s,
              d = t.okText,
              r = void 0 === d ? "确定" : d,
              l = t.cancelText,
              v = void 0 === l ? "取消" : l,
              u = t.ok,
              f = void 0 === u ? function () {} : u,
              p = t.closable,
              h = void 0 !== p && p,
              m = t.loading,
              $ = void 0 !== m && m,
              g = t.scrollable;
            return (
              a("error", e, i, c, r, v, f, void 0, h, $, void 0 !== g && g),
              this
            );
          },
          confirm: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.title,
              e = void 0 === n ? "" : n,
              o = t.content,
              i = void 0 === o ? "" : o,
              s = t.width,
              c = void 0 === s ? 416 : s,
              d = t.okText,
              r = void 0 === d ? "确定" : d,
              l = t.cancelText,
              v = void 0 === l ? "取消" : l,
              u = t.ok,
              f = void 0 === u ? function () {} : u,
              p = t.cancel,
              h = void 0 === p ? function () {} : p,
              m = t.closable,
              $ = void 0 !== m && m,
              g = t.loading,
              j = void 0 !== g && g,
              b = t.scrollable;
            return (
              a("confirm", e, i, c, r, v, f, h, $, j, void 0 !== b && b), this
            );
          },
          remove: function () {
            var t = $(".jv-msgbox-mask"),
              n = $(".jv-msgbox-wrap"),
              e = $(".jv-msgbox");
            return i(t, n, e), this;
          },
        },
      }),
        (window.$.MessageBox = $.MessageBox);
    })(),
    (function () {
      var t = "jv-notification",
        n = { top: 24, bottom: 24, duration: 4, placement: "top-right" },
        e =
          "M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372 372 166.6 372 372-166.6 372-372 372z",
        o = {
          info: {
            p1: e,
            p2:
              "M464 336a48 48 0 1096 0 48 48 0 10-96 0zm72 112h-48c-4.4 0-8 3.6-8 8v272c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V456c0-4.4-3.6-8-8-8z",
          },
          success: {
            p1:
              "M699 353h-46.9c-10.2 0-19.9 4.9-25.9 13.3L469 584.3l-71.2-98.8c-6-8.3-15.6-13.3-25.9-13.3H325c-6.5 0-10.3 7.4-6.5 12.7l124.6 172.8a31.8 31.8 0 0051.7 0l210.6-292c3.9-5.3.1-12.7-6.4-12.7z",
            p2: e,
          },
          warning: {
            p1: e,
            p2:
              "M464 688a48 48 0 1096 0 48 48 0 10-96 0zm24-112h48c4.4 0 8-3.6 8-8V296c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v272c0 4.4 3.6 8 8 8z",
          },
          error: {
            p1:
              "M685.4 354.8c0-4.4-3.6-8-8-8l-66 .3L512 465.6l-99.3-118.4-66.1-.3c-4.4 0-8 4-8 8 0 1.9.7 3.7 1.9 5.2l130.1 155L340.5 670a8.32 8.32 0 00-1.9 5.2c0 4.4 3.6 8 8 8l66.1-.3L512 564.4l99.3 118.4 66 .3c4.4 0 8-4 8-8 0-1.9-.7-3.7-1.9-5.2L554 515l130.1-155c1.2-1.4 1.8-3.3 1.8-5.2z",
            p2:
              "M512 65C264.6 65 64 265.6 64 513s200.6 448 448 448 448-200.6 448-448S759.4 65 512 65zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372 372 166.6 372 372-166.6 372-372 372z",
          },
          close:
            "M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z",
        },
        i = $('<div class="' + t + " " + t + '-top-left"></div>'),
        a = $('<div class="' + t + " " + t + '-top-right"></div>'),
        s = $('<div class="' + t + " " + t + '-bottom-left"></div>'),
        c = $('<div class="' + t + " " + t + '-bottom-right"></div>');
      $("body").append(i, a, s, c),
        setTimeout(function () {
          i.css({ top: n.top + "px", left: 0, bottom: "auto" }),
            a.css({ top: n.top + "px", right: 0, bottom: "auto" }),
            s.css({ top: "auto", left: 0, bottom: n.bottom + "px" }),
            c.css({ top: "auto", right: 0, bottom: n.bottom + "px" });
        }, 0);
      var d = 1005,
        f = "",
        p = function () {
          d++,
            i.css("z-index", d),
            a.css("z-index", d),
            s.css("z-index", d),
            c.css("z-index", d);
        },
        h = function (e, d, r, v, h, m, g, j) {
          var b = "",
            w = "",
            k = "";
          "open" !== e && (k = t + "-notice-with-icon"),
            "info" === e && ((b = o.info.p1), (w = o.info.p2)),
            "success" === e && ((b = o.success.p1), (w = o.success.p2)),
            "warning" === e && ((b = o.warning.p1), (w = o.warning.p2)),
            "error" === e && ((b = o.error.p1), (w = o.error.p2)),
            void 0 === v && (v = n.duration),
            4 !== n.duration && (v = n.duration),
            "" === h && (h = n.placement),
            void 0 === m && (m = "auto_" + Date.now());
          var C = { isLeft: /left$/, isRight: /right$/ };
          C.isRight.test(h) && (f = t + "-fade-enter"),
            C.isLeft.test(h) && (f = t + "-left-fade-enter");
          var x = $(
              '<div class="' +
                t +
                "-notice " +
                t +
                "-notice-closable " +
                f +
                '" data-notice-key="' +
                m +
                '"></div>'
            ),
            y = $(
              '\n                    <div class="' +
                t +
                '-notice-content">\n                        <div class="' +
                k +
                '" role="alert">\n                            <span role="img" class="' +
                t +
                "-notice-icon " +
                t +
                "-notice-icon-" +
                e +
                '">\n                                <svg viewBox="64 64 896 896"  width="1em" height="1em" fill="currentColor">\n                                  <path d="' +
                b +
                '"></path>\n                                  <path d="' +
                w +
                '"></path>\n                                </svg>\n                            </span>\n                            <div class="' +
                t +
                '-notice-title">' +
                d +
                '</div>\n                            <div class="' +
                t +
                '-notice-description">' +
                r +
                '</div>\n                        </div>\n                    </div>\n                    <a tabindex="0" class="' +
                t +
                '-notice-close" role="button">\n                        <span class="' +
                t +
                '-close-x">\n                            <span role="img" class="' +
                t +
                "-close " +
                t +
                '-close-icon">\n                                <svg viewBox="64 64 896 896" width="1em" height="1em" fill="currentColor">\n                                    <path d="' +
                o.close +
                '"></path>\n                                </svg>\n                            </span>\n                        </span>\n                    </a>\n            '
            );
          x.append(y);
          var q = x.find("." + t + "-notice-title");
          switch (("" == r && q.css("margin-bottom", 0), h)) {
            case "top-right":
              a.append(x);
              break;
            case "top-left":
              i.append(x);
              break;
            case "bottom-right":
              c.prepend(x);
              break;
            case "bottom-left":
              s.prepend(x);
              break;
            default:
              a.append(x);
          }
          p(),
            u(null, v, x, f, t + "-fade-leave"),
            x.map(function (n, e) {
              $(e)
                .find("." + t + "-notice-close")
                .click(function () {
                  "function" == typeof g && g(),
                    l(
                      $(e),
                      $(e)
                        .removeClass(f)
                        .addClass(t + "-fade-leave")
                    );
                }),
                $(e).click(function () {
                  "function" == typeof j && j();
                });
            });
        };
      $.extend({
        Notice: {
          open: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.title,
              e = void 0 === n ? "" : n,
              o = t.desc,
              i = void 0 === o ? "" : o,
              a = t.duration,
              s = t.placement,
              c = void 0 === s ? "" : s,
              d = t.key,
              r = t.onClose,
              l = void 0 === r ? function () {} : r,
              v = t.onClick;
            return (
              h("open", e, i, a, c, d, l, void 0 === v ? function () {} : v),
              this
            );
          },
          info: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.title,
              e = void 0 === n ? "" : n,
              o = t.desc,
              i = void 0 === o ? "" : o,
              a = t.duration,
              s = t.placement,
              c = void 0 === s ? "" : s,
              d = t.key,
              r = t.onClose,
              l = void 0 === r ? function () {} : r,
              v = t.onClick;
            return (
              h("info", e, i, a, c, d, l, void 0 === v ? function () {} : v),
              this
            );
          },
          success: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.title,
              e = void 0 === n ? "" : n,
              o = t.desc,
              i = void 0 === o ? "" : o,
              a = t.duration,
              s = t.placement,
              c = void 0 === s ? "" : s,
              d = t.key,
              r = t.onClose,
              l = void 0 === r ? function () {} : r,
              v = t.onClick;
            return (
              h("success", e, i, a, c, d, l, void 0 === v ? function () {} : v),
              this
            );
          },
          warning: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.title,
              e = void 0 === n ? "" : n,
              o = t.desc,
              i = void 0 === o ? "" : o,
              a = t.duration,
              s = t.placement,
              c = void 0 === s ? "" : s,
              d = t.key,
              r = t.onClose,
              l = void 0 === r ? function () {} : r,
              v = t.onClick;
            return (
              h("warning", e, i, a, c, d, l, void 0 === v ? function () {} : v),
              this
            );
          },
          error: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              n = t.title,
              e = void 0 === n ? "" : n,
              o = t.desc,
              i = void 0 === o ? "" : o,
              a = t.duration,
              s = t.placement,
              c = void 0 === s ? "" : s,
              d = t.key,
              r = t.onClose,
              l = void 0 === r ? function () {} : r,
              v = t.onClick;
            return (
              h("error", e, i, a, c, d, l, void 0 === v ? function () {} : v),
              this
            );
          },
          config: function () {
            var t =
                arguments.length > 0 && void 0 !== arguments[0]
                  ? arguments[0]
                  : {},
              e = t.top,
              o = t.bottom,
              i = t.duration,
              a = t.placement;
            return (
              void 0 === e ? (e = n.top) : (n.top = e),
              void 0 === o ? (o = n.bottom) : (n.bottom = o),
              void 0 === i ? (i = n.duration) : (n.duration = i),
              void 0 === a ? (a = n.placement) : (n.placement = a),
              this
            );
          },
          close: function (n, e) {
            return (
              v(
                $("." + t + "-notice"),
                n,
                "notice-key",
                f,
                t + "-fade-leave",
                e
              ),
              this
            );
          },
          destroy: function () {
            return r($("." + t + "-notice"), f, t + "-fade-leave"), this;
          },
        },
      }),
        (window.$.Notice = $.Notice);
    })();
  !(function () {
    var t = function (t, n) {
      var e = "",
        o = 2e3;
      return (
        o++,
        "dot" === t &&
          (e =
            '\n                  <div class="jv-spin-inner">\n                      <span class="jv-spin-dot"></span>\n                  </div>\n                '),
        "border" === t &&
          (e =
            '\n                    <div class=jv-spin-border ">\n                        <svg viewBox="25 25 50 50" class="circular">\n                            <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>\n                        </svg>\n                    </div>\n                '),
        $(
          '<div class=" jv-spin jv-spin-large jv-spin-fix jv-spin-' +
            n +
            ' jv-spin-fullscreen-wrap" role="status">\n                                ' +
            e +
            "\n                            </div>"
        ).css("z-index", o)
      );
    };
    $.extend({
      Spin: {
        show: function () {
          var n =
              arguments.length > 0 && void 0 !== arguments[0]
                ? arguments[0]
                : {},
            e = n.type,
            o = void 0 === e ? "dot" : e,
            i = n.color,
            a = void 0 === i ? "primary" : i,
            s = t(o, a);
          return $("body").append(s), this;
        },
        hide: function () {
          return (
            l(
              $(".jv-spin-fullscreen-wrap"),
              $(".jv-spin-fullscreen-wrap").fadeOut("fast")
            ),
            this
          );
        },
      },
    }),
      (window.$.Spin = $.Spin);
  })();
  !(function () {
    var t = "jv-modal",
      n = {
        create: function () {
          var n = $("j-modal");
          return (
            n.map(function (e, i) {
              i = $(i);
              var a = i.attr("id"),
                c = i.data("width") || "520px",
                d = i.attr("title"),
                r = i.data("ok-text"),
                l = i.data("z-index") || 1e3,
                v = i.html(),
                u = i.data("closable"),
                f = i.data("mask"),
                p = i.data("position") || [20, 0],
                h = i.data("max-height") || "none",
                m = i.data("footer-hide"),
                g = i.data("fullscreen"),
                j = i.data("cancel-text"),
                b = i.data("mask-closable");
              o(u) && (u = !0),
                o(f) && (f = !0),
                o(m) && (m = !1),
                o(g) && (g = !1),
                o(p[1]) && (p[1] = 0),
                o(b) && (b = !0);
              var w =
                '<div class="jv-modal-mask" style="z-index:' +
                l +
                '"></div>\n                          <div class="' +
                t +
                '-wrap" id=' +
                a +
                ' style="z-index:' +
                l +
                '">\n                          <div \n                          role="dialog" \n                          class="' +
                t +
                '"\n                          style="width:' +
                c +
                ";top:" +
                p[0] +
                "px;left:" +
                p[1] +
                'px;">\n                              <div class="' +
                t +
                '-main-box">\n                                  <a href="javascript:0" class="' +
                t +
                '-close">\n                                      <i class="jv-icon icon-cancel"></i>\n                                  </a>\n                                  <div class="' +
                t +
                '-header">\n                                      <div class="' +
                t +
                '-header-title">' +
                d +
                '</div>\n                                  </div>\n                                  <div class="' +
                t +
                '-body" style="max-height:' +
                h +
                '">' +
                v +
                '</div>\n                                  <div class="' +
                t +
                '-footer">\n                                      <button class="jv-btn jv-btn-text ' +
                t +
                '-cancel">' +
                j +
                '</button>\n                                      <button class="jv-btn jv-btn-primary ' +
                t +
                '-ok">' +
                r +
                "</button>\n                                  </div>\n                              </div>\n                          </div>\n                      </div> ";
              s(n.eq(e), w),
                $(".jv-modal-ok").click(function () {
                  k();
                }),
                $(".jv-modal-cancel").click(function () {
                  k();
                });
              var k = function () {
                $(".jv-modal").eq(e).hasClass("jv-modal-fullscreen")
                  ? $(".jv-modal")
                      .eq(e)
                      .addClass("jv-modal-fullscreen-leave")
                      .removeClass("jv-modal-fullscreen-enter")
                      .fadeOut()
                  : $(".jv-modal")
                      .eq(e)
                      .addClass("jv-modal-leave")
                      .removeClass("jv-modal-enter")
                      .fadeOut("fast"),
                  $(".jv-modal-mask").eq(e).fadeOut("fast"),
                  $(".jv-modal-wrap").eq(e).fadeOut();
              };
              !(function () {
                o(d) && $(".jv-modal-header").eq(e).remove();
              })(),
                (function () {
                  u
                    ? ($(".jv-modal-close").click(function () {
                        k();
                      }),
                      $(document).keydown(function (t) {
                        27 === t.keyCode && k();
                      }))
                    : u || $(".jv-modal-close").eq(e).remove();
                })(),
                (function () {
                  g && $(".jv-modal").eq(e).addClass("jv-modal-fullscreen");
                })(),
                (function () {
                  f ||
                    ((b = !1),
                    $(".jv-modal-mask").eq(e).addClass("jv-modal-mask-hide"),
                    $(".jv-modal-wrap")
                      .eq(e)
                      .addClass("jv-modal-no-mask")
                      .css("z-index", 0));
                })(),
                (function () {
                  m && $(".jv-modal-footer").eq(e).remove();
                })(),
                (function () {
                  b &&
                    $(document).click(function (t) {
                      var n = $(".jv-modal-wrap").eq(e);
                      n.is(t.target) && 0 === n.has(t.target).length && k();
                    });
                })();
            }),
            this
          );
        },
        show: function (t) {
          var n =
              arguments.length > 1 && void 0 !== arguments[1]
                ? arguments[1]
                : {},
            e = n.onlyHandleCb,
            o = void 0 !== e && e,
            i = n.ok,
            a = void 0 === i ? function () {} : i,
            s = n.cancel,
            c = void 0 === s ? function () {} : s;
          t = $(t);
          var d = t.attr("id"),
            r = t.find($("button")),
            l = r.eq(1).attr("id", d + "-ok-btn"),
            v = t.css("z-index"),
            u = t.prev($(".jv-modal-mask")),
            f = r.eq(0).attr("id", d + "-cancel-btn"),
            p = t.find($(".jv-modal"));
          return (
            o
              ? (l.click(a), f.click(c))
              : (v++,
                u.css("z-index", v).fadeIn("fast"),
                t.fadeIn("fast").css("z-index", v),
                p.hasClass("jv-modal-fullscreen")
                  ? p
                      .addClass("jv-modal-fullscreen-enter")
                      .removeClass("jv-modal-fullscreen-leave")
                      .show()
                  : p
                      .addClass("jv-modal-enter")
                      .removeClass("jv-modal-leave")
                      .show(),
                l.click(a),
                f.click(c)),
            this
          );
        },
        responsiveModal: function () {
          return (
            $(window).resize(function () {
              $(window).width() < 768
                ? $(".jv-modal").addClass("jv-modal-auto")
                : $(".jv-modal").removeClass("jv-modal-auto");
            }),
            this
          );
        },
      };
    n.create().responsiveModal(), (window.$Modal = n);
  })(),
    (function () {
      var t = $("j-alert");
      t.map(function (n, e) {
        var i = {
          msg: $(e).attr("message") || "",
          icon: $(e).data("icon") || "",
          desc: $(e).attr("desc") || "",
          type: $(e).attr("type") || "info",
          banner: $(e).data("banner"),
          closable: $(e).data("closable"),
          showIcon: $(e).data("show-icon"),
          closeText: $(e).data("close-text") || "",
        };
        o(i.banner) ? (i.banner = !1) : i.banner,
          o(i.closable) ? (i.closable = !1) : i.closable,
          o(i.showIcon) ? (i.showIcon = !1) : i.showIcon;
        var a = {
            msg: $(e).find('slot[name="message"]'),
            desc: $(e).find('slot[name="desc"]'),
            icon: $(e).find('slot[name="icon"]'),
          },
          c =
            '<div class="jv-alert jv-alert-' +
            i.type +
            '" role="alert">\n                                  <span class="jv-alert-icon"></span>\n                                  <span class="jv-alert-message">' +
            i.msg +
            '</span>\n                                  <span class="jv-alert-description">' +
            i.desc +
            '</span>\n                                  <a class="jv-alert-close" role="button">\n                                      <i class="jv-icon icon-cancel"></i>\n                                  </a>\n                              </div>';
        s($(t).eq(n), c);
        var d = $(".jv-alert").eq(n),
          r = d.find(".jv-alert-close"),
          v = d.find(".jv-alert-icon");
        if (
          (r.click(function () {
            l(d, d.slideUp("fast"));
          }),
          void 0 !== a.msg.html() &&
            d.find(".jv-alert-message").empty().html(a.msg.html()),
          ("" === i.desc && void 0 === a.desc.html()) ||
            d.addClass("jv-alert-with-description"),
          void 0 !== a.desc.html() &&
            d.find(".jv-alert-description").empty().html(a.desc.html()),
          i.showIcon)
        ) {
          switch ((d.addClass("jv-alert-with-icon"), i.type)) {
            case "success":
              v.html('<i class="jv-icon icon-ok-circled"></i>');
              break;
            case "warning":
              v.html('<i class="jv-icon icon-attention"></i>');
              break;
            case "error":
              v.html('<i class="jv-icon icon-cancel-circled"></i>');
              break;
            default:
              v.html('<i class="jv-icon icon-info-circled"></i>');
          }
          "" !== i.icon && v.empty().html(i.icon),
            void 0 !== a.icon.html() && v.empty().html(a.icon.html());
        } else d.addClass("jv-alert-no-icon");
        i.banner && d.addClass("jv-alert-with-banner"),
          i.closable
            ? (d.addClass("jv-alert-closable"),
              "" !== i.closeText &&
                d.find(".jv-alert-close").empty().html(i.closeText))
            : d.find(".jv-alert-close").remove();
      });
    })(),
    (function () {
      $(".jv-avatar-text").map(function (t, n) {
        n = $(n);
        var e = n.parent(".jv-avatar").width(),
          o = n.width(),
          i = n.text().length,
          a = 0;
        o >= e &&
          ((a = Math.floor(o / i)), a < 8 && (a = 8), n.css("font-size", a));
      });
    })(),
    (function () {
      var t = $("j-backtop"),
        n = function (t, n, e) {
          var o = $(
            '\n                <div class="jv-backtop" id="' +
              e +
              '" style="bottom: ' +
              t +
              "px; right: " +
              n +
              'px; display:none;">\n                    <i class="jv-icon icon-play jv-backtop-icon"></i>\n                </div>\n            '
          );
          return (
            $.fn.extend({
              backTop: function () {
                var t =
                    arguments.length > 0 && void 0 !== arguments[0]
                      ? arguments[0]
                      : {},
                  n = t.bottom,
                  e = void 0 === n ? 30 : n,
                  i = t.right,
                  a = void 0 === i ? 30 : i,
                  s = t.duration,
                  c = void 0 === s ? 800 : s,
                  d = t.height,
                  r = void 0 === d ? 400 : d,
                  l = t.onClick,
                  v = void 0 === l ? function () {} : l,
                  u = o;
                return (
                  u
                    .css({ bottom: e + "px", right: a + "px" })
                    .click(function () {
                      "function" == typeof v && v(),
                        $("html,body").animate({ scrollTop: 0 }, c);
                    }),
                  $(document).scroll(function () {
                    Math.floor($(document).scrollTop()) >= r
                      ? u.fadeIn("fast")
                      : u.fadeOut("fast");
                  }),
                  this
                );
              },
            }),
            o
          );
        };
      t.map(function (e, o) {
        var i = n(30, 30, $(o).attr("id"));
        setTimeout(function () {
          s($(t).eq(e), i);
        }, 0);
      });
    })(),
    (function () {
      $(".jv-badge").map(function (t, n) {
        n = $(n);
        var e = n.data("max") || 99,
          o = n.children("sup"),
          i = o.text();
        i > e && o.text(e + "+"),
          ("0" === i) | ("" === i) ? o.hide() : o.fadeIn("fast"),
          o.hasClass("jv-badge-dot") && o.html(""),
          o.bind("DOMNodeInserted", function (t) {
            var n = $(t.target).text();
            n > e && o.text(e + "+"),
              ("0" === n) | ("" === n) ? o.hide() : o.fadeIn("fast");
          });
      });
    })(),
    (function () {
      function t(t, e, o) {
        t.hasClass(n + "-item-active")
          ? t.removeClass(n + "-item-active")
          : t.addClass(n + "-item-active"),
          e.hasClass(n + "-header-active")
            ? e.removeClass(n + "-header-active")
            : e.addClass(n + "-header-active"),
          o.slideToggle(250);
      }
      var n = "jv-collapse",
        e = $("j-collapse");
      e.map(function (o, i) {
        var a = $(i).find($("j-collapse-panel")),
          c = $("<div></div>").attr("class", "" + n),
          d = $("<ul></ul>");
        c.append(d);
        var r = $(i).data("set-active-key") || -1,
          l = $(i).data("simple") || !1,
          v = $(i).data("accordion") || !1;
        a.map(function (e, o) {
          var i = $(o).attr("title") || "",
            a = $(o).data("key") || -2,
            s = $(o).html(),
            u = $("<li></li>").attr("class", n + "-item"),
            f = $("<div></div>")
              .attr("class", n + "-header")
              .html('<i class="jv-icon icon-right-open"></i> ' + i),
            p = $("<div></div>").attr("class", n + "-content"),
            h = $("<div></div>")
              .attr("class", n + "-content-box")
              .html("" + s);
          u.append(f, p),
            p.append(h),
            d.append(u),
            (function () {
              a === r &&
                (p.css("display", "block"),
                f.addClass(n + "-header-active"),
                u.addClass(n + "-item-active")),
                l &&
                  (c.addClass(n + "-simple"), u.addClass(n + "-simple-header")),
                v
                  ? f.click(function () {
                      u.siblings().removeClass(n + "-item-active"),
                        u.siblings().hasClass(n + "-item-active") ||
                          u
                            .removeClass(n + "-simple-header")
                            .siblings()
                            .addClass(n + "-simple-header")
                            .find("." + n + "-content")
                            .slideUp(),
                        t(u, f, p);
                    })
                  : f.click(function () {
                      t(u, f, p);
                    });
            })();
        }),
          s(e.eq(o), c);
      });
    })(),
    (function () {
      var t = "jv-dropdown",
        n = $("." + t),
        e = !1,
        i = /^top/,
        a = /left$/,
        s = /right$/,
        c = /^bottom/,
        d = function (n, e, o) {
          v(o, e, n),
            e.addClass(t + "-slide-enter").removeClass(t + "-slide-out");
        },
        r = function (n) {
          n.addClass(t + "-slide-out").removeClass(t + "-slide-enter");
        },
        l = function (t, n) {
          i.test(n) && t.css("transformOrigin", "center bottom"),
            c.test(n) && t.css("transformOrigin", "center top");
        },
        v = function (t, n, e) {
          var o = f(e, n),
            d = g(n, e);
          i.test(t) && n.css({ top: o - 5 + "px", willChange: "top,left" }),
            c.test(t) && n.css("will-change", "top,left"),
            s.test(t) && n.css({ right: 5, willChange: "top,right" }),
            a.test(t) && n.css({ left: 3, willChange: "top,left" }),
            ("top" !== t && "bottom" !== t) ||
              n.css({ left: d[0] + "px", willChange: "top,left" });
        },
        u = function (t, n, o, i, a) {
          "hover" === o &&
            (i.css("margin", 0),
            t
              .mouseenter(function () {
                d(n, i, a);
              })
              .mouseleave(function () {
                r(i);
              })),
            ("click" !== o && "contextMenu" !== o) ||
              $(document).click(function (n) {
                var o = t;
                o.is(n.target) ||
                  0 !== o.has(n.target).length ||
                  ((e = !1), r(i));
              }),
            "click" === o &&
              n.click(function () {
                d(n, i, a);
              }),
            "contextMenu" === o &&
              n.contextmenu(function () {
                return e ? ((e = !1), r(i)) : ((e = !0), d(n, i, a)), !1;
              });
        },
        p = function (t, n) {
          t.click(function () {
            (e = !1), r(n);
          });
        };
      n.map(function (t, n) {
        var e = $(n).data("placement"),
          i = $(n).data("trigger"),
          a = $(n).find(".jv-dropdown-rel"),
          s = $(n).find(".jv-dropdown-select-dropdown"),
          c = s.find(".jv-dropdown-item");
        o(e) && (e = "bottom"),
          o(i) && (i = "hover"),
          u($(n), a, i, s, e),
          l(s, e),
          p(c, s);
      });
    })(),
    (function () {
      $(".jv-input-wrapper").map(function (t, n) {
        var e = $(n).find(".jv-input"),
          i = e.attr("minlength") || 0,
          a = e.attr("maxlength") || 0,
          s = $(n).data("clearable"),
          c = $(n).data("show-word-limit"),
          d = $(n).data("show-hide-password");
        if (
          (o(s) && (s = !1),
          o(d) && (d = !0),
          $(n).hasClass("jv-input-disabled") && e.attr("readonly", !0),
          c)
        ) {
          var r = $(
            '<span class="jv-input-with-word-count">' + i + "/" + a + "</span>"
          );
          e.bind("input propertychange", function (t) {
            r.html(e.val().length + "/" + a);
          }),
            $(n).append(r);
        }
        if (s) {
          var l = function (t) {
              t ? v.hide() : v.show();
            },
            v = $('<i class="jv-icon jv-input-icon icon-cancel-circled"></i>');
          "" === e.val() || l(!1),
            e
              .before(
                v.hide().click(function () {
                  l(!0), e.val("");
                })
              )
              .bind("input propertychange", function (t) {
                l("" !== e.val() ? !1 : !0);
              });
        }
        if ("password" === e.attr("type") && d) {
          var u = $('<i class="jv-icon jv-input-icon icon-eye-on"></i>'),
            f = !0;
          e.before(
            u.click(function () {
              f
                ? ((f = !1),
                  u.addClass("icon-eye-off").removeClass("icon-eye-on"),
                  e.attr("type", "text"))
                : ((f = !0),
                  u.addClass("icon-eye-on").removeClass("icon-eye-off"),
                  e.attr("type", "password"));
            })
          );
        }
      });
    })(),
    b.create(),
    (function () {
      var t = "jv-page-header",
        n = function (n, e, o, i, a) {
          return $(
            '\n                <header class="' +
              t +
              '">\n                    <div class="' +
              t +
              "-left " +
              e +
              '" id="' +
              n +
              '" onclick="' +
              a +
              '">\n                        <i class="jv-icon icon-left ' +
              t +
              '-back"></i>\n                        <span class="' +
              t +
              '-title">' +
              o +
              '</span>\n                    </div>\n                    <div class="' +
              t +
              '-content">' +
              i +
              "</div>\n                </header>\n          "
          );
        };
      $("j-page-header").map(function (t, e) {
        var o = $(e).attr("id") || "",
          i = $(e).attr("class") || "",
          a = $(e).attr("title") || "",
          c = $(e).attr("on-back") || "",
          d = $(e).attr("content") || "",
          r = {
            title: $(e).find('slot[name="title"]').html(),
            content: $(e).find('slot[name="content"]').html(),
          };
        r.title && (a = r.title), r.content && (d = r.content);
        var l = n(o, i, a, d, c);
        s($(e).eq(t), l);
      });
    })(),
    (function () {
      var t = "jv-popover",
        n = $("j-popover");
      $.fn.extend({
        showPopover: function () {
          $(this)
            .addClass("jv-popover-fade-enter")
            .removeClass("jv-popover-fade-out");
        },
        hidePopover: function () {
          $(this)
            .addClass("jv-popover-fade-out")
            .removeClass("jv-popover-fade-enter");
        },
      });
      var e = function (n, e, o, i, a) {
          var s = "",
            c = "";
          return (
            e &&
              (s =
                '<div class="jv-popover-title"><span>' + e + "</span></div>"),
            o &&
              (c =
                '\n                      <div class="jv-popover-inner-content">\n                          <div style="overflow: auto;">' +
                o +
                "</div>\n                      </div>\n                  "),
            $(
              '\n                      <div class="jv-popover-wrap">\n                          <div class="jv-popover-rel">' +
                n +
                '</div>\n                          <div class="' +
                t +
                " " +
                t +
                '-fade-out" id="' +
                a +
                '" role="tooltip" x-placement="' +
                i +
                '">\n                              <div class="' +
                t +
                '-content">\n                                  <div class="' +
                t +
                '-arrow"></div>\n                                  <div class="' +
                t +
                '-inner">\n                                  ' +
                s +
                "\n                                  " +
                c +
                "\n                                  </div>\n                              </div>\n                          </div>\n                      </div>\n              "
            )
          );
        },
        o = function (t) {
          return {
            id: t.attr("id") || "",
            title: t.attr("title") || "",
            content: t.attr("content") || "",
            triggerEl: t.html() || "",
            placement: t.attr("placement") || "top",
            triggerMode: t.attr("trigger") || "click",
          };
        },
        i = function (t) {
          return {
            title: t.find('slot[name="title"]').html(),
            content: t.find('slot[name="content"]').html(),
          };
        },
        a = function (t, n, e) {
          var o = {
              top: /^top/,
              left: /^left/,
              right: /^right/,
              bottom: /^bottom/,
            },
            i = f(t, n),
            a = m(n),
            s = h(t),
            c = p(t),
            d = g(n, t),
            r = j(n, t);
          o.top.test(e) && n.css("top", i + "px"),
            o.left.test(e) && n.css("left", a + "px"),
            o.right.test(e) && n.css("left", s + "px"),
            o.bottom.test(e) && n.css("top", c + "px"),
            ("top" !== e && "bottom" !== e) || n.css("left", d[0] + "px"),
            ("top-right" !== e && "bottom-right" !== e) ||
              n.css("left", d[1] + "px"),
            ("left" !== e && "right" !== e) || n.css("top", r[0] + "px"),
            ("left-bottom" !== e && "right-bottom" !== e) ||
              n.css("top", r[1] + "px");
        },
        c = function (t, n, e, o) {
          "focus" === t &&
            $(n)
              .mousedown(function () {
                $(e)
                  .addClass("jv-popover-fade-enter")
                  .removeClass("jv-popover-fade-out"),
                  a(n, e, o);
              })
              .mouseup(function () {
                $(e)
                  .addClass("jv-popover-fade-out")
                  .removeClass("jv-popover-fade-enter");
              }),
            "click" === t &&
              ($(n)
                .find(".jv-popover-rel")
                .click(function () {
                  $(e)
                    .addClass("jv-popover-fade-enter")
                    .removeClass("jv-popover-fade-out"),
                    a(n, e, o);
                }),
              $(document).click(function (t) {
                var o = $(n);
                o.is(t.target) ||
                  0 !== o.has(t.target).length ||
                  $(e)
                    .addClass("jv-popover-fade-out")
                    .removeClass("jv-popover-fade-enter");
              })),
            "hover" === t &&
              $(n)
                .mouseenter(function () {
                  $(e)
                    .addClass("jv-popover-fade-enter")
                    .removeClass("jv-popover-fade-out"),
                    a(n, e, o);
                })
                .mouseleave(function () {
                  $(e)
                    .addClass("jv-popover-fade-out")
                    .removeClass("jv-popover-fade-enter");
                });
        },
        d = function (t, n, e, o) {
          a(n, e, o),
            c(t, n, e, o),
            n.find('slot[name="title"').remove(),
            n.find('slot[name="content"').remove();
        };
      !(function () {
        n.map(function (t, a) {
          var c = o($(a)),
            r = i($(a));
          r.title && (c.title = r.title), r.content && (c.content = r.content);
          var l = e(c.triggerEl, c.title, c.content, c.placement, c.id);
          s($(n).eq(t), l);
          var v = $(".jv-popover-wrap").eq(t),
            u = v.find(".jv-popover");
          d(c.triggerMode, v, u, c.placement);
        });
      })();
    })(),
    (function () {
      var n = "jv-popover",
        e = $("j-popconfirm"),
        o = function (t, e, o, i, a) {
          var s = "";
          return (
            e && (s = '<div class="jv-popover-message-title">' + e + "</div>"),
            $(
              '\n                      <div class="jv-popover-wrap">\n                          <div class="jv-popover-rel">' +
                t +
                '</div>\n                          <div class="' +
                n +
                " " +
                n +
                '-fade-out" role="tooltip" x-placement="' +
                o +
                '">\n                              <div class="' +
                n +
                '-content">\n                                  <div class="' +
                n +
                '-arrow"></div>\n                                  <div class="' +
                n +
                '-inner">\n                                      <div class="' +
                n +
                '-inner-content">\n                                          <div class="' +
                n +
                '-message">\n                                            <i class="jv-icon icon-attention"></i>\n                                            ' +
                s +
                '\n                                          </div>\n                                          <div class="' +
                n +
                '-buttons">\n                                              <button class="jv-btn jv-btn-text jv-btn-small" id="pop-cancelBtn">' +
                a +
                '</button>\n                                              <button class="jv-btn jv-btn-primary jv-btn-small" id="pop-okBtn">' +
                i +
                "</button>\n                                          </div>\n                                      </div>\n                                  </div>\n                              </div>\n                          </div>\n                      </div>\n              "
            )
          );
        },
        i = function (t) {
          return {
            title: t.attr("title") || "",
            okText: t.attr("ok-text") || "确定",
            cancelText: t.attr("cancel-text") || "取消",
            triggerEl: t.html() || "",
            placement: t.attr("placement") || "top",
            okClickFuncName: t.attr("onConfirm"),
            cancelClickFuncName: t.attr("onCancel"),
          };
        },
        a = function (t) {
          return { title: t.find('slot[name="title"]').html() };
        },
        c = function (t, n, e) {
          var o = {
              top: /^top/,
              left: /^left/,
              right: /^right/,
              bottom: /^bottom/,
            },
            i = f(t, n),
            a = m(n),
            s = h(t),
            c = p(t),
            d = g(n, t),
            r = j(n, t);
          o.top.test(e) && n.css("top", i + "px"),
            o.left.test(e) && n.css("left", a + "px"),
            o.right.test(e) && n.css("left", s + "px"),
            o.bottom.test(e) && n.css("top", c + "px"),
            ("top" !== e && "bottom" !== e) || n.css("left", d[0] + "px"),
            ("top-right" !== e && "bottom-right" !== e) ||
              n.css("left", d[1] + "px"),
            ("left" !== e && "right" !== e) || n.css("top", r[0] + "px"),
            ("left-bottom" !== e && "right-bottom" !== e) ||
              n.css("top", r[1] + "px");
        },
        d = function (t, n, e) {
          $(t)
            .find(".jv-popover-rel")
            .click(function () {
              $(n)
                .addClass("jv-popover-fade-enter")
                .removeClass("jv-popover-fade-out"),
                c(t, n, e);
            }),
            $(document).click(function (e) {
              var o = $(t);
              o.is(e.target) ||
                0 !== o.has(e.target).length ||
                $(n)
                  .addClass("jv-popover-fade-out")
                  .removeClass("jv-popover-fade-enter");
            });
        },
        r = function (n, e, o) {
          var i = $(n).find("#pop-okBtn"),
            a = $(n).find("#pop-cancelBtn");
          t(i, e),
            t(a, o),
            i.click(function () {
              $(n)
                .addClass("jv-popover-fade-out")
                .removeClass("jv-popover-fade-enter");
            }),
            a.click(function () {
              $(n)
                .addClass("jv-popover-fade-out")
                .removeClass("jv-popover-fade-enter");
            });
        },
        l = function (t, n, e, o, i) {
          c(t, n, e),
            d(t, n, e),
            r(n, o, i),
            $(t).find('slot[name="title"]').remove();
        };
      !(function () {
        e.map(function (t, n) {
          var c = i($(n)),
            d = a($(n));
          d.title && (c.title = d.title);
          var r = o(c.triggerEl, c.title, c.placement, c.okText, c.cancelText);
          s($(e).eq(t), r);
          var v = $(".jv-popover-wrap").eq(t),
            u = v.find(".jv-popover");
          l(v, u, c.placement, c.okClickFuncName, c.cancelClickFuncName);
        });
      })();
    })(),
    (function () {
      var t = $("j-result"),
        n = {
          info: "icon-info-circled",
          success: "icon-ok-circled",
          warning: "icon-attention",
          error: "icon-cancel-circled",
          403: "icon-lock-alt",
          404: "icon-paper-plane-alt",
          500: " icon-frown",
        },
        e = function (t, e, o, i, a) {
          var s = "",
            c = "",
            d = "",
            r = "";
          return (
            "info" === t && (s = n.info),
            "success" === t && (s = n.success),
            "warning" === t && (s = n.warning),
            "error" === t && (s = n.error),
            "403" === t && (s = n[403]),
            "404" === t && ((s = n[404]), (t = "info")),
            "500" === t && ((s = n[500]), (t = "error")),
            o && (r = '<div class="jv-result-subtitle">' + o + "</div>"),
            a ? (c = a) : (d = '<i class="jv-icon ' + s + '"></i>'),
            $(
              '\n                <section class="jv-result">\n                    <div class="jv-result-icon jv-result-' +
                t +
                '">\n                      ' +
                d +
                "\n                      " +
                c +
                '\n                    </div>\n                    <header class="jv-result-title">' +
                e +
                "</header>\n                    " +
                r +
                '\n                    <div class="jv-result-footer">' +
                i +
                "</div>\n                </section>\n            "
            )
          );
        };
      t.map(function (n, o) {
        var i = $(o).attr("icon") || "",
          a = $(o).attr("title") || "",
          c = $(o).attr("status") || "info",
          d = $(o).attr("footer") || "",
          r = $(o).attr("subTitle") || "",
          l = {
            icon: $(o).find('slot[name="icon"]').html(),
            title: $(o).find('slot[name="title"]').html(),
            footer: $(o).find('slot[name="footer"]').html(),
            subTitle: $(o).find('slot[name="subTitle"]').html(),
          };
        l.icon && (i = l.icon),
          l.title && (a = l.title),
          l.footer && (d = l.footer),
          l.subTitle && (r = l.subTitle);
        var v = e(c, a, r, d, i);
        s($(t).eq(n), v);
      });
    })(),
    (function () {
      var t = "jv-skeleton",
        n = $("j-skeleton");
      n.map(function (e, i) {
        var a = {
          id: $(i).attr("id") || "",
          isActive: $(i).data("active"),
          showTitle: $(i).data("show-title"),
          showAvatar: $(i).data("show-avatar"),
          titleWidth: $(i).data("title-width") || 100,
          showParagraph: $(i).data("show-paragraph"),
          paragraphRow: $(i).data("paragraph-row") || 3,
          paragraphWidth: $(i).data("paragraph-width") || [],
        };
        o(a.isActive) ? (a.isActive = !1) : a.isActive,
          o(a.showTitle) ? (a.showTitle = !0) : a.showTitle,
          o(a.showAvatar) ? (a.showAvatar = !1) : a.showAvatar,
          o(a.showParagraph) ? (a.showParagraph = !0) : a.showParagraph;
        var c = "",
          d = "",
          r = "",
          l = "";
        a.isActive && (c = t + "-active"),
          a.showAvatar &&
            ((l = t + "-with-avatar"),
            (r =
              '<header class="jv-skeleton-header">\n                                        <span class="jv-skeleton-avatar jv-skeleton-avatar-circle"></span>\n                                    </header>'));
        var v =
          '<div class="jv-skeleton ' +
          l +
          " " +
          c +
          '" id="' +
          a.id +
          '">\n                                    ' +
          r +
          '\n                                    <section class="' +
          t +
          '-content">\n                                        <h3 class="' +
          t +
          '-title" style="width:' +
          a.titleWidth +
          ';"></h3>\n                                        <ul class="' +
          t +
          '-paragraph"></ul>\n                                    </section>\n                              </div>';
        if ((s($(n).eq(e), v), a.paragraphRow > 0))
          for (var u = 0; u < a.paragraphRow; u++)
            (void 0 !== a.paragraphWidth[u] && 0 !== a.paragraphWidth[u]) ||
              (a.paragraphWidth[u] = 100),
              (d = "<li style=width:" + a.paragraphWidth[u] + "%></li>"),
              $(".jv-skeleton-paragraph").eq(e).append(d);
      });
    })(),
    (function () {
      for (
        var t = { openColor: "#2196f3", closeColor: "#c5c8ce" },
          n = $("j-switch"),
          e = 0;
        e < n.length;
        e++
      )
        !(function (e) {
          var o = n.eq(e),
            i = o.data("size") || "medium",
            a = o.attr("class") || "",
            c = o.attr("id") || "",
            d = o.data("open") || "",
            r = o.data("close") || "",
            l = o.data("actived") || !1,
            v = o.attr("disabled") || !1,
            u = o.data("true-color") || t.openColor,
            f = o.data("false-color") || t.closeColor,
            p = l,
            h = $(
              "<span \n                              " +
                v +
                '\n                              id="' +
                c +
                '" \n                              tabindex="0"\n                              class="jv-switch jv-switch-' +
                i +
                " " +
                a +
                '"\n                              data-actived=' +
                p +
                '\n                              role="switch">\n                              </span>'
            ),
            m = $("<span class=jv-switch-inner></span>"),
            g = $("<span>" + d + "</span>"),
            j = $("<span>" + r + "</span>"),
            b = $("<span class=jv-switch-ball></span>");
          h.append(m, b).css("background", "" + f), m.append(j, g), s(o, h);
          var w = function (t) {
              t
                ? (g.show(), j.hide(), m.css("left", 7))
                : (g.hide(), j.show(), m.css("left", 23));
            },
            k = function (t) {
              var n = h.width(),
                e = b.width(),
                o = n - e - 3;
              t
                ? b.css("transform", "translateX(" + o + "px)")
                : b.css("transform", "translateX(0)");
            },
            C = function () {
              !1 === p
                ? ((p = !p),
                  h.css("background", "" + u).data("actived", p),
                  h.attr("data-actived", p))
                : ((p = !p),
                  h.css("background", "" + f).data("actived", p),
                  h.attr("data-actived", p)),
                k(p),
                w(p);
            };
          !(function () {
            l &&
              (h.css("background", "" + u).data("actived", l),
              h.attr("data-actived", p)),
              k(l),
              w(l);
          })(),
            v
              ? h.unbind("click")
              : h.click(function () {
                  C();
                });
        })(e);
    })(),
    (function () {
      var t = "jv-timeline",
        n = $("j-timeline");
      n.map(function (e, i) {
        i = $(i);
        var a = i.children("j-timeline-item"),
          c = i.data("pending"),
          d = "";
        o(c) && (c = !1), c && (d = t + "-pending");
        var r = $('<ul class="jv-timeline ' + d + '"></ul>');
        a.map(function (n, e) {
          e = $(e);
          var o = e.attr("color") || "blue",
            i = e.html(),
            a = e.attr("dot") || "",
            s = "";
          a && (s = t + "-item-head-custom");
          var c = $(
            '<li class="jv-timeline-item">\n                                      <div class="jv-timeline-item-tail"></div>\n                                      <div class="jv-timeline-item-head ' +
              s +
              " " +
              t +
              "-item-head-" +
              o +
              '">' +
              a +
              '</div>\n                                      <div class="' +
              t +
              '-item-content">' +
              i +
              "</div>\n                                </li>"
          );
          r.append(c);
        }),
          s(n.eq(e), r);
      });
    })();
});
