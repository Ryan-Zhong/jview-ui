/*!
 * JView v1.0
 * https://gitee.com/Ryan-Zhong/jview-ui
 *
 * Licensed under MIT
 * https://gitee.com/Ryan-Zhong/jview-ui/blob/master/LICENSE
 *
 * Copyright (c) 2020 Ryan 钟
 *
 * Date: 2020-10-04
 */

if (typeof jQuery == "undefined") {
  throw Error("JQuery is not introduced on this page");
}

$(() => {
  const isUndef = (attr) => {
    return attr === undefined;
  };

  //在自定义标签后添加目标dom节点并移除自定义标签
  let commentTag = $(`<!>`);

  const recycle = (customTag) => {
    commentTag.append(customTag);
    $(`body`).append(commentTag);
  };

  const addNode = (customTag, targetTag) => {
    customTag.after(targetTag);
    recycle(customTag);
  };

  //任何元素都能使用
  const eachElement = $(`*`);

  const rules = {
    idSelector: /^#/,
    isFunc: /$()/,
  };

  const toggleEventHandler = (toggle, target) => {
    if (toggle === "" || target === "") {
      throw new Error(
        'Please check if you are missing the content of "data-toggle" or "data-target". They need to be used together to be effective'
      );
    }

    if (toggle && target) {
      //确保是id选择器
      if (!rules.idSelector.test(target)) {
        throw new Error(
          `The "#" is missing in front of the ${target}, and it only id selector are supported`
        );
      }

      //根据toggle的值，做出对应的不同切换类型
      switch (toggle) {
        case "modal":
          $Modal.show(target);
          break;
        case "drawer":
          $Drawer.show(target);
          break;
        case "popover":
          $(target).hidePopover();
          break;
        case "display":
          //通过目标元素的display属性判断是否切换显示或隐藏
          let display = $(target).css("display");
          if (display === "none") {
            $(target).fadeIn(250);
          } else if (display === "block") {
            $(target).fadeOut("fast");
          }
          break;
      }
    }
  };

  eachElement.click(function () {
    const toggle = $(this).data("toggle");
    const target = $(this).data("target");
    toggleEventHandler(toggle, target);
  });

  //对应元素添加属性onclick并加上函数名
  function addClick(el, name) {
    let funcName = getFuncName(name);
    el.attr("onclick", funcName);
  }
  //获取添加的函数名
  function getFuncName(name) {
    if (isFunc(name)) {
      return name;
    }
  }
  //判断元素属性onOk的值是否以 () 结尾
  function isFunc(attrName) {
    const reg = /\(\)$/;
    console.warn(
      "提示！自定义的点击事件回调函数的正确写法: funcName = function() {} / funcName = () => {}; 不要写成 var funcName = () {} 或者 function name() {}"
    );
    return reg.test(attrName);
  }

  /**
   * 全局销毁---销毁所有的指定元素
   * @param {String} el 目标元素
   * @param {String} fadeInCls 移除的入场动画
   * @param {String} fadeOutCls 添加的离场动画
   */
  const globalDestroy = (el, fadeInCls, fadeOutCls) => {
    $(el).map((i, tags) => {
      $(tags).removeClass(fadeInCls).addClass(fadeOutCls);
      setTimeout(() => {
        $(tags).remove();
      }, 300);
    });
  };

  /**
   * 通过关闭按钮将某个元素移除
   * @param {String} el 被移除的元素
   * @param {*} disappearEffect 元素消失过渡效果
   * @param {Function} callback 关闭后的回调事件
   */
  const destroy = (el, disappearEffect, callback) => {
    disappearEffect;
    setTimeout(() => {
      typeof callback === "function" ? callback() : callback;
      el.remove();
    }, 300);
  };

  /**
   * 通过组件的唯一标识key销毁某个实例化组件
   * @param {String} el 目标元素
   * @param {String|Number} key 当前组件元素的唯一标识，与下一个参数做比较用的
   * @param {String} dataAttr 当前组件元素的唯一标识
   * @param {String} enterCls 移除的入场动画类名
   * @param {String} leaveCls 添加的离场动画类名
   * @param {Function} callback 销毁后的回调事件
   */
  const destroyInstanceByKey = (
    el,
    key,
    dataAttr,
    enterCls,
    leaveCls,
    callback
  ) => {
    typeof callback === "function" ? callback() : callback;
    //遍历目标元素，并将key参数的值与目标元素设置的属性data-‘name’-key的值匹配是否一致，如果一致则销毁该元素

    el.map((i, tag) => {
      if (key == $(tag).data(dataAttr)) {
        destroy(
          $(tag),
          $(tag).removeClass(enterCls).addClass(leaveCls),
          callback
        );
      }
    });
  };

  /**
   * 通过定时器销毁元素
   * @param {*} tr 定时器名称
   * @param {Number} t 时间
   * @param {String} el 关闭的目标元素
   * @param {String} fadeInCls 移除入场动画类名
   * @param {String} fadeOutCls 添加出场动画类名
   * @param {Function} callback 关闭后的回调事件
   */
  const destroyElemByTimer = (tr, t, el, fadeInCls, fadeOutCls, callback) => {
    //定时器的时间小于或等于0，则清除定时器不自动关闭message
    if (t <= 0) {
      clearTimeout(tr);
    } else {
      tr = setTimeout(() => {
        el.removeClass(fadeInCls).addClass(fadeOutCls);
        //回调事件
        typeof callback === "function" ? callback() : callback;
        //移除
        setTimeout(() => {
          el.remove();
        }, 300);
      }, t * 1000);
    }
  };

  /**
   * 获取top方向top初始值
   *  start = -（popover自身高度 + 10） 。
   * @param {*} popover 气泡提示容器，它属于子容器
   */
  const getTopStart = (parentContainer, popover) => {
    const top = Math.floor(
      ($(popover).height() +
        $(parentContainer).height() -
        $(parentContainer).height() * 0.7) *
        -1
    );
    return top;
  };
  /**
   * 获取bottom方向bottom初始值
   *  start = 载体容器高度 + 10。
   * @param {*} parentContainer popover的父容器
   */
  const getBottomStart = (parentContainer) => {
    const bottom = Math.floor($(parentContainer).height());
    return bottom;
  };
  /**
   * 获取right方向right初始值
   * left = start = 载体容器宽度 + 10。
   * @param {*} parentContainer popover的父容器
   */
  const getRightStart = (parentContainer) => {
    const right = Math.floor($(parentContainer).width());
    return right;
  };
  /**
   * 获取left方向left初始值
   * left =  start = -（popover自身宽度 + 10）。
   * @param {*} popover 气泡提示容器，它属于子容器
   */
  const getLeftStart = (popover) => {
    const left = Math.floor(($(popover).width() + 10) * -1);
    return left;
  };
  /**
   * 上下方向局中和居右对齐
   * 居中：-（popover自身宽度 - 载体容器宽度） / 2。
   * 居右：-（popover自身宽度 - 载体容器宽度）。
   * 返回值是一个数组，第一个为居中的数值，第二个为居右的数值。
   * @param {*} parentContainer popover的父容器
   * @param {*} popover 气泡提示容器，它属于子容器
   */
  const topBottomAlignRightAndCenter = (popover, parentContainer) => {
    const h = $(popover).width() - $(parentContainer).width();
    const alignCenter = Math.floor((h / 2) * -1);
    const alignRight = Math.floor(h * -1);
    return [alignCenter, alignRight];
  };
  /**
   * 左右方向居中和居下对齐
   * 居中：top = -（popover自身高度 - 载体容器高度） / 2。
   * 居下：top = -（popover自身高度 - 载体容器高度）。
   * 返回值是一个数组，第一个为居中的数值，第二个为居下的数值。
   * @param {*} popover 气泡提示容器，它属于子容器
   * @param {*} parentContainer popover的父容器
   */
  const leftRightAlignBottomAndCenter = (popover, parentContainer) => {
    const h = $(popover).height() - $(parentContainer).height();
    const alignCenter = Math.floor((h / 2) * -1);
    const alignBottom = Math.floor(h * -1);
    return [alignCenter, alignBottom];
  };

  //alert警告提示
  const $Alert = () => {
    const prefixCls = "jv-alert";
    const tag = $(`j-alert`);

    tag.map((i, tags) => {
      const attributes = {
        msg: $(tags).attr("message") || "",
        icon: $(tags).data("icon") || "",
        desc: $(tags).attr("desc") || "",
        type: $(tags).attr("type") || "info",
        banner: $(tags).data("banner"),
        closable: $(tags).data("closable"),
        showIcon: $(tags).data("show-icon"),
        closeText: $(tags).data("close-text") || "",
      };

      isUndef(attributes.banner)
        ? (attributes.banner = false)
        : attributes.banner;
      isUndef(attributes.closable)
        ? (attributes.closable = false)
        : attributes.closable;
      isUndef(attributes.showIcon)
        ? (attributes.showIcon = false)
        : attributes.showIcon;

      const slot = {
        msg: $(tags).find(`slot[name="message"]`),
        desc: $(tags).find(`slot[name="desc"]`),
        icon: $(tags).find(`slot[name="icon"]`),
      };

      const template = `<div class="${prefixCls} ${prefixCls}-${attributes.type}" role="alert">
                                  <span class="${prefixCls}-icon"></span>
                                  <span class="${prefixCls}-message">${attributes.msg}</span>
                                  <span class="${prefixCls}-description">${attributes.desc}</span>
                                  <a class="${prefixCls}-close" role="button">
                                      <i class="jv-icon icon-cancel"></i>
                                  </a>
                              </div>`;

      addNode($(tag).eq(i), template);

      const wapper = $(`.${prefixCls}`).eq(i);
      const closeBtn = wapper.find(`.${prefixCls}-close`);
      const iconContainer = wapper.find(`.${prefixCls}-icon`);

      closeBtn.click(() => {
        destroy(wapper, wapper.slideUp("fast"));
      });

      if (slot.msg.html() !== undefined) {
        wapper.find(`.${prefixCls}-message`).empty().html(slot.msg.html());
      }

      if (attributes.desc !== "" || slot.desc.html() !== undefined) {
        wapper.addClass(`${prefixCls}-with-description`);
      }
      //slot的内容替换掉通过desc属性传入的内容
      if (slot.desc.html() !== undefined) {
        wapper.find(`.${prefixCls}-description`).empty().html(slot.desc.html());
      }

      if (attributes.showIcon) {
        wapper.addClass(`${prefixCls}-with-icon`);
        //根据不同的type类型添加对应的图标
        switch (attributes.type) {
          case "success":
            let successIcon = `<i class="jv-icon icon-ok-circled"></i>`;
            iconContainer.html(successIcon);
            break;

          case "warning":
            let warnIcon = `<i class="jv-icon icon-attention"></i>`;
            iconContainer.html(warnIcon);
            break;

          case "error":
            let Icon = `<i class="jv-icon icon-cancel-circled"></i>`;
            iconContainer.html(Icon);
            break;

          default:
            let infoIcon = `<i class="jv-icon icon-info-circled"></i>`;
            iconContainer.html(infoIcon);
            break;
        }

        if (attributes.icon !== "") {
          iconContainer.empty().html(attributes.icon);
        }
        //slot的图标替换掉通过原来的图标
        if (slot.icon.html() !== undefined) {
          iconContainer.empty().html(slot.icon.html());
        }
      } else {
        wapper.addClass(`${prefixCls}-no-icon`);
      }

      if (attributes.banner) {
        wapper.addClass(`${prefixCls}-with-banner`);
      }

      if (attributes.closable) {
        wapper.addClass(`${prefixCls}-closable`);

        if (attributes.closeText !== "") {
          wapper.find(`.${prefixCls}-close`).empty().html(attributes.closeText);
        }
      } else {
        wapper.find(`.${prefixCls}-close`).remove();
      }
    });
  };
  //avatar头像
  const $Avatar = () => {
    const prefixCls = "jv-avatar";
    const avatarStr = $(`.${prefixCls}-text`);
    avatarStr.map((i, str) => {
      str = $(str);
      const containerWidth = str.parent(`.${prefixCls}`).width();
      const strWidth = str.width();
      const strLen = str.text().length;
      let fontSize = 0;
      if (strWidth >= containerWidth) {
        fontSize = Math.floor(strWidth / strLen);
        fontSize < 8 ? (fontSize = 8) : fontSize;
        str.css("font-size", fontSize);
      }
    });
  };
  //backTop返回顶部
  const $BackTop = () => {
    const prefixCls = "jv-backtop";
    const tag = $(`j-backtop`);

    const template = (bottom, right, id) => {
      const html = $(`
                <div class="${prefixCls}" id="${id}" style="bottom: ${bottom}px; right: ${right}px; display:none;">
                    <i class="jv-icon icon-play ${prefixCls}-icon"></i>
                </div>
            `);

      $.fn.extend({
        backTop({
          bottom = 30,
          right = 30,
          duration = 800,
          height = 400,
          onClick = () => {},
        } = {}) {
          let backTop = html;

          backTop
            .css({
              bottom: `${bottom}px`,
              right: `${right}px`,
            })
            .click(() => {
              typeof onClick === "function" ? onClick() : onClick;
              $("html,body").animate({ scrollTop: 0 }, duration);
            });

          $(document).scroll(() => {
            let scroH = Math.floor($(document).scrollTop());
            if (scroH >= height) {
              backTop.fadeIn("fast");
            } else {
              backTop.fadeOut("fast");
            }
          });

          return this;
        },
      });

      return html;
    };

    tag.map((i, item) => {
      let tpl = template(30, 30, $(item).attr("id"));
      setTimeout(() => {
        addNode($(tag).eq(i), tpl);
      }, 0);
    });
  };
  //badge徽标
  const $Badge = () => {
    const prefixCls = "jv-badge";
    const badge = $(`.${prefixCls}`);
    badge.map((i, badges) => {
      badges = $(badges);
      const maxCount = badges.data("max") || 99;
      const sup = badges.children("sup");
      const count = sup.text();
      //预设
      count > maxCount ? sup.text(`${maxCount}+`) : sup;
      (count === "0") | (count === "") ? sup.hide() : sup.fadeIn("fast");
      sup.hasClass(`${prefixCls}-dot`) ? sup.html("") : sup;
      //实时监听sup标签的内容变化
      sup.bind("DOMNodeInserted", (e) => {
        const currentCount = $(e.target).text();
        currentCount > maxCount ? sup.text(`${maxCount}+`) : sup;
        (currentCount === "0") | (currentCount === "")
          ? sup.hide()
          : sup.fadeIn("fast");
      });
    });
  };
  //collapse折叠面板
  const $Collapse = () => {
    const prefixCls = "jv-collapse";
    //自定义标签，父标签
    const collapse = $(`j-collapse`);
    collapse.map((i, parentEle) => {
      const panel = $(parentEle).find($(`j-collapse-panel`)),
        //创建collapse的父元素
        container = $("<div></div>").attr("class", `${prefixCls}`),
        ul = $(`<ul></ul>`);
      container.append(ul);
      //接收data属性值的中间变量
      let defaultActiveKey = $(parentEle).data("set-active-key") || -1,
        isSimple = $(parentEle).data("simple") || false,
        isAccordion = $(parentEle).data("accordion") || false;
      //遍历子标签，面板标签
      panel.map((j, childrenEle) => {
        //接收data属性值的中间变量
        let title = $(childrenEle).attr("title") || "",
          key = $(childrenEle).data("key") || -2,
          collapseContent = $(childrenEle).html();
        //创建子元素
        let li = $(`<li></li>`).attr(`class`, `${prefixCls}-item`),
          header = $(`<div></div>`)
            .attr(`class`, `${prefixCls}-header`)
            .html(`<i class="jv-icon icon-right-open"></i> ${title}`),
          contentWrapper = $(`<div></div>`).attr(
            `class`,
            `${prefixCls}-content`
          ),
          contentBox = $(`<div></div>`)
            .attr(`class`, `${prefixCls}-content-box`)
            .html(`${collapseContent}`);
        //添加dom节点
        li.append(header, contentWrapper);
        contentWrapper.append(contentBox);
        ul.append(li);
        //api
        function propsAPI() {
          //设置默认展示某个面板
          if (key === defaultActiveKey) {
            contentWrapper.css("display", "block");
            header.addClass(`${prefixCls}-header-active`);
            li.addClass(`${prefixCls}-item-active`);
          }
          //简洁模式
          if (isSimple) {
            container.addClass(`${prefixCls}-simple`);
            li.addClass(`${prefixCls}-simple-header`);
          }
          //手风琴模式
          if (isAccordion) {
            header.click(() => {
              //移除除了当前点击的面板的其他同胞节点的该class
              li.siblings().removeClass(`${prefixCls}-item-active`);
              //若其他元素节点无该cls则将它们折叠起来
              if (!li.siblings().hasClass(`${prefixCls}-item-active`)) {
                li.removeClass(`${prefixCls}-simple-header`) //给当前点击的面板添加header下边框线
                  .siblings()
                  .addClass(`${prefixCls}-simple-header`) //其他面板折叠后取消其所有header的下边框线
                  .find(`.${prefixCls}-content`)
                  .slideUp();
              }
              clickChange(li, header, contentWrapper);
            });
          } else {
            header.click(() => {
              clickChange(li, header, contentWrapper);
            });
          }
        }
        propsAPI();
      });
      addNode(collapse.eq(i), container);
    });

    function clickChange(a, b, c) {
      a.hasClass(`${prefixCls}-item-active`)
        ? a.removeClass(`${prefixCls}-item-active`)
        : a.addClass(`${prefixCls}-item-active`);

      b.hasClass(`${prefixCls}-header-active`)
        ? b.removeClass(`${prefixCls}-header-active`)
        : b.addClass(`${prefixCls}-header-active`);

      c.slideToggle(250);
    }
  };
  //dropDown下拉菜单
  const $DropDown = () => {
    const prefixCls = "jv-dropdown";

    const dropDown = $(`.${prefixCls}`);

    let show = false,
      onTop = /^top/,
      onLeft = /left$/,
      onRight = /right$/,
      onBottom = /^bottom/;

    const slideDown = (dropDownRel, dropDownMenu, placement) => {
      dropDwonPlacement(placement, dropDownMenu, dropDownRel);
      dropDownMenu
        .addClass(`${prefixCls}-slide-enter`)
        .removeClass(`${prefixCls}-slide-out`);
    };

    const slideUp = (dropDownMenu) => {
      dropDownMenu
        .addClass(`${prefixCls}-slide-out`)
        .removeClass(`${prefixCls}-slide-enter`);
    };

    const setTransformOrigin = (dropDownMenu, p) => {
      //是否top方向
      if (onTop.test(p)) {
        dropDownMenu.css("transformOrigin", "center bottom");
      }
      //是否bottom方向
      if (onBottom.test(p)) {
        dropDownMenu.css("transformOrigin", "center top");
      }
    };

    const dropDwonPlacement = (p, dropDownMenu, dropDownRel) => {
      let top = getTopStart(dropDownRel, dropDownMenu),
        center = topBottomAlignRightAndCenter(dropDownMenu, dropDownRel);
      //是否top方向
      if (onTop.test(p)) {
        dropDownMenu.css({
          top: `${top - 5}px`,
          willChange: "top,left",
        });
      }
      //是否bottom方向
      if (onBottom.test(p)) {
        dropDownMenu.css("will-change", "top,left");
      }
      //是否以right结尾的方向
      if (onRight.test(p)) {
        dropDownMenu.css({
          right: 5,
          willChange: "top,right",
        });
      }
      //是否以left结尾的方向
      if (onLeft.test(p)) {
        dropDownMenu.css({
          left: 3,
          willChange: "top,left",
        });
      }
      //居中方向
      if (p === "top" || p === "bottom") {
        dropDownMenu.css({
          left: `${center[0]}px`,
          willChange: "top,left",
        });
      }
    };

    const toggel = (
      dropDown,
      dropDownRel,
      triggerWay,
      dropDownMenu,
      placement
    ) => {
      if (triggerWay === "hover") {
        dropDownMenu.css("margin", 0);
        dropDown
          .mouseenter(() => {
            slideDown(dropDownRel, dropDownMenu, placement);
          })
          .mouseleave(() => {
            slideUp(dropDownMenu);
          });
      }
      if (triggerWay === "click" || triggerWay === "contextMenu") {
        //点击空白处关闭菜单
        $(document).click((e) => {
          const con = dropDown;
          if (!con.is(e.target) && con.has(e.target).length === 0) {
            show = false;
            slideUp(dropDownMenu);
          }
        });
      }
      if (triggerWay === "click") {
        dropDownRel.click(() => {
          slideDown(dropDownRel, dropDownMenu, placement);
        });
      }
      if (triggerWay === "contextMenu") {
        dropDownRel.contextmenu(() => {
          if (!show) {
            show = true;
            slideDown(dropDownRel, dropDownMenu, placement);
          } else {
            show = false;
            slideUp(dropDownMenu);
          }
          return false;
        });
      }
    };

    const listItemClick = (item, dropDownMenu) => {
      item.click(() => {
        show = false;
        slideUp(dropDownMenu);
      });
    };

    dropDown.map((i, tags) => {
      let placement = $(tags).data("placement"),
        triggerWay = $(tags).data("trigger");

      const dropDownRel = $(tags).find(`.${prefixCls}-rel`);
      const dropDownMenu = $(tags).find(`.${prefixCls}-select-dropdown`);
      const dropDownItem = dropDownMenu.find(`.${prefixCls}-item`);

      isUndef(placement) ? (placement = "bottom") : placement;
      isUndef(triggerWay) ? (triggerWay = "hover") : triggerWay;

      toggel($(tags), dropDownRel, triggerWay, dropDownMenu, placement);
      setTransformOrigin(dropDownMenu, placement);
      listItemClick(dropDownItem, dropDownMenu);
    });
  };
  //input输入框
  const $Input = () => {
    const prefixCls = "jv-input";

    $(`.${prefixCls}-wrapper`).map((i, wrapper) => {
      let input = $(wrapper).find(`.${prefixCls}`),
        minLength = input.attr("minlength") || 0,
        maxLength = input.attr("maxlength") || 0,
        clearable = $(wrapper).data("clearable"),
        showWordLimit = $(wrapper).data("show-word-limit"),
        showHidePassword = $(wrapper).data("show-hide-password");

      isUndef(clearable) ? (clearable = false) : clearable;
      isUndef(showHidePassword) ? (showHidePassword = true) : showHidePassword;

      if ($(wrapper).hasClass(`${prefixCls}-disabled`)) {
        input.attr("readonly", true);
      }
      if (showWordLimit) {
        const wordContainer = $(
          `<span class="${prefixCls}-with-word-count">${minLength}/${maxLength}</span>`
        );

        input.bind("input propertychange", (e) => {
          wordContainer.html(`${input.val().length}/${maxLength}`);
        });

        $(wrapper).append(wordContainer);
      }
      if (clearable) {
        const isShowClearIcon = (flag) => {
          flag ? clearIcon.hide() : clearIcon.show();
        };
        const clearIcon = $(
          `<i class="jv-icon ${prefixCls}-icon icon-cancel-circled"></i>`
        );

        input.val() !== "" ? isShowClearIcon(false) : true;

        input
          .before(
            clearIcon.hide().click(() => {
              isShowClearIcon(true);
              input.val("");
            })
          )
          .bind("input propertychange", (e) => {
            input.val() !== "" ? isShowClearIcon(false) : isShowClearIcon(true);
          });
      }
      if (input.attr("type") === "password") {
        if (showHidePassword) {
          let eyeOn = $(
              `<i class="jv-icon ${prefixCls}-icon icon-eye-on"></i>`
            ),
            isOn = true;

          input.before(
            eyeOn.click(() => {
              if (isOn) {
                isOn = false;
                eyeOn.addClass("icon-eye-off").removeClass("icon-eye-on");
                input.attr("type", "text");
              } else {
                isOn = true;
                eyeOn.addClass("icon-eye-on").removeClass("icon-eye-off");
                input.attr("type", "password");
              }
            })
          );
        }
      }
    });
  };
  //input-number数字输入框
  const $InputNumber = {
    create() {
      const prefixCls = "jv-input-number";
      const tag = $(`j-input-number`);
      tag.map((i, tags) => {
        let id = $(tags).attr("id") || "",
          val = $(tags).data("val") || "",
          max = $(tags).data("max") || Infinity,
          min = $(tags).data("min") || -Infinity,
          top = 1,
          size = $(tags).attr("size") || "default",
          step = $(tags).data("step") || 1,
          bottom = 0,
          readonly = $(tags).data("readonly"),
          editable = $(tags).data("editable"),
          disabled = $(tags).data("disabled"),
          upIdSel = $(tags).attr("up-id") || "",
          downIdSel = $(tags).attr("down-id") || "",
          disabledCls = "",
          placeholder = $(tags).attr("placeholder") || "";
        //不同尺寸下，上下按钮的箭头需要调整一下
        if (size === "small") {
          top = 0;
          bottom = -4;
        } else if (size === "large") {
          top = 2;
          bottom = 1;
        }
        isUndef(readonly) ? (readonly = false) : readonly;
        isUndef(editable) ? (editable = false) : editable;
        isUndef(disabled) ? (disabled = false) : disabled;
        disabled ? (disabledCls = `${prefixCls}-disabled`) : disabledCls;

        const tpl = `<div class="${prefixCls} ${prefixCls}-${size} ${disabledCls}">
                                        <div class="${prefixCls}-handler-container">
                                            <a class="${prefixCls}-handler ${prefixCls}-handler-up" id="${upIdSel}">
                                                <span class="${prefixCls}-handler-up-inner jv-icon icon-up-open" style="top:${top}px"></span>
                                            </a>
                                            <a class="${prefixCls}-handler ${prefixCls}-handler-down" id="${downIdSel}">
                                                <span class="${prefixCls}-handler-down-inner jv-icon icon-down-open" style="bottom:${bottom}px"></span>
                                            </a>
                                        </div>
                                        <div class="${prefixCls}-input-container">
                                            <input type="number" placeholder="${placeholder}" value="${val}" max="${max}" min="${min}" step="${step}" class="${prefixCls}-input" id="${id}">
                                        </div>
                                    </div>`;

        addNode($(tag).eq(i), tpl);
        //是否设置输入框为不可编辑或只读
        if (readonly || editable)
          $(`.${prefixCls}-input`).eq(i).attr("readonly", true);
        //是否禁用输入框
        if (disabled) $(`.${prefixCls}-input`).eq(i).attr("disabled", true);
        //输入框聚焦样式
        $(`.${prefixCls}`).click(() => {
          if (!disabled)
            $(`.${prefixCls}`).eq(i).addClass(`${prefixCls}-focused`);
        });
        //点击空白处移除聚焦样式
        $(document).click((e) => {
          const con = $(`.${prefixCls}`).eq(i);
          if (!con.is(e.target) && con.has(e.target).length === 0)
            $(`.${prefixCls}`).eq(i).removeClass(`${prefixCls}-focused`);
        });
        //当前输入框的值
        let currentVal = "";
        //如果输入框有最小值限制则让当前输入框的初始值为最小值
        if (min !== -Infinity)
          currentVal = $(`.${prefixCls}-input`).eq(i).val();
        //输入框初始值为空则禁用递减按钮，只读和不可编辑状态除外
        if (currentVal === "" && !readonly && !editable)
          $(`.${prefixCls}-handler-down`)
            .eq(i)
            .addClass(`${prefixCls}-handler-down-disabled`);
        //通过键盘修改输入框的值需要同步currentVal的值
        $(`.${prefixCls}-input`)
          .eq(i)
          .bind("input propertychange", (e) => {
            //达到上限或下限值则禁用按钮
            if (
              $(`.${prefixCls}-input`).eq(i).val() >= max &&
              max !== Infinity
            ) {
              $(`.${prefixCls}-input`).eq(i).val(max);
              $(`.${prefixCls}-handler-up`)
                .eq(i)
                .addClass(`${prefixCls}-handler-up-disabled`);
            } else {
              $(`.${prefixCls}-handler-up`)
                .eq(i)
                .removeClass(`${prefixCls}-handler-up-disabled`);
            }
            if (
              $(`.${prefixCls}-input`).eq(i).val() <= min &&
              max !== -Infinity
            ) {
              $(`.${prefixCls}-input`).eq(i).val(min);
              $(`.${prefixCls}-handler-down`)
                .eq(i)
                .addClass(`${prefixCls}-handler-down-disabled`);
            } else {
              $(`.${prefixCls}-handler-down`)
                .eq(i)
                .removeClass(`${prefixCls}-handler-down-disabled`);
            }
            currentVal = $(`.${prefixCls}-input`).eq(i).val();
          });
        //在非只读状态下才能点击按钮
        if (!readonly) {
          //点击向上箭头叠加输入框的数值
          $(`.${prefixCls}-handler-up`)
            .eq(i)
            .click(() => {
              //获取当前输入框的值
              currentVal = $(`.${prefixCls}-input`).eq(i).val();
              currentVal++;
              $(`.${prefixCls}-handler-down`)
                .eq(i)
                .removeClass(`${prefixCls}-handler-down-disabled`);
              //达到上限值则禁用按钮
              if (currentVal > max) return false;
              else if (currentVal >= max)
                $(`.${prefixCls}-handler-up`)
                  .eq(i)
                  .addClass(`${prefixCls}-handler-up-disabled`);
              $(`.${prefixCls}-input`).eq(i).val(currentVal);
            });
          //点击向下箭头递减输入框的数值
          $(`.${prefixCls}-handler-down`)
            .eq(i)
            .click(() => {
              //获取当前输入框的值
              currentVal = $(`.${prefixCls}-input`).eq(i).val();
              $(`.${prefixCls}-handler-up`)
                .eq(i)
                .removeClass(`${prefixCls}-handler-up-disabled`);
              //达到下限值则禁用按钮
              if (currentVal <= min + 1)
                $(`.${prefixCls}-handler-down`)
                  .eq(i)
                  .addClass(`${prefixCls}-handler-down-disabled`);
              if (currentVal <= min) return false;
              $(`.${prefixCls}-input`)
                .eq(i)
                .val((currentVal -= 1));
            });
        }
      });
    },
  };
  //loadingBar加载中
  (() => {
    const prefixCls = "jv-loading-bar";

    const defaults = {
      height: 3,
      color: "#2196f3",
      failedColor: "#ff4d4f",
      duration: 250,
    };

    let timer = null,
      percent = 0,
      duration = 0,
      initPercent = 0;

    const template = $(`
                <div class="${prefixCls}" style="height:${defaults.height}px">
                    <div class="${prefixCls}-inner ${prefixCls}-inner-color-primary"></div>
                </div>
            `);
    $("body").append(template);

    const wrapper = $(`.${prefixCls}`);
    const barInner = $(`.${prefixCls}-inner`);

    setTimeout(() => {
      duration = defaults.duration;
      wrapper.height(`${defaults.height}px`);
      barInner
        .width(0)
        .height(`${defaults.height}px`)
        .css("background-color", `${defaults.color}`);
    }, 0);

    const clearTimer = () => {
      if (timer) {
        clearInterval(timer);
        timer = null;
      }
    };

    const finish = () => {
      setTimeout(() => {
        percent = 100;
        barInner.width(`${percent}%`);
      }, 0);
      setTimeout(() => {
        percent = 0;
        barInner.fadeOut();
        setTimeout(() => {
          //初始化进度和颜色
          barInner
            .width(`${percent}%`)
            .css("background-color", defaults.color)
            .fadeIn("fast");
        }, 200);
      }, 800);
    };

    const finishStatus = (status) => {
      if (status === "finish") {
        barInner.css("background-color", defaults.color);
      }
      if (status === "error") {
        setTimeout(() => {
          barInner.css("background-color", defaults.failedColor);
        }, 0);
      }
      if (status) {
        finish();
        clearTimer();
      }
    };

    const loading = () => {
      percent = initPercent;
      timer = setInterval(() => {
        percent += Math.floor(Math.random() * 3 + 1);
        if (percent > 95) {
          clearTimer();
        }
        if (percent > 100) {
          clearTimer();
          throw new Error(
            "Because your custom precision value is too large, the progress bar overflows the 100% range. Please reduce the precision value and debug again until no exception occurs"
          );
        }
        barInner.fadeIn("fast").width(`${percent}%`);
      }, duration);
    };

    $.extend({
      Loading: {
        start() {
          if (timer) return;
          setTimeout(() => {
            loading();
          }, 0);
          return this;
        },
        finish() {
          finishStatus("finish");
          return this;
        },
        error() {
          finishStatus("error");
          return this;
        },
        update(customPercent) {
          if (customPercent) {
            percent = customPercent;
            initPercent = customPercent;
          }
          return this;
        },
        config({ color, failedColor, height, duration } = {}) {
          color === undefined
            ? (color = defaults.color)
            : (defaults.color = color);
          failedColor === undefined
            ? (failedColor = defaults.failedColor)
            : (defaults.failedColor = failedColor);
          height === undefined
            ? (height = defaults.height)
            : (defaults.height = height);
          duration === undefined
            ? (duration = defaults.duration)
            : (defaults.duration = duration);
          return this;
        },
        destroy() {
          clearTimer();
          $(`.${prefixCls}`).remove();
          return this;
        },
      },
    });
    window.$.Loading = $.Loading;
  })();
  //message消息提示
  (() => {
    const prefixCls = "jv-message";
    const iconPrefixCls = "jv-icon";

    //事先添加message父容器到body底部
    const container = $(`<div class="${prefixCls}"></div>`);
    $("body").append(container);

    //默认message全局配置
    const defaults = {
      top: 12,
      duration: 3,
    };

    setTimeout(() => {
      container.css("top", defaults.top);
    }, 0);

    //获取容器的层级
    let zIndex = container.css("z-index");

    //每次调用message实例化让message父容器层级+1
    const level = () => {
      zIndex++;
      container.css("z-index", zIndex);
    };

    //message实例化
    //clickCb是点击关闭按钮后的回调事件
    //autoCloseCb是在message自动关闭后的回调事件
    const messageInstance = (
      key,
      content,
      closable,
      duration,
      type,
      background,
      clickCb,
      autoCloseCb
    ) => {
      let timer = null,
        iconType = "",
        closeElement = "",
        withBackGroundCls = "";

      if (key === undefined) {
        key = `${new Date().getTime()}`;
      }

      //如果调用全局配置方法 config 修改了默认duration的值，则所有实例化方法的duration变成修改的值
      if (defaults.duration !== 3) {
        duration = defaults.duration;
      }

      //是否显示关闭按钮
      if (closable) {
        closeElement = `
                          <a class="${prefixCls}-close" role="button">
                              <i class="${iconPrefixCls} icon-cancel"></i>
                          </a>
                      `;
      }

      //message是否显示背景色
      if (background) {
        withBackGroundCls = `${prefixCls}-notice-content-with-background`;
      }

      //根据不同类型添加不同图标
      if (type === "info") {
        iconType = "icon-info-circled";
      }
      if (type === "success") {
        iconType = "icon-ok-circled";
      }
      if (type === "warning") {
        iconType = "icon-attention";
      }
      if (type === "error") {
        iconType = "icon-cancel-circled";
      }
      if (type === "loading") {
        type = "info";
        iconType = "icon-spin5 animate-spin";
      }

      const messageNotice = $(
        `<div class="${prefixCls}-notice ${withBackGroundCls}" data-message-key="${key}" role="alert"></div>`
      );

      const contentTpl = $(`
                          <div class="${prefixCls}-notice-content">
                              <div class="${prefixCls}-custom-content ${prefixCls}-${type}">
                                  <i class="${iconPrefixCls} ${iconType}"></i>
                                  <span class="${prefixCls}-text">${content}</span>
                                  ${closeElement}
                              </div>
                          </div>
              `);

      level();
      container.append(
        messageNotice.addClass(`${prefixCls}-fade-in`).append(contentTpl)
      );

      //通过定时器移除message
      destroyElemByTimer(
        timer,
        duration,
        messageNotice,
        `${prefixCls}-fade-in`,
        `${prefixCls}-fade-out`,
        autoCloseCb
      );

      //通过关闭按钮点击将message移除
      //只在开启了显示关闭按钮的状态下进行遍历
      if (closable) {
        $(`.${prefixCls}-notice`).map((i, tags) => {
          //获取对应的message关闭按钮
          const close = $(tags).find(`.${prefixCls}-close`);

          close.click(() => {
            //回调事件
            typeof clickCb === "function" ? clickCb() : clickCb;
            //移除
            destroy(
              $(tags),
              $(tags)
                .removeClass(`${prefixCls}-fade-in`)
                .addClass(`${prefixCls}-fade-out`)
            );
          });
        });
      }
    };

    $.extend({
      Message: {
        info(
          c,
          {
            key,
            duration = 3,
            closable = false,
            background = false,
            onClose = () => {},
            afterClose = () => {},
          } = {}
        ) {
          messageInstance(
            key,
            c,
            closable,
            duration,
            "info",
            background,
            onClose,
            afterClose
          );
          return this;
        },

        success(
          c,
          {
            key,
            duration = 3,
            closable = false,
            background = false,
            onClose = () => {},
            afterClose = () => {},
          } = {}
        ) {
          messageInstance(
            key,
            c,
            closable,
            duration,
            "success",
            background,
            onClose,
            afterClose
          );
          return this;
        },

        warning(
          c,
          {
            key,
            duration = 3,
            closable = false,
            background = false,
            onClose = () => {},
            afterClose = () => {},
          } = {}
        ) {
          messageInstance(
            key,
            c,
            closable,
            duration,
            "warning",
            background,
            onClose,
            afterClose
          );
          return this;
        },
        error(
          c,
          {
            key,
            duration = 3,
            closable = false,
            background = false,
            onClose = () => {},
            afterClose = () => {},
          } = {}
        ) {
          messageInstance(
            key,
            c,
            closable,
            duration,
            "error",
            background,
            onClose,
            afterClose
          );
          return this;
        },

        loading(
          c,
          {
            key,
            duration = 3,
            closable = false,
            background = false,
            onClose = () => {},
            afterClose = () => {},
          } = {}
        ) {
          messageInstance(
            key,
            c,
            closable,
            duration,
            "loading",
            background,
            onClose,
            afterClose
          );
          return this;
        },

        config({ top, duration } = {}) {
          defaults.top = top;
          defaults.duration = duration;
          return this;
        },
        destroy(key, callback) {
          const message = $(`.${prefixCls}-notice`);

          if (key === undefined) {
            globalDestroy(
              message,
              `${prefixCls}-fade-in`,
              `${prefixCls}-fade-out`
            );
          } else {
            destroyInstanceByKey(
              message,
              key,
              "message-key",
              `${prefixCls}-fade-in`,
              `${prefixCls}-fade-out`,
              callback
            );
          }
          return this;
        },
      },
    });

    window.$.Message = $.Message;
  })();
  //messageBox消息盒子提示框
  (() => {
    const prefixCls = "jv-msgbox";

    let zIndex = 1010;

    const iconTypes = {
      info: "icon-info-circled",
      success: "icon-ok-circled",
      warning: "icon-attention",
      error: "icon-cancel-circled",
      confirm: "icon-help-circled",
      loading: "icon-spin6 animate-spin",
    };

    const _fadeIn = (mask, wrap, msgBox) => {
      mask.fadeIn("fast");
      wrap.fadeIn("fast");
      msgBox.addClass(`${prefixCls}-fade-enter`);
    };

    const _fadeOut = (mask, wrap, msgBox) => {
      mask.fadeOut("fast");
      wrap.fadeOut("fast");
      msgBox
        .addClass(`${prefixCls}-fade-leave`)
        .removeClass(`${prefixCls}-fade-enter`);
      setTimeout(() => {
        mask.remove();
        wrap.remove();
      }, 300);
      $("body").removeAttr("style");
    };

    const instance = (
      type,
      title,
      content,
      width,
      okText,
      cancelText,
      ok,
      cancel,
      closable,
      loading,
      scrollable
    ) => {
      let iconCls = "",
        cancelButton = "";

      type === "info" ? (iconCls = iconTypes.info) : type;
      type === "success" ? (iconCls = iconTypes.success) : type;
      type === "warning" ? (iconCls = iconTypes.warning) : type;
      type === "error" ? (iconCls = iconTypes.error) : type;

      if (type === "confirm") {
        type = "warning";
        iconCls = iconTypes.confirm;
        cancelButton = `<button class="jv-btn jv-btn-text" id="msgBoxCancelBtn">${cancelText}</button>`;
      }
      if (!scrollable) {
        $("body").css({
          paddingRight: 17,
          overflow: "hidden",
        });
      }

      const template = $(`
                    <div class="${prefixCls}-mask"></div>
                    <div class="${prefixCls}-wrap" tabindex="-1" role="document">
                        <div class="${prefixCls}" style="width: ${width}px;" role="dialog">
                            <div class="${prefixCls}-content">
                                <div class="${prefixCls}-body">
                                    <div class="${prefixCls}-confirm">
                                        <header class="${prefixCls}-confirm-head">
                                            <div class="${prefixCls}-confirm-head-icon ${prefixCls}-confirm-head-icon-${type}">
                                                <i class="jv-icon ${iconCls}"></i>
                                            </div>
                                            <div class="${prefixCls}-confirm-head-title">${title}</div>
                                        </header>
                                        <section class="${prefixCls}-confirm-body">
                                            <div>${content}</div>
                                        </section>
                                        <footer class="${prefixCls}-confirm-footer">
                                          ${cancelButton}
                                            <button class="jv-btn jv-btn-primary" id="msgBoxOkBtn">${okText}</button>
                                        </footer>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            `);
      zIndex++;
      $("body").append(template.css("z-index", zIndex));
      const wrap = $(`.${prefixCls}-wrap`);

      wrap.map((i, tags) => {
        const mask = $(tags).prev(`.${prefixCls}-mask`);
        const msgBox = $(tags).find(`.${prefixCls}`);
        const okBtn = $(tags).find(`#msgBoxOkBtn`);
        const cancelBtn = $(tags).find(`#msgBoxCancelBtn`);
        _fadeIn(mask, $(tags), msgBox);
        //确认按钮点击事件
        okBtn.click(() => {
          typeof ok === "function" ? ok() : ok;
          //确定按钮是否显示 loading 状态，开启则需手动调用 $Message.remove()来关闭对话框
          if (!loading) {
            _fadeOut(mask, wrap, msgBox);
          } else {
            let loadingStatus = `<i class="jv-icon ${iconTypes.loading}"></i>`;
            okBtn
              .addClass("loading")
              .attr("disabled", true)
              .prepend(loadingStatus);
          }
        });
        //取消按钮点击事件
        cancelBtn.click(() => {
          typeof cancel === "function" ? cancel() : cancel;
          _fadeOut(mask, wrap, msgBox);
        });
      });
      //是否能够esc键关闭对话框
      if (closable) {
        $(document).keydown((e) => {
          if (e.keyCode === 27) {
            _fadeOut(mask, wrap, msgBox);
          }
        });
      }
    };
    $.extend({
      MessageBox: {
        info({
          title = "",
          content = "",
          width = 416,
          okText = "确定",
          cancelText = "取消",
          ok = () => {},
          closable = false,
          loading = false,
          scrollable = false,
        } = {}) {
          instance(
            "info",
            title,
            content,
            width,
            okText,
            cancelText,
            ok,
            undefined,
            closable,
            loading,
            scrollable
          );
          return this;
        },
        success({
          title = "",
          content = "",
          width = 416,
          okText = "确定",
          cancelText = "取消",
          ok = () => {},
          closable = false,
          loading = false,
          scrollable = false,
        } = {}) {
          instance(
            "success",
            title,
            content,
            width,
            okText,
            cancelText,
            ok,
            undefined,
            closable,
            loading,
            scrollable
          );
          return this;
        },
        warning({
          title = "",
          content = "",
          width = 416,
          okText = "确定",
          cancelText = "取消",
          ok = () => {},
          closable = false,
          loading = false,
          scrollable = false,
        } = {}) {
          instance(
            "warning",
            title,
            content,
            width,
            okText,
            cancelText,
            ok,
            undefined,
            closable,
            loading,
            scrollable
          );
          return this;
        },
        error({
          title = "",
          content = "",
          width = 416,
          okText = "确定",
          cancelText = "取消",
          ok = () => {},
          closable = false,
          loading = false,
          scrollable = false,
        } = {}) {
          instance(
            "error",
            title,
            content,
            width,
            okText,
            cancelText,
            ok,
            undefined,
            closable,
            loading,
            scrollable
          );
          return this;
        },
        confirm({
          title = "",
          content = "",
          width = 416,
          okText = "确定",
          cancelText = "取消",
          ok = () => {},
          cancel = () => {},
          closable = false,
          loading = false,
          scrollable = false,
        } = {}) {
          instance(
            "confirm",
            title,
            content,
            width,
            okText,
            cancelText,
            ok,
            cancel,
            closable,
            loading,
            scrollable
          );
          return this;
        },
        remove() {
          const mask = $(`.${prefixCls}-mask`);
          const wrap = $(`.${prefixCls}-wrap`);
          const msgBox = $(`.${prefixCls}`);
          _fadeOut(mask, wrap, msgBox);
          return this;
        },
      },
    });
    window.$.MessageBox = $.MessageBox;
  })();
  //notice通知提醒
  (() => {
    const prefixCls = "jv-notification";

    const defaults = {
      top: 24,
      bottom: 24,
      duration: 4,
      placement: "top-right",
    };

    //svg -> path图标参数
    const commonPath =
      "M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372 372 166.6 372 372-166.6 372-372 372z";
    const iconPath = {
      info: {
        p1: commonPath,
        p2:
          "M464 336a48 48 0 1096 0 48 48 0 10-96 0zm72 112h-48c-4.4 0-8 3.6-8 8v272c0 4.4 3.6 8 8 8h48c4.4 0 8-3.6 8-8V456c0-4.4-3.6-8-8-8z",
      },
      success: {
        p1:
          "M699 353h-46.9c-10.2 0-19.9 4.9-25.9 13.3L469 584.3l-71.2-98.8c-6-8.3-15.6-13.3-25.9-13.3H325c-6.5 0-10.3 7.4-6.5 12.7l124.6 172.8a31.8 31.8 0 0051.7 0l210.6-292c3.9-5.3.1-12.7-6.4-12.7z",
        p2: commonPath,
      },
      warning: {
        p1: commonPath,
        p2:
          "M464 688a48 48 0 1096 0 48 48 0 10-96 0zm24-112h48c4.4 0 8-3.6 8-8V296c0-4.4-3.6-8-8-8h-48c-4.4 0-8 3.6-8 8v272c0 4.4 3.6 8 8 8z",
      },
      error: {
        p1:
          "M685.4 354.8c0-4.4-3.6-8-8-8l-66 .3L512 465.6l-99.3-118.4-66.1-.3c-4.4 0-8 4-8 8 0 1.9.7 3.7 1.9 5.2l130.1 155L340.5 670a8.32 8.32 0 00-1.9 5.2c0 4.4 3.6 8 8 8l66.1-.3L512 564.4l99.3 118.4 66 .3c4.4 0 8-4 8-8 0-1.9-.7-3.7-1.9-5.2L554 515l130.1-155c1.2-1.4 1.8-3.3 1.8-5.2z",
        p2:
          "M512 65C264.6 65 64 265.6 64 513s200.6 448 448 448 448-200.6 448-448S759.4 65 512 65zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372 372 166.6 372 372-166.6 372-372 372z",
      },
      close:
        "M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z",
    };

    //不同方向的notice容器
    const topLeftContainer = $(
      `<div class="${prefixCls} ${prefixCls}-top-left"></div>`
    );
    const topRightContainer = $(
      `<div class="${prefixCls} ${prefixCls}-top-right"></div>`
    );
    const bottomLeftContainer = $(
      `<div class="${prefixCls} ${prefixCls}-bottom-left"></div>`
    );
    const bottomRightContainer = $(
      `<div class="${prefixCls} ${prefixCls}-bottom-right"></div>`
    );

    $("body").append(
      topLeftContainer,
      topRightContainer,
      bottomLeftContainer,
      bottomRightContainer
    );

    //全局配置的参数发生改变要同步到dom，这里需要异步执行
    setTimeout(() => {
      topLeftContainer.css({
        top: `${defaults.top}px`,
        left: 0,
        bottom: "auto",
      });
      topRightContainer.css({
        top: `${defaults.top}px`,
        right: 0,
        bottom: "auto",
      });
      bottomLeftContainer.css({
        top: "auto",
        left: 0,
        bottom: `${defaults.bottom}px`,
      });
      bottomRightContainer.css({
        top: "auto",
        right: 0,
        bottom: `${defaults.bottom}px`,
      });
    }, 0);

    let zIndex = 1005,
      fadeEnterCls = "";

    const levelsIncrease = () => {
      zIndex++;
      topLeftContainer.css("z-index", zIndex);
      topRightContainer.css("z-index", zIndex);
      bottomLeftContainer.css("z-index", zIndex);
      bottomRightContainer.css("z-index", zIndex);
    };

    //创建notice实例化元素
    //closeCb 点击关闭按钮的回调
    //onClickCb 点击notice的回调
    const noticeInstance = (
      type,
      title,
      desc,
      duration,
      placement,
      key,
      closeCb,
      onClickCb
    ) => {
      let path1 = "",
        path2 = "",
        timer = null,
        withIconCls = "";

      type !== "open" ? (withIconCls = `${prefixCls}-notice-with-icon`) : type;

      if (type === "info") {
        path1 = iconPath.info.p1;
        path2 = iconPath.info.p2;
      }
      if (type === "success") {
        path1 = iconPath.success.p1;
        path2 = iconPath.success.p2;
      }
      if (type === "warning") {
        path1 = iconPath.warning.p1;
        path2 = iconPath.warning.p2;
      }
      if (type === "error") {
        path1 = iconPath.error.p1;
        path2 = iconPath.error.p2;
      }

      duration === undefined ? (duration = defaults.duration) : duration;

      //如果默认的全局duration值发生改变则让所有私有duration变成全局duration
      defaults.duration !== 4 ? (duration = defaults.duration) : duration;

      placement === "" ? (placement = defaults.placement) : placement;

      key === undefined ? (key = `auto_${Date.now()}`) : key;

      const reg = {
        isLeft: /left$/,
        isRight: /right$/,
      };

      reg.isRight.test(placement)
        ? (fadeEnterCls = `${prefixCls}-fade-enter`)
        : placement;

      reg.isLeft.test(placement)
        ? (fadeEnterCls = `${prefixCls}-left-fade-enter`)
        : placement;

      const childWrapper = $(
        `<div class="${prefixCls}-notice ${prefixCls}-notice-closable ${fadeEnterCls}" data-notice-key="${key}"></div>`
      );

      const noticeContent = $(`
                    <div class="${prefixCls}-notice-content">
                        <div class="${withIconCls}" role="alert">
                            <span role="img" class="${prefixCls}-notice-icon ${prefixCls}-notice-icon-${type}">
                                <svg viewBox="64 64 896 896"  width="1em" height="1em" fill="currentColor">
                                  <path d="${path1}"></path>
                                  <path d="${path2}"></path>
                                </svg>
                            </span>
                            <div class="${prefixCls}-notice-title">${title}</div>
                            <div class="${prefixCls}-notice-description">${desc}</div>
                        </div>
                    </div>
                    <a tabindex="0" class="${prefixCls}-notice-close" role="button">
                        <span class="${prefixCls}-close-x">
                            <span role="img" class="${prefixCls}-close ${prefixCls}-close-icon">
                                <svg viewBox="64 64 896 896" width="1em" height="1em" fill="currentColor">
                                    <path d="${iconPath.close}"></path>
                                </svg>
                            </span>
                        </span>
                    </a>
            `);

      childWrapper.append(noticeContent);

      const noticeTitle = childWrapper.find(`.${prefixCls}-notice-title`);

      desc == "" ? noticeTitle.css("margin-bottom", 0) : desc;

      //根据弹出位置放入已经设定好的不同方向的容器中
      switch (placement) {
        case "top-right":
          topRightContainer.append(childWrapper);
          break;
        case "top-left":
          topLeftContainer.append(childWrapper);
          break;
        case "bottom-right":
          bottomRightContainer.prepend(childWrapper);
          break;
        case "bottom-left":
          bottomLeftContainer.prepend(childWrapper);
          break;
        default:
          topRightContainer.append(childWrapper);
          break;
      }

      levelsIncrease();

      //通过定时器关闭notice
      destroyElemByTimer(
        timer,
        duration,
        childWrapper,
        fadeEnterCls,
        `${prefixCls}-fade-leave`
      );

      //通过点击关闭按钮关闭notice
      childWrapper.map((i, tags) => {
        const close = $(tags).find(`.${prefixCls}-notice-close`);

        close.click(() => {
          //点击关闭按钮的回调
          typeof closeCb === "function" ? closeCb() : closeCb;

          destroy(
            $(tags),
            $(tags)
              .removeClass(fadeEnterCls)
              .addClass(`${prefixCls}-fade-leave`)
          );
        });

        //点击notice的回调
        $(tags).click(() => {
          typeof onClickCb === "function" ? onClickCb() : onClickCb;
        });
      });
    };

    $.extend({
      Notice: {
        open({
          title = "",
          desc = "",
          duration,
          placement = "",
          key,
          onClose = () => {},
          onClick = () => {},
        } = {}) {
          noticeInstance(
            "open",
            title,
            desc,
            duration,
            placement,
            key,
            onClose,
            onClick
          );
          return this;
        },
        info({
          title = "",
          desc = "",
          duration,
          placement = "",
          key,
          onClose = () => {},
          onClick = () => {},
        } = {}) {
          noticeInstance(
            "info",
            title,
            desc,
            duration,
            placement,
            key,
            onClose,
            onClick
          );
          return this;
        },
        success({
          title = "",
          desc = "",
          duration,
          placement = "",
          key,
          onClose = () => {},
          onClick = () => {},
        } = {}) {
          noticeInstance(
            "success",
            title,
            desc,
            duration,
            placement,
            key,
            onClose,
            onClick
          );
          return this;
        },
        warning({
          title = "",
          desc = "",
          duration,
          placement = "",
          key,
          onClose = () => {},
          onClick = () => {},
        } = {}) {
          noticeInstance(
            "warning",
            title,
            desc,
            duration,
            placement,
            key,
            onClose,
            onClick
          );
          return this;
        },
        error({
          title = "",
          desc = "",
          duration,
          placement = "",
          key,
          onClose = () => {},
          onClick = () => {},
        } = {}) {
          noticeInstance(
            "error",
            title,
            desc,
            duration,
            placement,
            key,
            onClose,
            onClick
          );
          return this;
        },
        config({ top, bottom, duration, placement } = {}) {
          top === undefined ? (top = defaults.top) : (defaults.top = top);
          bottom === undefined
            ? (bottom = defaults.bottom)
            : (defaults.bottom = bottom);
          duration === undefined
            ? (duration = defaults.duration)
            : (defaults.duration = duration);
          placement === undefined
            ? (placement = defaults.placement)
            : (defaults.placement = placement);
          return this;
        },
        close(key, callback) {
          destroyInstanceByKey(
            $(`.${prefixCls}-notice`),
            key,
            "notice-key",
            fadeEnterCls,
            `${prefixCls}-fade-leave`,
            callback
          );
          return this;
        },
        destroy() {
          globalDestroy(
            $(`.${prefixCls}-notice`),
            fadeEnterCls,
            `${prefixCls}-fade-leave`
          );
          return this;
        },
      },
    });
    window.$.Notice = $.Notice;
  })();
  //pageHeader页头
  const $PageHeader = () => {
    const prefixCls = "jv-page-header";
    const tag = $(`j-page-header`);

    const template = (id, cls, title, content, onBack) => {
      const html = $(`
                <header class="${prefixCls}">
                    <div class="${prefixCls}-left ${cls}" id="${id}" onclick="${onBack}">
                        <i class="jv-icon icon-left ${prefixCls}-back"></i>
                        <span class="${prefixCls}-title">${title}</span>
                    </div>
                    <div class="${prefixCls}-content">${content}</div>
                </header>
          `);
      return html;
    };

    tag.map((i, tags) => {
      let id = $(tags).attr("id") || "",
        cls = $(tags).attr("class") || "",
        title = $(tags).attr("title") || "",
        onBack = $(tags).attr("on-back") || "",
        content = $(tags).attr("content") || "";

      const slot = {
        title: $(tags).find(`slot[name="title"]`).html(),
        content: $(tags).find(`slot[name="content"]`).html(),
      };

      if (slot.title) {
        title = slot.title;
      }
      if (slot.content) {
        content = slot.content;
      }

      const tpl = template(id, cls, title, content, onBack);
      addNode($(tags).eq(i), tpl);
    });
  };

  //popover气泡卡片提示框
  const $Popover = () => {
    const prefixCls = "jv-popover";
    const tag = $(`j-popover`);

    $.fn.extend({
      showPopover() {
        let popover = $(this);
        popover
          .addClass(`${prefixCls}-fade-enter`)
          .removeClass(`${prefixCls}-fade-out`);
      },
      hidePopover() {
        let popover = $(this);
        popover
          .addClass(`${prefixCls}-fade-out`)
          .removeClass(`${prefixCls}-fade-enter`);
      },
    });

    const template = (triggerEl, title, content, placement, id) => {
      let titleElement = "",
        contentElement = "";
      if (title) {
        titleElement = `<div class="${prefixCls}-title"><span>${title}</span></div>`;
      }
      if (content) {
        contentElement = `
                      <div class="${prefixCls}-inner-content">
                          <div style="overflow: auto;">${content}</div>
                      </div>
                  `;
      }
      const html = $(`
                      <div class="${prefixCls}-wrap">
                          <div class="${prefixCls}-rel">${triggerEl}</div>
                          <div class="${prefixCls} ${prefixCls}-fade-out" id="${id}" role="tooltip" x-placement="${placement}">
                              <div class="${prefixCls}-content">
                                  <div class="${prefixCls}-arrow"></div>
                                  <div class="${prefixCls}-inner">
                                  ${titleElement}
                                  ${contentElement}
                                  </div>
                              </div>
                          </div>
                      </div>
              `);
      return html;
    };

    const getAttr = (el) => {
      return {
        id: el.attr("id") || "",
        title: el.attr("title") || "",
        content: el.attr("content") || "",
        triggerEl: el.html() || "",
        placement: el.attr("placement") || "top",
        triggerMode: el.attr("trigger") || "click",
      };
    };

    const slotContent = (el) => {
      return {
        title: el.find(`slot[name="title"]`).html(),
        content: el.find(`slot[name="content"]`).html(),
      };
    };

    const setPlacement = (parentContainer, popover, p) => {
      const reg = {
        top: /^top/,
        left: /^left/,
        right: /^right/,
        bottom: /^bottom/,
      };

      let topStart = getTopStart(parentContainer, popover),
        leftStart = getLeftStart(popover),
        rightStart = getRightStart(parentContainer),
        bottomStart = getBottomStart(parentContainer),
        top_bottom_align_center_right = topBottomAlignRightAndCenter(
          popover,
          parentContainer
        ),
        left_right_alignCenter = leftRightAlignBottomAndCenter(
          popover,
          parentContainer
        );

      if (reg.top.test(p)) {
        popover.css("top", `${topStart}px`);
      }
      if (reg.left.test(p)) {
        popover.css("left", `${leftStart}px`);
      }
      if (reg.right.test(p)) {
        popover.css("left", `${rightStart}px`);
      }
      if (reg.bottom.test(p)) {
        popover.css("top", `${bottomStart}px`);
      }
      if (p === "top" || p === "bottom") {
        popover.css("left", `${top_bottom_align_center_right[0]}px`);
      }
      if (p === "top-right" || p === "bottom-right") {
        popover.css("left", `${top_bottom_align_center_right[1]}px`);
      }
      if (p === "left" || p === "right") {
        popover.css("top", `${left_right_alignCenter[0]}px`);
      }
      if (p === "left-bottom" || p === "right-bottom") {
        popover.css("top", `${left_right_alignCenter[1]}px`);
      }
    };

    const triggerMode = (mode, triggerEl, popover, placement) => {
      if (mode === "focus") {
        $(triggerEl)
          .mousedown(() => {
            $(popover)
              .addClass(`${prefixCls}-fade-enter`)
              .removeClass(`${prefixCls}-fade-out`);
            setPlacement(triggerEl, popover, placement);
          })
          .mouseup(() => {
            $(popover)
              .addClass(`${prefixCls}-fade-out`)
              .removeClass(`${prefixCls}-fade-enter`);
          });
      }
      if (mode === "click") {
        $(triggerEl)
          .find(`.${prefixCls}-rel`)
          .click(() => {
            $(popover)
              .addClass(`${prefixCls}-fade-enter`)
              .removeClass(`${prefixCls}-fade-out`);
            setPlacement(triggerEl, popover, placement);
          });
        //点击空白处关闭菜单
        $(document).click((e) => {
          const con = $(triggerEl); // 设置目标区域
          if (!con.is(e.target) && con.has(e.target).length === 0) {
            $(popover)
              .addClass(`${prefixCls}-fade-out`)
              .removeClass(`${prefixCls}-fade-enter`);
          }
        });
      }
      if (mode === "hover") {
        $(triggerEl)
          .mouseenter(() => {
            $(popover)
              .addClass(`${prefixCls}-fade-enter`)
              .removeClass(`${prefixCls}-fade-out`);
            setPlacement(triggerEl, popover, placement);
          })
          .mouseleave(() => {
            $(popover)
              .addClass(`${prefixCls}-fade-out`)
              .removeClass(`${prefixCls}-fade-enter`);
          });
      }
    };

    const popoverConfig = (mode, triggerEl, popover, placement) => {
      setPlacement(triggerEl, popover, placement);
      triggerMode(mode, triggerEl, popover, placement);
      triggerEl.find('slot[name="title"').remove();
      triggerEl.find('slot[name="content"').remove();
    };

    const addElNode = () => {
      tag.map((i, tags) => {
        const attr = getAttr($(tags));
        const slot = slotContent($(tags));
        //如果指定的slot标签存在并且有内容则替换掉通过attr属性添加的内容
        if (slot.title) {
          attr.title = slot.title;
        }
        if (slot.content) {
          attr.content = slot.content;
        }

        const tpl = template(
          attr.triggerEl,
          attr.title,
          attr.content,
          attr.placement,
          attr.id
        );

        addNode($(tag).eq(i), tpl);

        const triggerEl = $(`.${prefixCls}-wrap`).eq(i);
        const popover = triggerEl.find(`.${prefixCls}`);
        popoverConfig(attr.triggerMode, triggerEl, popover, attr.placement);
      });
    };
    addElNode();
  };
  //popover-confirm气泡卡片确认框
  const $PopoverConfirm = () => {
    const prefixCls = "jv-popover";
    const tag = $(`j-popconfirm`);

    const template = (triggerEl, title, placement, okText, cancelText) => {
      let titleElement = "";
      if (title) {
        titleElement = `<div class="${prefixCls}-message-title">${title}</div>`;
      }
      const html = $(`
                      <div class="${prefixCls}-wrap">
                          <div class="${prefixCls}-rel">${triggerEl}</div>
                          <div class="${prefixCls} ${prefixCls}-fade-out" role="tooltip" x-placement="${placement}">
                              <div class="${prefixCls}-content">
                                  <div class="${prefixCls}-arrow"></div>
                                  <div class="${prefixCls}-inner">
                                      <div class="${prefixCls}-inner-content">
                                          <div class="${prefixCls}-message">
                                            <i class="jv-icon icon-attention"></i>
                                            ${titleElement}
                                          </div>
                                          <div class="${prefixCls}-buttons">
                                              <button class="jv-btn jv-btn-text jv-btn-small" id="pop-cancelBtn">${cancelText}</button>
                                              <button class="jv-btn jv-btn-primary jv-btn-small" id="pop-okBtn">${okText}</button>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
              `);
      return html;
    };

    const getAttr = (popover) => {
      return {
        title: popover.attr("title") || "",
        okText: popover.attr("ok-text") || "确定",
        cancelText: popover.attr("cancel-text") || "取消",
        triggerEl: popover.html() || "",
        placement: popover.attr("placement") || "top",
        okClickFuncName: popover.attr("onConfirm"),
        cancelClickFuncName: popover.attr("onCancel"),
      };
    };

    const slotContent = (popover) => {
      return {
        title: popover.find(`slot[name="title"]`).html(),
      };
    };

    const setPlacement = (parentContainer, popover, p) => {
      const reg = {
        top: /^top/,
        left: /^left/,
        right: /^right/,
        bottom: /^bottom/,
      };

      let topStart = getTopStart(parentContainer, popover),
        leftStart = getLeftStart(popover),
        rightStart = getRightStart(parentContainer),
        bottomStart = getBottomStart(parentContainer),
        top_bottom_align_center_right = topBottomAlignRightAndCenter(
          popover,
          parentContainer
        ),
        left_right_alignCenter = leftRightAlignBottomAndCenter(
          popover,
          parentContainer
        );

      if (reg.top.test(p)) {
        popover.css("top", `${topStart}px`);
      }
      if (reg.left.test(p)) {
        popover.css("left", `${leftStart}px`);
      }
      if (reg.right.test(p)) {
        popover.css("left", `${rightStart}px`);
      }
      if (reg.bottom.test(p)) {
        popover.css("top", `${bottomStart}px`);
      }
      if (p === "top" || p === "bottom") {
        popover.css("left", `${top_bottom_align_center_right[0]}px`);
      }
      if (p === "top-right" || p === "bottom-right") {
        popover.css("left", `${top_bottom_align_center_right[1]}px`);
      }
      if (p === "left" || p === "right") {
        popover.css("top", `${left_right_alignCenter[0]}px`);
      }
      if (p === "left-bottom" || p === "right-bottom") {
        popover.css("top", `${left_right_alignCenter[1]}px`);
      }
    };

    const triggerMode = (triggerEl, popover, placement) => {
      $(triggerEl)
        .find(`.${prefixCls}-rel`)
        .click(() => {
          $(popover)
            .addClass(`${prefixCls}-fade-enter`)
            .removeClass(`${prefixCls}-fade-out`);
          setPlacement(triggerEl, popover, placement);
        });
      //点击空白处关闭菜单
      $(document).click((e) => {
        const con = $(triggerEl); // 设置目标区域
        if (!con.is(e.target) && con.has(e.target).length === 0) {
          $(popover)
            .addClass(`${prefixCls}-fade-out`)
            .removeClass(`${prefixCls}-fade-enter`);
        }
      });
    };

    const addClickEvents = (popover, okClickFuncName, cancelClickFuncName) => {
      let okBtn = $(popover).find("#pop-okBtn"),
        cancelBtn = $(popover).find("#pop-cancelBtn");
      addClick(okBtn, okClickFuncName);
      addClick(cancelBtn, cancelClickFuncName);
      okBtn.click(() => {
        $(popover)
          .addClass(`${prefixCls}-fade-out`)
          .removeClass(`${prefixCls}-fade-enter`);
      });
      cancelBtn.click(() => {
        $(popover)
          .addClass(`${prefixCls}-fade-out`)
          .removeClass(`${prefixCls}-fade-enter`);
      });
    };

    const popoverConfig = (
      triggerEl,
      popover,
      placement,
      okClickFuncName,
      cancelClickFuncName
    ) => {
      setPlacement(triggerEl, popover, placement);
      triggerMode(triggerEl, popover, placement);
      addClickEvents(popover, okClickFuncName, cancelClickFuncName);
      $(triggerEl).find('slot[name="title"]').remove();
    };

    const addElNode = () => {
      tag.map((i, tags) => {
        const attr = getAttr($(tags));
        const slot = slotContent($(tags));
        //如果指定的slot标签存在并且有内容则替换掉通过attr属性添加的内容
        if (slot.title) {
          attr.title = slot.title;
        }
        const tpl = template(
          attr.triggerEl,
          attr.title,
          attr.placement,
          attr.okText,
          attr.cancelText
        );

        addNode($(tag).eq(i), tpl);

        const triggerEl = $(`.${prefixCls}-wrap`).eq(i);
        const popover = triggerEl.find(`.${prefixCls}`);
        popoverConfig(
          triggerEl,
          popover,
          attr.placement,
          attr.okClickFuncName,
          attr.cancelClickFuncName
        );
      });
    };
    addElNode();
  };
  //result结果
  const $Result = () => {
    const prefixCls = "jv-result";
    const tag = $(`j-result`);

    const iconType = {
      info: "icon-info-circled",
      success: "icon-ok-circled",
      warning: "icon-attention",
      error: "icon-cancel-circled",
      403: "icon-lock-alt",
      404: "icon-paper-plane-alt",
      500: " icon-frown",
    };

    const template = (status, title, subTitle, footerContent, icon) => {
      let icons = "",
        customIcon = "",
        builtInIcons = "",
        subTitleElement = "";

      if (status === "info") {
        icons = iconType.info;
      }
      if (status === "success") {
        icons = iconType.success;
      }
      if (status === "warning") {
        icons = iconType.warning;
      }
      if (status === "error") {
        icons = iconType.error;
      }
      if (status === "403") {
        icons = iconType[403];
      }
      if (status === "404") {
        icons = iconType[404];
        status = "info";
      }
      if (status === "500") {
        icons = iconType[500];
        status = "error";
      }
      if (subTitle) {
        subTitleElement = `<div class="${prefixCls}-subtitle">${subTitle}</div>`;
      }
      if (!icon) {
        builtInIcons = `<i class="jv-icon ${icons}"></i>`;
      } else {
        customIcon = icon;
      }

      const html = $(`
                <section class="${prefixCls}">
                    <div class="${prefixCls}-icon ${prefixCls}-${status}">
                      ${builtInIcons}
                      ${customIcon}
                    </div>
                    <header class="${prefixCls}-title">${title}</header>
                    ${subTitleElement}
                    <div class="${prefixCls}-footer">${footerContent}</div>
                </section>
            `);

      return html;
    };

    tag.map((i, tags) => {
      let icon = $(tags).attr("icon") || "",
        title = $(tags).attr("title") || "",
        status = $(tags).attr("status") || "info",
        footer = $(tags).attr("footer") || "",
        subTitle = $(tags).attr("subTitle") || "";

      const slot = {
        icon: $(tags).find(`slot[name="icon"]`).html(),
        title: $(tags).find(`slot[name="title"]`).html(),
        footer: $(tags).find(`slot[name="footer"]`).html(),
        subTitle: $(tags).find(`slot[name="subTitle"]`).html(),
      };

      if (slot.icon) {
        icon = slot.icon;
      }
      if (slot.title) {
        title = slot.title;
      }
      if (slot.footer) {
        footer = slot.footer;
      }
      if (slot.subTitle) {
        subTitle = slot.subTitle;
      }

      const tpl = template(status, title, subTitle, footer, icon);

      addNode($(tag).eq(i), tpl);
    });
  };
  //skeleton骨架图
  const $Skeleton = () => {
    const prefixCls = "jv-skeleton";
    const tag = $(`j-skeleton`);

    tag.map((i, tags) => {
      const attribute = {
        id: $(tags).attr("id") || "",
        isActive: $(tags).data("active"),
        showTitle: $(tags).data("show-title"),
        showAvatar: $(tags).data("show-avatar"),
        titleWidth: $(tags).data("title-width") || 100,
        showParagraph: $(tags).data("show-paragraph"),
        paragraphRow: $(tags).data("paragraph-row") || 3,
        paragraphWidth: $(tags).data("paragraph-width") || [],
      };

      isUndef(attribute.isActive)
        ? (attribute.isActive = false)
        : attribute.isActive;
      isUndef(attribute.showTitle)
        ? (attribute.showTitle = true)
        : attribute.showTitle;
      isUndef(attribute.showAvatar)
        ? (attribute.showAvatar = false)
        : attribute.showAvatar;
      isUndef(attribute.showParagraph)
        ? (attribute.showParagraph = true)
        : attribute.showParagraph;

      let activeCls = "",
        paragraphRows = "",
        avatarContainer = "",
        wrapWithAvatarCls = "";

      if (attribute.isActive) {
        activeCls = `${prefixCls}-active`;
      }

      if (attribute.showAvatar) {
        wrapWithAvatarCls = `${prefixCls}-with-avatar`;
        avatarContainer = `<header class="${prefixCls}-header">
                                        <span class="${prefixCls}-avatar ${prefixCls}-avatar-circle"></span>
                                    </header>`;
      }

      const template = `<div class="${prefixCls} ${wrapWithAvatarCls} ${activeCls}" id="${attribute.id}">
                                    ${avatarContainer}
                                    <section class="${prefixCls}-content">
                                        <h3 class="${prefixCls}-title" style="width:${attribute.titleWidth};"></h3>
                                        <ul class="${prefixCls}-paragraph"></ul>
                                    </section>
                              </div>`;

      addNode($(tag).eq(i), template);

      if (attribute.paragraphRow > 0) {
        //根据 data-paragraph-row 的数值添加相等数量的占位图
        for (let j = 0; j < attribute.paragraphRow; j++) {
          //设置段落占位图的宽度，数组的值对应的每行宽度
          //若该数组不存在时则占位图宽度都设置为100%
          //如果数组中某个值设置为0，则相当于不设置该项，宽度等于默认100%
          if (
            attribute.paragraphWidth[j] === undefined ||
            attribute.paragraphWidth[j] === 0
          ) {
            attribute.paragraphWidth[j] = 100;
          }
          paragraphRows = `<li style=width:${attribute.paragraphWidth[j]}%></li>`;
          $(`.${prefixCls}-paragraph`).eq(i).append(paragraphRows);
        }
      }
    });
  };
  //spin旋转图标
  (() => {
    const template = (type, color) => {
      const prefixCls = "jv-spin";

      let spinType = "",
        zIndex = 2000;

      zIndex++;

      if (type === "dot") {
        spinType = `
                  <div class="${prefixCls}-inner">
                      <span class="${prefixCls}-dot"></span>
                  </div>
                `;
      }

      if (type === "border") {
        spinType = `
                    <div class=${prefixCls}-border ">
                        <svg viewBox="25 25 50 50" class="circular">
                            <circle cx="50" cy="50" r="20" fill="none" class="path"></circle>
                        </svg>
                    </div>
                `;
      }

      const html = $(`<div class=" ${prefixCls} ${prefixCls}-large ${prefixCls}-fix ${prefixCls}-${color} ${prefixCls}-fullscreen-wrap" role="status">
                                ${spinType}
                            </div>`).css("z-index", zIndex);
      return html;
    };

    $.extend({
      Spin: {
        show({ type = "dot", color = "primary" } = {}) {
          let h = template(type, color);
          $("body").append(h);
          return this;
        },
        hide() {
          destroy(
            $(".jv-spin-fullscreen-wrap"),
            $(".jv-spin-fullscreen-wrap").fadeOut("fast")
          );
          return this;
        },
      },
    });
    window.$.Spin = $.Spin;
  })();
  //switch开关按钮
  const $Switch = () => {
    const prefixCls = "jv-switch";
    const attr = {
      openColor: "#2196f3",
      closeColor: "#c5c8ce",
    };
    //获取自定义标签
    const jSwitch = $(`j-switch`);
    for (let i = 0; i < jSwitch.length; i++) {
      const switchs = jSwitch.eq(i);
      //获取标签属性值
      let size = switchs.data("size") || "medium", //尺寸
        cls = switchs.attr("class") || "", //class选择器
        id = switchs.attr("id") || "", //id选择器
        openText = switchs.data("open") || "", //开关打开后的文字
        closeText = switchs.data("close") || "", //开关关闭后的文字
        isActive = switchs.data("actived") || false, //是否选中
        isDisabled = switchs.attr("disabled") || false, //是否禁用
        trueColor = switchs.data("true-color") || attr.openColor, //开关打开后的颜色
        falseColor = switchs.data("false-color") || attr.closeColor; //开关关闭后的颜色
      //switch初始状态
      let flag = isActive;
      //创建dom
      let wrapper = $(`<span 
                              ${isDisabled}
                              id="${id}" 
                              tabindex="0"
                              class="${prefixCls} ${prefixCls}-${size} ${cls}"
                              data-actived=${flag}
                              role="switch">
                              </span>`),
        inner = $(`<span class=${prefixCls}-inner></span>`),
        _open = $(`<span>${openText}</span>`),
        close = $(`<span>${closeText}</span>`),
        ball = $(`<span class=${prefixCls}-ball></span>`);
      //自定义switch背景色，默认展示关闭状态下的颜色
      wrapper.append(inner, ball).css(`background`, `${falseColor}`);
      inner.append(close, _open);
      addNode(switchs, wrapper);
      //切换switch显示或隐藏文字
      const showAndHideText = (status) => {
        if (status) {
          _open.show();
          close.hide();
          inner.css("left", 7);
        } else {
          _open.hide();
          close.show();
          inner.css("left", 23);
        }
      };
      //点击switch移动圆球
      const switchBallMovement = (status) => {
        //获取容器和圆球宽度
        let containerWidth = wrapper.width(),
          ballWidth = ball.width();
        //计算移动距离
        let offsetX = containerWidth - ballWidth - 3;
        //根据不同状态true或false切换方向
        if (status) {
          ball.css(`transform`, `translateX(${offsetX}px)`);
        } else {
          ball.css(`transform`, `translateX(0)`);
        }
      };
      //点击switch切换状态
      const clickChangeSwitch = () => {
        if (flag === false) {
          flag = !flag;
          wrapper.css(`background`, `${trueColor}`).data("actived", flag);
          wrapper.attr("data-actived", flag); //目的是为了在页面的html代码能够显示 true 或 false
        } else {
          flag = !flag;
          wrapper.css(`background`, `${falseColor}`).data("actived", flag);
          wrapper.attr("data-actived", flag); //目的是为了在页面的html代码能够显示 true 或 false
        }
        switchBallMovement(flag);
        showAndHideText(flag);
      };
      //通过标签设置的属性开启是否选中状态
      const setActive = () => {
        if (isActive) {
          wrapper.css(`background`, `${trueColor}`).data("actived", isActive);
          wrapper.attr("data-actived", flag); //目的是为了在页面的html代码能够显示 true 或 false
        }
        switchBallMovement(isActive);
        showAndHideText(isActive);
      };
      setActive();
      //是否禁用switch
      if (isDisabled) wrapper.unbind("click");
      else {
        wrapper.click(() => {
          clickChangeSwitch();
        });
      }
    }
  };
  //timeline时间轴
  const $TimeLine = () => {
    const prefixCls = "jv-timeline";
    const tag = $(`j-timeline`);

    tag.map((i, tags) => {
      tags = $(tags);

      const itemTag = tags.children(`j-timeline-item`);

      let isPending = tags.data("pending"),
        pendingCls = "";

      isUndef(isPending) ? (isPending = false) : isPending;

      if (isPending) {
        pendingCls = `${prefixCls}-pending`;
      }

      const wrapper = $(`<ul class="${prefixCls} ${pendingCls}"></ul>`);

      itemTag.map((j, itemTags) => {
        itemTags = $(itemTags);

        const color = itemTags.attr("color") || "blue";
        const content = itemTags.html();
        const CustomizeDotContent = itemTags.attr("dot") || "";

        let isCustom = "";

        if (CustomizeDotContent) {
          isCustom = `${prefixCls}-item-head-custom`;
        }

        const items = $(`<li class="${prefixCls}-item">
                                      <div class="${prefixCls}-item-tail"></div>
                                      <div class="${prefixCls}-item-head ${isCustom} ${prefixCls}-item-head-${color}">${CustomizeDotContent}</div>
                                      <div class="${prefixCls}-item-content">${content}</div>
                                </li>`);
        wrapper.append(items);
      });
      addNode(tag.eq(i), wrapper);
    });
  };
  //modal模态提示框
  (() => {
    const prefixCls = "jv-modal";
    /**
     * 方法说明：
     * @method create  创建modal渲染到页面
     * @method show  显示modal
     * @method responsiveModal modal宽度响应式。当屏幕尺寸小于 768px 时，modal宽度会变为自动auto。
     */
    const $Modal = {
      create() {
        const tag = $(`j-modal`);
        tag.map((i, tags) => {
          tags = $(tags);
          let id = tags.attr("id"),
            width = tags.data("width") || "520px",
            title = tags.attr("title"),
            okText = tags.data("ok-text"),
            zIndex = tags.data("z-index") || 1000,
            content = tags.html(),
            closable = tags.data("closable"),
            showMask = tags.data("mask"),
            position = tags.data("position") || [20, 0],
            maxHeight = tags.data("max-height") || "none",
            footerHide = tags.data("footer-hide"),
            fullscreen = tags.data("fullscreen"),
            cancelText = tags.data("cancel-text"),
            maskClosable = tags.data("mask-closable");
          isUndef(closable) ? (closable = true) : closable;
          isUndef(showMask) ? (showMask = true) : showMask;
          isUndef(footerHide) ? (footerHide = false) : footerHide;
          isUndef(fullscreen) ? (fullscreen = false) : fullscreen;
          isUndef(position[1]) ? (position[1] = 0) : position; //只传入top值的情况下，left值为0
          isUndef(maskClosable) ? (maskClosable = true) : maskClosable;
          //html模板
          const modalTpl = `<div class="${prefixCls}-mask" style="z-index:${zIndex}"></div>
                          <div class="${prefixCls}-wrap" id=${id} style="z-index:${zIndex}">
                          <div 
                          role="dialog" 
                          class="${prefixCls}"
                          style="width:${width};top:${position[0]}px;left:${position[1]}px;">
                              <div class="${prefixCls}-main-box">
                                  <a href="javascript:0" class="${prefixCls}-close">
                                      <i class="jv-icon icon-cancel"></i>
                                  </a>
                                  <div class="${prefixCls}-header">
                                      <div class="${prefixCls}-header-title">${title}</div>
                                  </div>
                                  <div class="${prefixCls}-body" style="max-height:${maxHeight}">${content}</div>
                                  <div class="${prefixCls}-footer">
                                      <button class="jv-btn jv-btn-text ${prefixCls}-cancel">${cancelText}</button>
                                      <button class="jv-btn jv-btn-primary ${prefixCls}-ok">${okText}</button>
                                  </div>
                              </div>
                          </div>
                      </div> `;
          addNode(tag.eq(i), modalTpl);
          //默认点击确定和取消按钮关闭模态框
          $(`.${prefixCls}-ok`).click(() => {
            modalHidden();
          });
          $(`.${prefixCls}-cancel`).click(() => {
            modalHidden();
          });
          //如果标题为undefined则不显示标题栏
          const isShowHeader = () => {
            isUndef(title) ? $(`.${prefixCls}-header`).eq(i).remove() : title;
          };
          //是否点击右上角关闭按钮和ESC键关闭模态框
          const closableWidthEsc = () => {
            if (closable) {
              $(`.${prefixCls}-close`).click(() => {
                modalHidden();
              });
              $(document).keydown((e) => {
                e.keyCode === 27 ? modalHidden() : null;
              });
            } //移除关闭按钮
            else if (!closable) {
              $(`.${prefixCls}-close`).eq(i).remove();
            }
          };
          //是否全屏
          const isFullscreen = () => {
            fullscreen
              ? $(`.${prefixCls}`).eq(i).addClass(`${prefixCls}-fullscreen`)
              : !fullscreen;
          };
          //是否显示遮盖层
          const isShowMask = () => {
            if (!showMask) {
              //不显示显示遮盖层则禁用遮盖层关闭
              maskClosable = false;
              $(`.${prefixCls}-mask`).eq(i).addClass(`${prefixCls}-mask-hide`);
              $(`.${prefixCls}-wrap`)
                .eq(i)
                .addClass(`${prefixCls}-no-mask`)
                .css("z-index", 0);
            }
          };
          // 是否显示底部按钮栏
          const isShowFooter = () => {
            footerHide ? $(`.${prefixCls}-footer`).eq(i).remove() : !footerHide;
          };
          //是否点击遮盖层关闭
          const isMaskClosable = () => {
            if (maskClosable) {
              $(document).click((e) => {
                const con = $(`.${prefixCls}-wrap`).eq(i); // 设置目标区域
                if (con.is(e.target) && con.has(e.target).length === 0)
                  modalHidden();
              });
            }
          };
          const modalHidden = () => {
            let isFullScreen = $(`.${prefixCls}`)
              .eq(i)
              .hasClass(`${prefixCls}-fullscreen`);
            if (isFullScreen) {
              $(`.${prefixCls}`)
                .eq(i)
                .addClass(`${prefixCls}-fullscreen-leave`)
                .removeClass(`${prefixCls}-fullscreen-enter`)
                .fadeOut();
            } else {
              $(`.${prefixCls}`)
                .eq(i)
                .addClass(`${prefixCls}-leave`)
                .removeClass(`${prefixCls}-enter`)
                .fadeOut("fast");
            }
            $(`.${prefixCls}-mask`).eq(i).fadeOut("fast");
            $(`.${prefixCls}-wrap`).eq(i).fadeOut();
          };
          isShowHeader();
          closableWidthEsc();
          isFullscreen();
          isShowMask();
          isShowFooter();
          isMaskClosable();
        });
        return this;
      },
      /**
       * 参数说明：
       * @param {String} target （需触发的目标modal，实际上是将自定义标签的id选择器添加给当前modal的父容器wrap，再以它为原点向前查找mask，再查找自己内部modal元素，这样就得到了对应的mask和modal），
       * @param {Boolean} onlyHandleCb （是否只处理按钮回调，为true则不是用来展示modal而是只用来处理对应modal按钮的回调事件。配合点击事件触发展示modal时使用）
       * @param {Function} ok （确定按钮的回调），
       * @param {Function} cancel （取消按钮的回调）。
       */
      show(
        target,
        { onlyHandleCb = false, ok = () => {}, cancel = () => {} } = {}
      ) {
        target = $(target);
        const identifyPrefix = target.attr("id"); //为确定和取消按钮添加以当前trigge的id名为前缀的id选择器
        let btns = target.find($(`button`)), //获取当前modal下的确定和取消按钮
          okBtn = btns.eq(1).attr("id", `${identifyPrefix}-ok-btn`),
          zIndex = target.css("z-index"),
          modalMask = target.prev($(`.${prefixCls}-mask`)), //查找当前modal位置的遮盖层
          cancelBtn = btns.eq(0).attr("id", `${identifyPrefix}-cancel-btn`),
          targeModal = target.find($(`.${prefixCls}`)); //需触发的目标modal
        if (onlyHandleCb) {
          okBtn.click(ok);
          cancelBtn.click(cancel);
        } else {
          zIndex++;
          modalMask.css("z-index", zIndex).fadeIn("fast");
          target.fadeIn("fast").css("z-index", zIndex);
          //区别全屏和非全屏modal的入场动画
          if (targeModal.hasClass(`${prefixCls}-fullscreen`)) {
            targeModal
              .addClass(`${prefixCls}-fullscreen-enter`)
              .removeClass(`${prefixCls}-fullscreen-leave`)
              .show();
          } else {
            targeModal
              .addClass(`${prefixCls}-enter`)
              .removeClass(`${prefixCls}-leave`)
              .show();
          }
          okBtn.click(ok);
          cancelBtn.click(cancel);
        }
        return this;
      },
      responsiveModal() {
        //当屏幕尺寸小于 768px 时，modal宽度变为auto。
        $(window).resize(() => {
          $(window).width() < 768
            ? $(`.${prefixCls}`).addClass(`${prefixCls}-auto`)
            : $(`.${prefixCls}`).removeClass(`${prefixCls}-auto`);
        });
        return this;
      },
    };
    $Modal.create().responsiveModal();
    window.$Modal = $Modal;
  })();

  $Alert();
  $Avatar();
  $BackTop();
  $Badge();
  $Collapse();
  $DropDown();
  $Input();
  $InputNumber.create();
  $PageHeader();
  $Popover();
  $PopoverConfirm();
  $Result();
  $Skeleton();
  $Switch();
  $TimeLine();
});
