# JView UI

<img src="./logo.png" alt="logo" title="logo" width="250" />

JView UI is an open source UI component library based on JQuery for the Web

The application provides some basic UI components, mainly for PCS

Interface. Most of our original components needed to write multi-line HTML

Code can achieve the effect, encapsulated in a single line or a small amount of HTML code, and provides part of the API

To implement components, and we will continue to optimize and update more UI components.

## Summarize

- Short, concise, highly recognizable component HTML tags

- Reduce the amount of HTML code you write and avoid redundant, bulky structures

- Ideal for small - and medium-sized web pages

- Fine, beautiful UI

- Detailed documentation

- Friendly API

### Install

#### CSS file

Add the JView CSS file as the `<link>` TAB to the `<head>` TAB and place it before all other stylesheets.

```html
<link rel="stylesheet" href="JView.min.css"></link>
```

#### JS file

Many of the components that JView provides rely on jQuery as well as our own JavaScript plug-ins. Place the following `<script>` TAB on the page before placing it on the `<body>` TAB. In this order, jQuery is introduced first, followed by our JavaScript plug-in.

```html
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
<script src="JView.min.js"></script>
```

### Components demonstrate

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <!-- Example alert component -->
    <j-alert message="Hello, world!"></j-alert>

    <!-- 1.Reference to the jQuery -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <!-- 2.Reference to the JView js -->
    <script src="JView.min.js"></script>
    <script>
      $(() => {
        // Example Message component
        $.Message.info("This is a Message prompt from the Message component");
        // Example Notice component
        $.Notice.info(
          "This is a notification reminder from the Notice component"
        );
      });
    </script>
  </body>
</html>
```

### Related links

- [jQuery official document](https://jquery.com/)

- [ES6 introductory tutorial](https://es6.ruanyifeng.com/)

- [Gitee](https://gitee.com/Ryan-Zhong/jview-ui)
